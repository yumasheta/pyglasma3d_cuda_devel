---
title: Welcome
type: docs
---

# Welcome to the Wiki of "pyglasma3d_numba"!

## About this Project

This project is a port of the [pyglasma3d][pyglasma3d-cy] project to Python 3. It uses the [Numba][numba-link] open source JIT compiler to parallelize and accelerate the code and also offers [Numba][numba-link] based GPU acceleration with [CUDA][cuda-link] (so it is limited to compatible Nvidia GPUs).  
The currently present version is still in development and does lack lots of the features from the original [pyglasma3d][pyglasma3d-cy].


[//]: # (#TODO: rephrase this part and fix the links)
<=== *This section is out of date*  
In addition to the 'master' branch there is the ['cython-module-porting'](https://gitlab.com/yumasheta/pyglasma3d_cuda_devel/tree/cython_module_porting) branch. This branch provides the development path, which was used for porting the currently present modules from the original [pyglasma3d](https://gitlab.com/monolithu/pyglasma3d) version to Python 3. The main feature of this path is to allow to swap out single functions from the original version for their replacements in the 'pyglasma3d-numba' version. This enables to test the new code for bugs and other coding errors in an isolated environment.  
To streamline these tests, the Python 3 package 'test-consistency' is provided. At its core, this package compares the numerical results of identical simulation setups ran by the different versions of the code.

Please refer to this section of the Wiki on detailed explanations of the module porting process: [Branch: cython-module-porting](/cython-module-porting/home)  
and this section for how to use the consistency tests: [test-consistency](/test-consistency/home)  
===>

## Roadmap

  - v0.5  
    full CUDA documentation of what is implemented  
    final documentation, properly done with GitLab Pages + minimal READMEs (short, links to wiki)

  - v0.6  
    full documentation  
    move to standalone repository

  - v0.X  
    will have the interpolate charges loops fixed  
    and optimal init? with unified jit and minimal copying

  - v1.0  
    final optimizations,  
    perfect code

  - v1.X / vX.X  
    adding more features from the original pyglasma3d code  
    porting necessary modules to python


## Pyglasma3d-Numba Project

## Help for the consistency tests


[//]: # (references: )  
[pyglasma3d-cy]: https://gitlab.com/monolithu/pyglasma3d "Link to original Cython version"
[numba-link]: numba.pydata.org "Link to Numba homepage"
[cuda-link]: https://developer.nvidia.com/cuda-zone "Link to CUDA homepage"
