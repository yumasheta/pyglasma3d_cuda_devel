---
title: Python Package `test_consistency`
---

# Python Package `test_consistency`

## Contents

- [Quick-Start Guide](./_index#quick-start-guide)
  - [`start.sh` Script](./_index#startsh-script)
  - [`consistency_tests.py` Script](./_index#consistency_testspy-script)
  - [`evaluation` Module](./_index#evaluation-module)
- [User Guide](./_index#user-guide)
  - [Requirements](./_index#requirements)
  - [Execution Methods](./_index#execution-methods)
  - [Data File Format](./_index#data-file-format)
  - [Preparing Comparison Data](./_index#preparing-comparison-data)
- [Understanding the Test Results](./_index#understanding-the-test-results)
  - [Unittest Setups](./_index#unittest-setups)
  - [Test Outputs](./_index#test-outputs)
  - [Sample Plot](./_index#sample-plot)
  - [Sample Console Log](./_index#sample-console-log)

### Additional Documentation
- [`start.sh` Script][start-link]
- [`consistency_tests.py` Script][tests-link]
- [`evaluation` Module][eval-link]

* * * *

## Quick-Start Guide

This [test_consistency][folder-link] package provides tools to easily compare data files with  
numerical data. Its main purpose is to assist with the development of [pyglasma3d_numba][root-link].  
The different ways to use this package are:


#### `start.sh` Script
The [`start.sh`][start-link] script provides a set of preconfigured test setups to choose  
from for the [`consistency_tests.py`][tests-link] script. Upon execution, first the selected  
[simulation setup][sst-link] will be run and then the data it generated will be tested.

Use the [`start.sh`][start-link] script with the bash shell:  
```bash
$ bash start.sh
```
Or by calling it directly from the command line:
```bash
$ ./start.sh
```
Then follow the prompts on the screen.


#### `consistency_tests.py` Script

The [`consistency_tests.py`][tests-link] script provides a feature rich testing and analysis  
tool for comparing the numerical data of two files. It is built as a command line  
tool and offers customization options. Within the [test_consistency][folder-link] package  
this script is also used by the [`start.sh`][start-link] bash script.

The [`consistency_tests.py`][tests-link] script can be called directly, as long as all modules of  
the [test_consistency][folder-link] package are importable from the current working directory. Use   
the `-f` / `--files` parameter to specify the two data files to compare:
```
$ ./consistency_tests.py -f path/to/file1 path/to/file2
```
```
$ python3 consistency_tests.py -f path/to/file1 path/to/file2
```
```
$ python3 -m consistency_tests -f path/to/file1 path/to/file2
```
Use the `-h` / `--help` flag or see the section ['Parameters'][tests-params-link] for [`consistency_tests.py`][tests-link]  
for explanations of all available parameters.


#### `evaluation` Module

The calculations for the comparison of the data files are handled by the [`evaluation`][eval-link]  
module. It generates error values for every value compared from the data files. This  
module can be imported and used in custom code.


## User Guide

The [test_consistency][folder-link] package is located inside the root folder of the [pyglasma3d_numba][root-link]  
project and consists of the following files:
```
pyglasma3d_numba/
└─ test_consistency/
    ├─ evaluation.py            [custom module]
    ├─ __init__.py              [internal file]
    ├─ original/                [folder for data files]
    │  └─ ...                  
    ├─ README.md                [readme]
    ├─ start.sh                 [script for usage]
    ├─ TestRunner.py            [custom module]
    └─ consistency_tests.py     [script for usage]
```


### Requirements

This package requires:  
- Python version 3.8.1 or higher  

Additionally the following external modules are required:  

| Package                       | Version &#8805; |
| ----------------------------- | :-------------: |
| [matplotlib][matplotlib-link] |      3.1.2      |
| [numpy][numpy-link]           |     1.18.1      |


### Execution Methods

Two scripts:
- [`start.sh`][start-link]
- [`consistency_tests.py`][tests-link]

are provided with the [test_consistency][folder-link] package. They fit different usecases:

The [`start.sh`][start-link] script provides a set of preconfigured test setups for the  
[`consistency_tests.py`][tests-link] script to choose from. First the selected [simulation  
setup][sst-link] will be run and then the output will be compared to a [preconfigured file][start-files-link].  
This method is designed to assist with the development of the [pyglasma3d_numba][root-link]  
project. It allows quick testing for bugs that do not break the code, but produce  
wrong results when being compared to the [cython version][cython-link] of the pyglasma3d  
codebase. For detailed execution instructions please see the ['Execution'][start-exec-link] section  
for [`start.sh`][start-link].

The [`consistency_tests.py`][tests-link] script provides a feature rich testing and analysis  
tool for comparing the numerical data of two files. In contrast to [`start.sh`][start-link]  
[`consistency_tests.py`][tests-link] provides a number of [parameters][tests-params-link] to configure a custom  
test setup. For detailed execution instructions please see the ['Execution'][tests-exec-link] section for  
[`consistency_tests.py`][tests-link].

Both scripts produce the same kind of output (log to console and graphs). Please  
refer to the ['Understanding the Test Results'](./_index#understanding-the-test-results) section for explanations on the  
meanings of the output.


### Data File Format

The data files with the numerical data to compare have to follow a strict format  
which is required for the [`evaluation`][eval-link] module to work. They should be text  
files. On the first two lines there has to be one integer each. The product of those  
two integers plus 2 (val1*val2 + 2) has to match the number of lines the file has.  
On each subsequent line have to be five whitespace separated values. For more  
detail please see the ['Parameters'][eval-iter-params-link] section for the [`evaluation`][eval-link] module.

The data files produced by the [pyglasma3d_numba][root-link] simulation code meet these  
criteria after being converted to text files using the [`data_io` module][dio-link].
Some of the  
provided [simulation setups][sst-link] convert their binary output to text files as a last step.  

An example for a compatible data file is:
```
3
2
8.118377781300748965e-34 1.516547440201633402e-32 2.539145522631136880e-31 1.630555665954511669e-32 -2.783912529521835096e-32
7.006971246215732269e-34 1.674598777474349129e-32 2.740350899963080185e-31 1.139540826052872642e-32 -6.276361421468398732e-33
6.946098194118973359e-34 1.697061908076236292e-32 2.757402828885618003e-31 1.059846084545592635e-32 -1.045003201325233200e-33
6.946102784942463474e-34 1.697245103263265276e-32 2.758471643340202761e-31 1.059046310965086305e-32 -3.131123374594051781e-34
6.946102784942463474e-34 1.697245103263265276e-32 2.758471643340202761e-31 1.059046310965086305e-32 -3.043964740698971201e-34
6.946102784942463474e-34 1.697245103263265276e-32 2.758471643340202761e-31 1.059046310965086305e-32 -3.043964740698971201e-34
```

### Preparing Comparison Data

The [test_consistency][folder-link] package was designed to compare data files generated by  
the original [cython version][cython-link] of 'pyglasma3d' with the data files generated by   
[pyglasma3d_numba][root-link]. The following steps provide a guide for how to get comparison  
data for the [`mv_simple.py` setup][ssmvs-link] from the [cython version][cython-link]. The names and locations  
of the files are chosen correctly, so that they are compatible with the requirements  
of the [`start.sh`][start-link] script.

The [cython version][cython-link] requires modules as dependencies that may conflict with the  
[dependencies][deps-link] of [pyglasma3d_numba][root-link]. Please consider creating an extra virtual  
environment for using the [cython version][cython-link] with the following modules:
- Python 2.7.9
- NumPy 1.11.0
- Cython 0.24.1
- gcc 5.4.0 or equivalent C compiler

Note that the [cython version][cython-link] requires Python version 2.7.X.  
Compatibility with the newest versions of the modules listed above was tested,  
but no warranty or support will be provided when using newer version. For up  
to date version numbers please visit the [home page of the cython version][cython-link].

**Steps:**

1. Clone the git repository of the [cython version][cython-link] found on gitlab.com:  
   Via ssh:
   ```
   $ git clone git@gitlab.com:monolithu/pyglasma3d.git
   ```
   Or via https:
   ```
   $ git clone https://gitlab.com/monolithu/pyglasma3d.git
   ```
   Enter your credentials if prompted.

2. Change into the directory `pyglasma3d/`:  
    ```bash
    $ cd pyglasma3d
    ```

3. Compile the custom simulation modules with:
   ```bash
   $ python2 setup.py build_ext --inplace
   ```
   Note that this step requires Python 2.7.X.  
   This step is a manual workaround for compiling the modules. Using the  
   `setup.sh` script instead, will use whatever Python version is linked to the  
   `python` command in the current environment, which might not be 2.7.X.

4. On successful compilation the simulation can be run. Copy the `mv_simple.py`  
   setup from the root folder of the [test_consistency][folder-link] package into the `examples/`  
   folder of the [cython version][cython-link]:  
   (substitute 'path/to' with the correct path)
   ```bash
   $ cp path/to/pyglasma3d_numba/test_consistency/mv_simple.py pyglasma3d/examples/
   ```


5. Run the simulation:
   ```bash
   $ python2 -m pyglasma3d.examples.mv_simple
   ```

   After the simulation run completed a new folder `output/` will appear,  
   containing the binary file:
    - `T_mv_trial_simple.dat`  
    
   ```
   pyglasma3d/
    ├─ ...
    ├─ output/
    │   └─ T_mv_trial_simple.dat
    ├─ ...
   ```

6. This file needs to be converted to a text file. First copy the file into the `original/`  
   folder inside [test_consistency][folder-link] package:  
   (substitute 'path/to' with the correct path)
   ```bash
   $ cp pyglasma3d/output/T_mv_trial_simple.dat path/to/pyglasma3d_numba/test_consistency/original/
   ```

7. Change into the root directory of the [pyglasma3d_numba][root-link] project and start an  
   active Python interpreter:  
   (substitute 'path/to' with the correct path)
   ```
   $ cd path/to/pyglasma3d_numba
   $ python
   ```  
   Then import this module from `pyglasma3d_numba_source/`
   ```python
   import pyglasma3d_numba_source.data_io as io
   ```
   It provides helper functions to easily convert the file.

   Then execute this line of code to convert the binary file into a text file:
   ```python
   io.write("./test_consistency/original/T_mv_trial_simple_cython_textfile.dat", **io.read("./test_consistency/original/T_mv_trial_simple.dat"))
   ```
   This will create a text file in the `pyglasma3d_numba/test_consistency/original`  
   folder:
   - `T_mv_trial_simple_cython_textfile.dat`  
    
   ```
   pyglasma3d_numba/
   └─ test_consistency/
       ├─ ...
       ├─ original/
       │  ├─ T_mv_trial_simple.dat
       │  └─ T_mv_trial_simple_cython_textfile.dat
       ├─ ...
   ```
   The [`start.sh`][start-link] script is configured to source this file as the first file containing  
   the reference data when it calls the [`consistency_tests.py`][tests-link] script internally.


## Understanding the Test Results

In the following the term 'tests' will not only include the [unittests][tests-unit-link] conducted  
by the [`consistency_tests.py`][tests-link] script, but also every evaluation done on the  
comparison data itself.  
The 'number of matching digits' when comparing two numbers will be counted  
starting from the coefficient of the highest power of 10 until the first mismatched  
coefficient (please see the [`diff_statistics()`][eval-ds-link] section for the [`evaluation`][eval-link]  
module).


### Unittest Setups

These [unittests][tests-unit-link] are only meant to be a fast way to check for major errors. A <span style="color:green">'pass'</span>  
for these tests does not equate to a 'perfect match' of the data, nor does it give any  
information about the quality of the match. Assessments should only be based on the  
plots and logs discussed in the [next section](./_index#test-outputs).

These [unittests][tests-unit-link] probe the minimum and average number of matching digits against  
a chosen value. The tests will only be performed on one 'chunk' of the comparison  
data. For [pyglasma3d_numba][root-link] a 'chunk' corresponds to one simulation step. The  
amount of data included in one 'chunk' (or simulation step) is given by the integer  
value on the first line of each of the original data files. The default simulation step  
tested is the last step. A custom step can be set with the [`-s` parameter][tests-params-link].

The value for the limit of the matching digits should be chosen very carefully.  
It has to be matched with the precision of the numerical data types used and  
adjusted according to any mathematically imprecise operations used in the  
code. The defaults set are 15 digits for standard 'double precision' and 10  
digits with the [`--fastmath` flag][tests-params-link] set, to account for accuracy loss based on  
faster evaluation of standard mathematical functions (eg. sin(), log()). The  
limit can be set to a custom value with the [`-p` parameter][tests-params-link].

For reference, the 'IEEE-754' double precision standard defines 15 - 17 digits  
as the maximum amount of significant digits for standard mathematical  
operations. The 'IEEE-754' standard is the most used for double precision  
(see https://en.wikipedia.org/wiki/Double-precision_floating-point_format).  

In the special case of Python, the precision used on the target machine is  
saved in the named tuple [`sys.float_info`](https://docs.python.org/3/library/sys.html?highlight=float_info#sys.float_info) and typically will be 15. Officially,  
Python adheres to the ['C99' standard](http://www.open-std.org/jtc1/sc22/wg14/www/docs/n1256.pdf), where at §5.2.4.2.2 p.25f the 'DBL_DIG'  
value is given as 10. However, on p.27f example 2 describes a floating point  
representation that is also compliant with the 'IEEE-754' double precision standard  
and requires 'DBL_DIG' to be 15.  
It is also important to check any modules using a C backend. Numpy, for example,  
provides a precision value of 15 digits for its `numpy.float64` (the default) floating  
point type (as can be checked with `numpy.finfo(np.float64).precision`).  
So it matches the standard Python `float` type.  

Another special case to consider for the [pyglasma3d_numba][root-link] project is the  
precision used by [Numba](http://numba.pydata.org/). There is no specific value listed in the [Numba  
documentation](http://numba.pydata.org/numba-doc/latest/reference/fpsemantics.html), but insted compliance with both, the 'IEEE-754' and 'C99'  
standards is assured. These standards get 'relaxed' when using the [`fastmath`](https://numba.pydata.org/numba-doc/dev/user/performance-tips.html#fastmath)  
option during just-in-time compilation.

Based on this information, the default values for the [unittests][tests-unit-link] are set to  
15 digits for standard calculations and 10 digits for `fastmath` enabled  
calculations.


### Test Outputs

At the end of this page are examples for the test outputs consisting of plots and data  
logged to the console. They are separated based on output that is only produced  
when using [`start.sh`][start-link] and output that is produced for both [execution methods](./_index#execution-methods)  
when the [`-v flag`][tests-params-link] is set.  
As [`start.sh`][start-link] runs a [simulation setup][sst-link] and then the [`consistency_tests.py`][tests-link] script,  
its output will be embedded in the outputs of the latter two.


**[`start.sh`][start-link] Script**

The only output unique to the [`start.sh`][start-link] script are three timing values:
- 'real'
- 'user'
- 'sys'  

which are the output of the bash built-in command `time`. They show the elapsed  
real time ('real'), user cpu time ('user') and system cpu time ('sys') for  
the chosen simulation setup, including the amount of time it took the Python  
interpreter to initialize.  
These values provide an accurate approximation of the total runtime, but cannot  
be treated as 'profiling data'. However, averaging multiple runs and comparing  
different setups is a valid procedure.


**[`consistency_tests.py`][tests-link] Script**

The output of the [`consistency_tests.py`][tests-link] script starts with a line of '#' which is  
followed by a log of the data files compared. If the [`-v` flag][tests-params-link] is set (like in the example),  
the next part of the output until the next line of '#' will show the numerical values, that  
will be used for the plots.  

The first table lists the mean, max and std values for the differences of the two data  
files for each 'chunk' (simulation step). In this raw form, the numbers represent the  
difference values normed to the magnitude of the data (see the section on the  
[`diff_statistics()` function][eval-ds-link]). For negative exponents the value of the exponent  
is equal to the number of digits the compared data matches. For positive exponents  
the error is equal to the magnitudes of the compared data. The leading factor tells  
how large the numerical value of the error is. Only the absolute values of the differences  
are used, disregarding any minus signs. Ideally all exponents should be negative  
and their values should be around the precision specified.

After that the bin edges and frequency data for the historgam are logged.

The last part of the output will show the 'Unit Test Report'. It will log the success  
or failure of the configured [unittests][tests-unit-link], with some information on the cause for  
the latter case. The 'Unit Test Report' will always show, ignoring the [`-v` flag][tests-params-link], except  
when [`--skiptests`][tests-params-link] is set.


**Plots**

The plots are the most important results of the evaluation and will show how well  
the compared data matches together. By default the generated image will contain  
three graphs:
- Matching Digits for each Simulation Step
- Histogram of Matching Digits
- Matching Digits vs Data Value Magnitudes  

For for available customization options please see the ['Structure'][tests-unit-link] section for  
[`consistency_tests.py`][tests-link].

The first plot is the visualization of the table of data mentioned before. For each  
'chunk' (simulation step) the mean and minimum number of matching digits are  
plotted as lines. The shaded are in between these lines marks the standard deviation.  
The value for the chosen precision ('limit') is denoted by a dashed magenta line.  
Ideally this plot should show the limit line above all other values for all simulation  
steps.  
In the [provided example](./_index#sample-plot) are large deviations for the first part of the simulation  
run. Those differences vanish towards the end of the run, where the target limit  
is reached by the mean number of matching digits, but not by the minimun number.  
This result is confirmed by the [sample unittests](./_index#sample-console-log), which passed for the mean 
value  
but not for the minimum value.

For further judgement of the error, the other plots should be used. The histogram shows  
the frequency distribution of the number of matching digits. It shows that the majority  
of the values does lie below the limit (dashed magenta line). However the distribution is  
skewed towards large errors.

Lastly, when examining the third plot, it becomes clear, that the compared data deviates  
for small magnitudes of the original data. Large magnitudes are near the limit. Even for  
small magnitudes parts of the data are around the limit, while others do not match at all.

Taking all those aspects into account, it is possible to assess the the results. Although  
the first plot would suggest a rather bad match, the other plots do show, that the  
majority of the difference values meet the set limit. That means, that the first 20  
simulation steps are mixed values of good and bad matches.  

For further evaluation the background of the original data (the simulation) is required.  
In this case, the first half of the simulation produces output that is very localized and  
close to zero on a broad scale. The localized values are large numbers, which according  
to the examination are always matching very well. The rest of the values are close to  
zero, which gives a bad match. Now it can also be explained, why there are difference  
values of zero (perfect matches of all digits). The initial part of the simulation data  
contains large amounts of 0.0 values that are set to 0.0 exactly. The fast increase in  
matching digits around the 20th simulation step can be explained by considering, that  
standard mathematical functions acting on values very close to zero might not yield  
stable values. Around the 20th simulation step, the data values change from localized  
to spread out, so that all output now has about the same magnitude and is no longer  
close to zero. Hence the mathematical functions will yield stable results.

This example should provide a guide for how to interpret the results of the 'consistency  
tests'. The last decision on wether or not the compared data can be considered 'equal'  
will always depend on the particular example and its background. A rather good match  
could be not good enough for one case, whereas a rather bad match could still be  
sufficient for another case.


### Sample Plot
![sample plot](sample-plot.png)


### Sample Console Log

[`start.sh`][start-link]
```
...
...
10.5 Complete cycle. 0.013
10.5 Writing energy momentum tensor to file. 0.071


converting binary output file to text file...

real    0m13.761s
user    0m13.978s
sys     0m0.543s

calling consistency_test.py with the following arguments:
...
...
```

* * * *

[`consistency_tests.py`][tests-link]
```
$ ./consistency_tests.py -f original/source1.dat ../output/source2.dat -v -p 12


 ########################################################################################################### 

statistics on the differences of the input data for files:

source1.dat      and      source2.dat


iter step   mean                             max                              std
01          1.35577513829527407e+00          7.74888939547188187e+01          5.50970142251704065e+00
02          2.26365120406981868e+00          1.02877499651102951e+02          1.20787460922383953e+01
03          1.15009304294217141e+00          3.06004059695398837e+02          1.26631266052475553e+01
04          7.87784438753769689e-01          2.05693557604310513e+02          9.22254932934870908e+00
05          1.73564889204917000e-01          4.41079251521181774e+01          1.83345508377370514e+00
06          9.60185454223270035e-02          9.32408380883245869e+00          5.51220599454362414e-01
07          3.01340317367190302e-01          1.22600075645861011e+02          5.04831749860405843e+00
08          6.08174065759607210e-02          4.20814329350417715e+00          2.89134040859762353e-01
09          4.43888696505132310e-02          2.56987229502978742e+00          2.20787443872144690e-01
10          3.50617259233448053e-02          6.33863068477764546e+00          3.21629791835636836e-01
11          2.96954729970064155e-02          6.91109047474592231e+00          3.14315498685657946e-01
12          1.15276070887471629e-02          3.30973908567323782e+00          1.37568470770284501e-01
13          1.16771949429892996e-02          4.45285821160801731e+00          1.79309423253428385e-01
14          1.96902842421528600e-03          2.96802780547799350e-01          1.90669002888077463e-02
15          1.08762796855263488e-03          2.46376214891992268e-01          1.44119577373309250e-02
16          2.49172543953420608e-05          5.73914057904975107e-03          2.93176333079626624e-04
17          5.58517332655165808e-07          1.34614942929934026e-04          7.01453953193296769e-06
18          7.31127422846260111e-08          1.83406384155369379e-05          9.09209775088760163e-07
19          7.90262335366803274e-09          2.53396570481578707e-06          1.17568084200348622e-07
20          8.03306887088818682e-10          2.21888109133479746e-07          9.97896467991271115e-09
21          1.31781895666571552e-10          2.60323124972753992e-08          1.24678113422254276e-09
22          2.83153980946671553e-11          1.98392522320204758e-09          1.47634197854159882e-10
23          1.52985467931831168e-11          1.18074680809563681e-09          7.43755435127281596e-11
24          8.91379507373511869e-12          5.37864499789802890e-10          3.91529436164786606e-11
25          5.74941482907897239e-12          3.07515973245163781e-10          2.53365964351700262e-11
26          4.16571926929091587e-12          2.31200289931021421e-10          1.88774448250151683e-11
27          2.50359987866224794e-12          2.39399304162647058e-10          1.31392147029293180e-11
28          1.37074981843111085e-12          8.51843829696908748e-11          5.45034018314416472e-12
29          8.34914316988791812e-13          2.05228629229781134e-11          1.98989733876906178e-12
30          6.90675927207212226e-13          2.01835076429901505e-11          1.38784843261321192e-12
31          6.03640895639448087e-13          1.49121166803656280e-11          9.01164532162344935e-13
32          6.32351180192231383e-13          1.80834080348857285e-11          1.01786370241056221e-12
33          6.16458701232689083e-13          2.16536857888804946e-11          1.14214193229268491e-12
34          7.23448349252382781e-13          9.07190988996831038e-11          3.73059707507050039e-12
35          6.49980031561578440e-13          3.80311017053602818e-11          1.73786791095697974e-12
36          6.80196187142296074e-13          2.07971660726169461e-11          1.24272327657413728e-12
37          6.75574326900987559e-13          1.79261987198753303e-11          1.02226265169151048e-12
38          7.27324053125903206e-13          2.06204411185018088e-11          1.46768346327258552e-12
39          7.68872038553888280e-13          4.38234518118640828e-11          2.16583221317711108e-12
40          7.74437849263032007e-13          3.55340756819089165e-11          1.95215905493762232e-12
41          7.65694469496693876e-13          2.54397197751998760e-11          1.52630123397558897e-12
42          8.82222123424921458e-13          6.19751645836164045e-11          2.95757323583797538e-12

historgram data:
bin edges: 0e+00    1e-18    1e-17    1e-16    1e-15    1e-14    1e-13    1e-12    1e-11    1e-10    1e-09    1e-08    1e-07    1e-06    1e-05    1e-04    1e-03    1e-02    1e-01    1e+00    1e+01    1e+03    
frequency:      725      0        0        154      694      3560     10878    3988     2264     1517     382      176      144      155      116      107      129      459      707      233      72       

 ########################################################################################################### 

              Unit Test Report
Status: 
    Pass: 2

Description:
Summary: 
    +-------------------------------------------------------------------------------------------+-------+------+------+-------+
    |                                    Test group/Test case                                   | Count | Pass | Fail | Error |
    +-------------------------------------------------------------------------------------------+-------+------+------+-------+
    | TestDataConsistency: Unittests on difference values of data for specified iteration step. | 2     | 2    | 0    | 0     |
    | Total                                                                                     | 2     | 2    | 0    | 0     |
    +-------------------------------------------------------------------------------------------+-------+------+------+-------+
    TestDataConsistency
        +---------------------------------------------------------+---------------------------------------------------------------------------------------------+------------+
        |                        Test name                        |                                            Stack                                            |   Status   |
        +---------------------------------------------------------+---------------------------------------------------------------------------------------------+------------+
        | test_max_accuracy: Check if max delta is below limit.   | Traceback (most recent call last):                                                          | fail       |
        |                                                         |   File "./consistency_tests.py", line 264, in test_max_accuracy                             |            |
        |                                                         |     self.assertTrue(math.isclose(self.results[2], 0.0, abs_tol=float(f"1e-{args.digits}")), |            |
        |                                                         | AssertionError: False is not true : max delta below 1e-12 limit                             |            |
        | test_mean_accuracy: Check if mean delta is below limit. |                                                                                             | pass       |
        +---------------------------------------------------------+---------------------------------------------------------------------------------------------+------------+
```


[//]: # (references: )  
[folder-link]: TODO:_link_to_package_root "Link to Package Folder in Project Repository"
[root-link]: TODO:_link_to_project_root "Link to Project Repository"
[start-link]: ./test-consistency-start
[start-exec-link]: ./test-consistency-start#execution
[start-files-link]: ./test-consistency-start#comparison-data-and-default-arguments
[tests-link]: ./test-consistency-tests
[tests-params-link]: ./test-consistency-tests#parameters
[tests-exec-link]: ./test-consistency-tests#execution
[tests-unit-link]: ./test-consistency-tests#structure
[eval-link]: ./test-consistency-evaluation
[eval-iter-params-link]: ./test-consistency-evaluation#read-iter-params
[eval-ds-link]: ./test-consistency-evaluation#function-diff_statistics
[sst-link]: TODO:_link_to_sim_setups
[ssmvs-link]: TODO:_link_to_sim_setup_mv_simple
[matplotlib-link]: https://pypi.org/project/matplotlib/
[numpy-link]: https://pypi.org/project/numpy/
[cython-link]: https://gitlab.com/monolithu/pyglasma3d
[dio-link]: #TODO:_link_to_data_io_module
[deps-link]: #TODO:_link_to_project_dependencies
[unitt-link]: https://docs.python.org/3/library/unittest.html?highlight=unittest#module-unittest "Link to Python Docs"