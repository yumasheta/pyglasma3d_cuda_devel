---
title: Python Script `consistency_tests.py`
---

# Python Script `consistency_tests.py


## Contents

- [Overview](./test-consistency-tests#overview)
- [User Guide](./test-consistency-tests#user-guide)
  - [Structure](./test-consistency-tests#structure)
  - [Execution](./test-consistency-tests#execution)
  - [Data File Format](./test-consistency-tests#data-file-format)
  - [Parameters](./test-consistency-tests#parameters)
  - [Output](./test-consistency-tests#output)
- [Example](./test-consistency-tests#example)


## Overview

The [`consistency_tests.py`][file-link] script provides a feature rich testing and analysis  
tool for comparing the numerical data of two files. It is built as a command line  
tool and offers customization options. Within the ['test_consistency'][home-link] package  
this script is also used by the [`start.sh`][start-link] bash script.

The [`consistency_tests.py`][file-link] script can either be used indirectly via the  
[`start.sh`][start-link] script:
```
$ ./start.sh
```
Or it can be called directly, as long as all modules of the ['test_consistency'][home-link]  
package are importable from the current working directory. Use the  
`-f` / `--files` parameter to specify the two data files to compare:
```
$ ./consistency_tests.py -f path/to/file1 path/to/file2
```
```
$ python3 consistency_tests.py -f path/to/file1 path/to/file2
```
```
$ python3 -m consistency_tests -f path/to/file1 path/to/file2
```
Use the `-h` / `--help` flag or see the section ['Parameters'](./test-consistency-tests#parameters) for explanations of  
all available parameters.


## User Guide

This script is designed as an executable script. As such it is not importable as  
a module. When trying to import this script the [`ImportError`][imperr-link] exception will  
be raised with the following error message:
```
WARNING!
This `test_consistency.py` script cannot be imported or called as a secondary module.
```


### Structure
This script accomplishes three tasks:  

#### 1. Comparing the Data

The function [`diff_statistics()`][diff-link] from the [`evaluation`][eval-link] module is  
used to calculate the differences of the numerical data supplied.

#### 2. Statistical Evaluation of the comparison Data

The function [`diff_statistics()`][diff-link] additionally provides statistical data  
on the difference values, which are then presented to the user in an easily  
interpretable form. Graphs and numerical values can be controlled by  
[command line parameters](./test-consistency-tests#parameters). For help on how to interpret the results please  
see the section ['Understanding the Test Results'][res-link]. A sample output is provided  
in the ['Output'](./test-consistency-tests#output) section below.

By default, three graphs are drawn:
1. **'Matching Digits for each Simulation Step'**  
   Showing mean+&#963; and min values
2. **'Histogram of Matching Digits'**  
   Showing frequency values accumulated over all simulation steps
3. **'Matching Digits vs. Data Value Magnitudes'**  
   Showing values accumulated over all simulation steps

The third plot will contain a data point for each data value present in the  
data files, as it uses the return data `scatter` from the [`diff_statistics()`][diff-link]  
function. For large data files it might not be possible to load all data values  
into memory. Hence, the flag `--hide` is provided which disables the third plot  
and also passes the `skip_scatter` parameter to [`diff_statistics()`][diff-link].  

The standard behavior is to show a pop-up window with the graphs. The script  
pauses as long as this window is open. For available manipulation options  
please see the [matplotlib documentation][mpl-link].  
To change this the option `-g` / `--graphs` can be set to a valid image file path  
where the graphs will be saved automatically and the pop-up window will be  
suppressed. This option can also be set to `False`, which will disable all plotting  
functionality.

If the flag `-v` / `--verbose` is set, the data for the `results` and `(hist, bins)`  
return values of [`diff_statistics()`][diff-link] are printed to the terminal. This  
output can additionally be written to a file by setting the `-o` / `--output`  
option to a valid text file path.


#### 3. Unittests on statistical Evaluation Data

A [unittest][unitt-link] testsuite is set up to give quick feedback about the comparison.  
The unittests are only performed for a single simulation step on the `results`  
return value of [`diff_statistics()`][diff-link]. As default the last step is chosen.  
This can be changed by setting the `-s` / `--step` option to the number of the  
desired simulation step (negative values will count backwards).

`results` contains information about the average and minimal number of  
matching digits of the data compared. The unittests compare these numbers  
with predefined values and fail if the numbers for the matching digits are too low.

In particular the default testcase probes for 15 matching digits. If the flag  
`--fastmath` is set, this limit is reduced to 10 digits. It is also possible to set a  
custom limit with the `-p` / `--precision` option. However, either `--fastmath`  
or `-p` / `--precision` are allowed, not both. For more background on the  
number of matching digits please see the section ['Understanding the Test Results'][res-link].

The results of the unittests are printed to the console. All tests can be skipped  
by setting the flag `--skiptests`.


### Execution

#### Indirectly via `start.sh`

```
$ ./start.sh
```

The [`start.sh`][start-link] script uses this script with the following options:  
```
-f original/T_mv_trial_simple_cython_textfile.dat ../output/T_mv_trial_simple_textfile.dat -v [--fastmath]
```
- `-f`:  
  The first file is located in the subfolder `original/`.  
  The second path points to a file that is created by the simulation setup [mv_simple.py][mvs-link].
- `-v`:  
  The verbose flag is set to print output to the console.
- `--fastmath`:  
  Depending on the test setup chosen when running [`start.sh`][start-link], the `--fastmath` flag  
  is either added or not.

For help about the [`start.sh`][start-link] script please visit the section 
['Bash Script `start.sh`'][start-link].

#### Direct Usage

**Requirements**

As part of the ['test_consistency'][home-link] package, the [`consistency_tests.py`][file-link] script  
requires other modules of that package to work. These are the [`evaluation`][eval-link]  
module, used for calculating the comparison data and the `TestRunner` module,  
used for printing nicely formatted test results.  
Those two modules need to be importable from the current working directory. The  
recommended minimal file tree for the current working directory is:  
```
/
├─ evaluation.py
├─ TestRunner.py
└─ consistency_tests.py
```
If one of those modules cannot be found in [`sys.path`][ppath-link], a [`ModuelNotFoundError`][moderr-link]  
will be raised. The traceback will also log the current working directory.

**Execution**

This script can be called like any other Python script, as long as it is the  
main module. For the first two options, the specified Python interpreter  
will execute the script.

```
$ python consistency_tests.py -f path/to/file1 path/to/file2
```
```
$ python -m consistency_tests -f path/to/file1 path/to/file2
```
It is also possible to execute the script directly, without specifying an interpreter:
```
$ ./consistency_tests.py -f path/to/file1 path/to/file2
```
In this case the interpreter set for the current environment will be used,  
as returned by this command:
```
/usr/bin/env python
```

### Data File Format

The paths to the data files to compare specified by the required `-f / --files`  
flag need to be valid paths to those files. The paths can either be absolute, or  
relative. For the latter case, each path has to be reachable from the current  
working directory.

**File Format**

The supplied data files have to obey a certain format as required by the  
[`evaluation`][eval-params-link] module. First of all, they must be text files. On the first two  
lines there has to be one integer each. The product of those two integers  
plus 2 (val1*val2 + 2) has to match the number of lines the file has. On each  
subsequent line have to be five whitespace separated values that are compatible  
with the [float64 format of Numpy][npf-link].

The data files produced by the [pyglasma3d_numba][root-link] simulation code meet these  
criteria after being converted to text files using the [`data_io` module][dio-link].
Some of the  
provided [simulation setups][simset-link] convert their binary output to a text file as a last step.  

For more information on the format requirements please see the sections about  
the [`evaluation`][eval-link] module or [`data_io`][dio-link] module.


### Parameters

For all available parameters use `-h`/ `--help`:

```
$ ./consistency_tests.py -h
usage: consistency_tests.py [-h] -f path/to/file path/to/file [-v] [--hide] [-o path/to/file] [-g path/to/file or True/False] [--skiptests] [-s step] [--fastmath | -p {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18}]

A python script to plot and test difference data.

This python script is meant to be executed from the command line, either
directly `./consistency_tests.py` or via `python consistency_tests.py`.
For available command line options use the '-h' or '--help' flags.

The command line argument '-f' / '--files' is required and takes to paths
to the data files to analyze as string arguments.

The differences are calculated using the `evaluation.py` module.

optional arguments:
  -h, --help            show this help message and exit.
                         
  -f path/to/file path/to/file, --files path/to/file path/to/file
                        specify two files to compare
                        this argument is required
                         
  -v, --verbose         print evaluation results to terminal
                        default: not set
                         
  --hide                set this flag to skip the third plot
                        this is useful if there is not enough system memory to save all the data to compare
                        default: not set
                         
  -o path/to/file, --output path/to/file
                        specify a file to write evaluation data to
                        default: None
                         
  -g path/to/file or True/False, --graphs path/to/file or True/False
                        prepare 3 (or 2 with option --hide) plots of data
                        if set to True, plots are shown interactively
                        if set to a string representing a path, plots are saved to the given path
                        default: True
                         
  --skiptests           skip all unittests
                        default: not set
                         
  -s step, --step step  specify the iteration step on which to perform the unittests
                        negative values count backwards
                        default: -1, the last iteration step
                         
  -p {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18}, --precision {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18}
                        set exponent "p" used for calculating precision for unittests: 1e-"p"
                        this option conflicts with --fastmath
                        default: 15
                         
  --fastmath            change accuracy in unittests to 1e-10
                        this option conflicts with -p / --precision
                        default: not set
```

### Output

The [example](./test-consistency-tests#sample-console-log) below shows how a typical output looks like. After a line  
of '#' the names of the two files to compare are repeated. If the `-v` /  
`--verbose` flag is set (like in the example), the next part of the output  
until the next line of '#' will show the values of the `results` and  
`(hist, bins)` return values of [`diff_statistics()`][diff-link].  

At this point of the execution, either a pop-up window with the [plots](./test-consistency-tests#sample-plot)  
will show up or the script will continue, depending on how the `-g` / `--graphs`  
parameter was used. If a window with the plots pops up, the script will pause,  
until that window is closed. For available manipulation options please see the  
[matplotlib documentation][mpl-link].

The last part of the output will be the 'Unit Test Report'. It will log the success  
or failure of the configured [unittests](./test-consistency-tests#3-unittests-on-statistical-evaluation-data).

For help on how to interpret these results please see the section  
['Understanding the Test Results'][res-link].

## Example

### Sample Plot
![sample plot](sample-plot.png)


### Sample Console Log
```
$ ./consistency_tests.py -f original/source1.dat ../output/source2.dat -v -p 12


 ########################################################################################################### 

statistics on the differences of the input data for files:

source1.dat      and      source2.dat


iter step   mean                             max                              std
01          1.35577513829527407e+00          7.74888939547188187e+01          5.50970142251704065e+00
02          2.26365120406981868e+00          1.02877499651102951e+02          1.20787460922383953e+01
03          1.15009304294217141e+00          3.06004059695398837e+02          1.26631266052475553e+01
04          7.87784438753769689e-01          2.05693557604310513e+02          9.22254932934870908e+00
05          1.73564889204917000e-01          4.41079251521181774e+01          1.83345508377370514e+00
06          9.60185454223270035e-02          9.32408380883245869e+00          5.51220599454362414e-01
07          3.01340317367190302e-01          1.22600075645861011e+02          5.04831749860405843e+00
08          6.08174065759607210e-02          4.20814329350417715e+00          2.89134040859762353e-01
09          4.43888696505132310e-02          2.56987229502978742e+00          2.20787443872144690e-01
10          3.50617259233448053e-02          6.33863068477764546e+00          3.21629791835636836e-01
11          2.96954729970064155e-02          6.91109047474592231e+00          3.14315498685657946e-01
12          1.15276070887471629e-02          3.30973908567323782e+00          1.37568470770284501e-01
13          1.16771949429892996e-02          4.45285821160801731e+00          1.79309423253428385e-01
14          1.96902842421528600e-03          2.96802780547799350e-01          1.90669002888077463e-02
15          1.08762796855263488e-03          2.46376214891992268e-01          1.44119577373309250e-02
16          2.49172543953420608e-05          5.73914057904975107e-03          2.93176333079626624e-04
17          5.58517332655165808e-07          1.34614942929934026e-04          7.01453953193296769e-06
18          7.31127422846260111e-08          1.83406384155369379e-05          9.09209775088760163e-07
19          7.90262335366803274e-09          2.53396570481578707e-06          1.17568084200348622e-07
20          8.03306887088818682e-10          2.21888109133479746e-07          9.97896467991271115e-09
21          1.31781895666571552e-10          2.60323124972753992e-08          1.24678113422254276e-09
22          2.83153980946671553e-11          1.98392522320204758e-09          1.47634197854159882e-10
23          1.52985467931831168e-11          1.18074680809563681e-09          7.43755435127281596e-11
24          8.91379507373511869e-12          5.37864499789802890e-10          3.91529436164786606e-11
25          5.74941482907897239e-12          3.07515973245163781e-10          2.53365964351700262e-11
26          4.16571926929091587e-12          2.31200289931021421e-10          1.88774448250151683e-11
27          2.50359987866224794e-12          2.39399304162647058e-10          1.31392147029293180e-11
28          1.37074981843111085e-12          8.51843829696908748e-11          5.45034018314416472e-12
29          8.34914316988791812e-13          2.05228629229781134e-11          1.98989733876906178e-12
30          6.90675927207212226e-13          2.01835076429901505e-11          1.38784843261321192e-12
31          6.03640895639448087e-13          1.49121166803656280e-11          9.01164532162344935e-13
32          6.32351180192231383e-13          1.80834080348857285e-11          1.01786370241056221e-12
33          6.16458701232689083e-13          2.16536857888804946e-11          1.14214193229268491e-12
34          7.23448349252382781e-13          9.07190988996831038e-11          3.73059707507050039e-12
35          6.49980031561578440e-13          3.80311017053602818e-11          1.73786791095697974e-12
36          6.80196187142296074e-13          2.07971660726169461e-11          1.24272327657413728e-12
37          6.75574326900987559e-13          1.79261987198753303e-11          1.02226265169151048e-12
38          7.27324053125903206e-13          2.06204411185018088e-11          1.46768346327258552e-12
39          7.68872038553888280e-13          4.38234518118640828e-11          2.16583221317711108e-12
40          7.74437849263032007e-13          3.55340756819089165e-11          1.95215905493762232e-12
41          7.65694469496693876e-13          2.54397197751998760e-11          1.52630123397558897e-12
42          8.82222123424921458e-13          6.19751645836164045e-11          2.95757323583797538e-12

historgram data:
bin edges: 0e+00    1e-18    1e-17    1e-16    1e-15    1e-14    1e-13    1e-12    1e-11    1e-10    1e-09    1e-08    1e-07    1e-06    1e-05    1e-04    1e-03    1e-02    1e-01    1e+00    1e+01    1e+03    
frequency:      725      0        0        154      694      3560     10878    3988     2264     1517     382      176      144      155      116      107      129      459      707      233      72       

 ########################################################################################################### 

              Unit Test Report
Status: 
    Pass: 2

Description:
Summary: 
    +-------------------------------------------------------------------------------------------+-------+------+------+-------+
    |                                    Test group/Test case                                   | Count | Pass | Fail | Error |
    +-------------------------------------------------------------------------------------------+-------+------+------+-------+
    | TestDataConsistency: Unittests on difference values of data for specified iteration step. | 2     | 2    | 0    | 0     |
    | Total                                                                                     | 2     | 2    | 0    | 0     |
    +-------------------------------------------------------------------------------------------+-------+------+------+-------+
    TestDataConsistency
        +---------------------------------------------------------+---------------------------------------------------------------------------------------------+------------+
        |                        Test name                        |                                            Stack                                            |   Status   |
        +---------------------------------------------------------+---------------------------------------------------------------------------------------------+------------+
        | test_max_accuracy: Check if max delta is below limit.   | Traceback (most recent call last):                                                          | fail       |
        |                                                         |   File "./consistency_tests.py", line 264, in test_max_accuracy                             |            |
        |                                                         |     self.assertTrue(math.isclose(self.results[2], 0.0, abs_tol=float(f"1e-{args.digits}")), |            |
        |                                                         | AssertionError: False is not true : max delta below 1e-12 limit                             |            |
        | test_mean_accuracy: Check if mean delta is below limit. |                                                                                             | pass       |
        +---------------------------------------------------------+---------------------------------------------------------------------------------------------+------------+
```


[//]: # (references: )  
[home-link]: ./_index
[start-link]: ./test-consistency-start
[eval-link]: ./test-consistency-evaluation
[eval-params-link]: ./test-consistency-evaluation#read-iter-params
[diff-link]: ./test-consistency-evaluation#function-diff_statistics
[res-link]: ./_index#understanding-the-test-results
[mpl-link]: https://matplotlib.org/users/navigation_toolbar.html "Link to matplotlib Wiki"
[unitt-link]: https://docs.python.org/3/library/unittest.html?highlight=unittest#module-unittest "Link do Python Docs"
[imperr-link]: https://docs.python.org/3/library/exceptions.html#ImportError "Link to Python Docs"
[moderr-link]: https://docs.python.org/3/library/exceptions.html#ModuleNotFoundError "Link to Python Docs"
[mvs-link]: #TODO:_add_link_to_sim_setup_simple
[ppath-link]: https://docs.python.org/3/library/sys.html#sys.path "Link to Python Docs"
[npf-link]: https://docs.scipy.org/doc/numpy/user/basics.types.html "Link to Numpy Docs: Data types"
[dio-link]: #TODO:link_to_data_io_module
[simset-link]: #TODO:link_to_sim_setups
[root-link]: #TODO:_link_to_project_home "Link to Project Repository"
[file-link]: #TODO:_link_to_this_file_in_repo "Link to File in Project Repository"