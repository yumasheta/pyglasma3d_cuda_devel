---
title: Python Module `evaluation`
---

# Python Module `evaluation`

## Contents

- [Overview](./test-consistency-evaluation#overview)
- [Function `diff_statistics()`](./test-consistency-evaluation#function-diff_statistics)
  - [Parameters](./test-consistency-evaluation#diff-stat-params)
  - [Return Values](./test-consistency-evaluation#diff-stat-return)
- [Generator Function `read_textfile_iter()`][ript-link]
  - [Parameters](./test-consistency-evaluation#read-iter-params)
  - [Return Value](./test-consistency-evaluation#read-iter-return)


* * * *


## Overview

The [`evaluation`][file-link] module is meant to be used in conjunction with the  
[`consistency_tests.py`](./test-consistency-tests) script. It calculates the differences of numerical  
data supplied by data files and performs statistical analysis on the differences.  

The supplied data files have to obey a certain layout. On the first two lines  
there has to be one integer each. The product of those two integers plus 2  
(val1*val2 + 2) has to match the number of lines the file has. On each subsequent  
line there have to be five whitespace separated values that are compatible with  
the [float64 format of Numpy][npf-link]. The data files produced by the [pyglasma3d_numba][root-link]  
simulation code meet these criteria after being converted to text files using the  
[`data_io`][dio-link] module.  
For an in depth explanation of this format please see the section on the  
[Generator Function `read_textfile_iter()`][ript-link].

The [`evaluation`][file-link] module provides two functions:

## Function `diff_statistics()`

The function `diff_statistics()` calculates the element wise differences  
of supplied numerical data files and normalizes those difference values to  
represent the error in significant digits. The resulting numbers can be easily  
interpreted and are displayed as standard floating point numbers. For negative  
exponents the value of the exponent is equal to the number of digits the  
compared data matches. For positive exponents the error is equal to the  
magnitudes of the compared data. The leading factor tells how large the  
numerical value of the error is. Only the absolute values of the differences  
are used, disregarding any minus signs.

The difference values are calculated following this simple method:  

1. Two 1D Numpy arrays with identical shapes that represent each data set  
   are required. They are sourced from the arguments supplied.  
3. The per element difference of the Numpy arrays is calculated.
4. The calculated difference values are normalized with respect to the  
   magnitudes of the first data file's values. If the first data file contains 0,  
   the other file's data values' magnitudes are used for that specific  
   difference value. If both are 0, the difference is 0 as well.

For example:  
The compared values match up to 7 digits. The 8th digits show a difference of 3.  
```
val1: 1.111111111111111e+03
val2: 1.111111425270376e+03
diff: 0.000000314159265e+03
normalized difference: 3.14159265e-07
```  

The required 1D Numpy arrays are built by calling the [`read_textfile_iter()`][ript-link]  
generator function with the specified files. As this function represents an iterator  
over the provided file, the calculations mentioned above take place in a loop over  
this iterator.

### Parameters {#diff-stat-params}  

There is one required parameter `files_dict` and one optional parameter  
`skip_scatter`.

`files_dict`  

The `files_dict` is required to be a Python dictionary of the following format:  
```python
files_dict = {
    "source1": "path1",
    "source2": "path2"
    }
```
The dictionary has to have two elements with the hard coded key names  
"source1" and "source2". Each of these elements is a string representing  
the path of one of the data files to source. The file paths have to be  
absolute or relative to the current working directory. Any [`KeyErrors`][kerr-link]  
raised because of supplying a wrong `files_dict` will be handled. 

`skip_scatter`  

The parameter `skip_scatter` is an optional boolean flag, that is set to  
`False` per default. If set to `True`, the return value `scatter` (see next  
section) will be set to `None` and all associated calculations will be skipped.  
Use this if dealing with very large files with lots of data values whose  
accumulated size would surpass the system memory available. It will  
prevent storing all difference and data values in memory. Instead only  
the difference and data values for each iteration step over the  
[`read_textfile_iter()`][ript-link] generator 
function as well as the other return  
values will be stored in memory.

### Return Values {#diff-stat-return}  

The `diff_statistics()` function returns a tuple of three values:  
``` python
results, (hist, bins), scatter
```  
The first, `results`, is a Numpy array with shape (N, 3), where N is the number  
of iterations done over the input files by using [`read_textfile_iter()`][ript-link]. 
For  
each iteration three values are stored in this array with the following order:  
```
(mean, std, max)
```
Here, `mean` is the average difference value for each iteration, `std` is the  
standard deviation for each iteration and `max` is the maximum difference  
value for each iteration.

The second value `(hist, bins)` is a tuple itself. For each iteration step  
the frequency data of the difference values for a histogram with bins `bins`  
is calculated. The bin edges are:  
```
[0, 1e-18, 1e-17, 1e-16, 1e-15, 1e-14, 1e-13, 1e-12, 1e-11, 1e-10, 1e-09, 1e-08, 1e-07, 1e-06, 1e-05, 1e-4, 1e-3, 1e-2, 1e-1, 1, 1e+1, 1e+3]
```
If there are difference values which could not be assigned to one of these  
bins, a warning is issued, but the evaluation continues.  
The frequency values for each iteration step are accumulated in the `hist`  
Numpy array. This array has the shape ```(len(bins)-1, )``` and carries the  
[Numpy integer][npf-link] data type.

The `scatter` value is controlled by the parameter `skip_scatter` (see  
section above). If `skip_scatter` is set to `False` (default), scatter will be a  
2D Numpy array with shape (2, n) where n is the total number of all difference  
values. This number is equal to the number of iterations times the size of  
the array returned from [`read_textfile_iter()`][ript-link].  
The first dimension of `scatter` contains the n data values from the first  
source as specified in the `files_dict` parameter. The second dimension  
contains the n difference values calculated over the course of all iterations.  
The 2*n values represent (val, diff) pairs and are the values of a scatter plot.  
For example:
```python
scatter = [[10, 11, 12, 13, 14, 15, 16, 17, 18, 19],
           [ 0,  1,  2,  3,  4,  5,  6,  7,  8,  9]])
pairs = [(v, d) for v, d in zip(scatter[0], scatter[1])]
# [(10, 0), (11, 1), (12, 2), (13, 3), (14, 4), (15, 5), (16, 6), (17, 7), (18, 8), (19, 9)]
```  
If `skip_scatter` is set to `True`, `scatter` will be `None` and none of the  
calculations just mentioned are performed.

 
## Generator Function `read_textfile_iter()`

The function `read_textfile_iter()` is an IO helper [generator function][genf-link]  
that allows to iterate over a file. For each iteration step it yields a Numpy  
array with a subset of the numerical data the file contains. The benefit of  
using this function compared to loading data from the file directly is, that  
only the part of the file required for the current iteration will be loaded into  
memory as opposed to the whole file.

### Parameters {#read-iter-params}  

The function takes one argument `file` as a string representing a valid path  
to the data file. The standard build-in Python function [`open()`][open-link] is used to get  
a file object for the data file. Any [`OSErrors`][oserr-link] raised will be handled.  

The supplied data file has to obey a certain format. First of all, the file must  
be a text file. On the first two lines there has to be one integer each. The first  
value will specify the number of lines to read at each iteration. The second  
value will specify the number of iterations necessary to deplete all the data of  
the file. The product of those two integers plus 2 (val1*val2 + 2) has to match  
the number of lines the file has. If this is not the case and the last iteration does  
not yield the expected number of values, [`sys.exit()`][syse-link] is called and the current  
Python instance will exit. On each subsequent line have to be five whitespace  
separated values that are compatible with the [float64 format of Numpy][npf-link]. Any  
[`ValueErrors`][valerr-link] raised by incompatible data types will be handled.  

The data files produced by the [pyglasma3d_numba][root-link] simulation code meet these  
criteria after being converted to text files using the [`data_io` module][dio-link].
Some of the  
provided [simulation setups][simset-link] convert their binary output to test files as a last step.  

For these data files the iteration steps of this function will match the internal  
time steps of the simulation. The number of lines read each iteration will match  
the size of the simulation grid.

### Return Value {#read-iter-return}  

At each iteration step the [Numpy function loadtxt()][npltxt-link] is used to read the specified  
number of lines and get the corresponding Numpy array object. This Numpy  
array then gets flattened by using the [Numpy function ravel()][npr-link] and is returned  
as the return value. It's shape is (n, ) where n is the number of lines read  
multiplied with 5.


[//]: # (references: )  
[ript-link]: ./test-consistency-evaluation#generator-function-read_textfile_iter
[dio-link]: #TODO:link_to_data_io_module
[simset-link]: #TODO:link_to_sim_setups
[root-link]: #TODO:_link_to_project_home "Link to Project Repository"
[file-link]: #TODO:_link_to_this_file_in_repo "Link to File in Project Repository"
[genf-link]: https://wiki.python.org/moin/Generators "Link to Python Wiki: Generators"
[open-link]: https://docs.python.org/3/library/functions.html#open "Link to Python Docs: open()"
[syse-link]: https://docs.python.org/3/library/sys.html#sys.exit "Link to Python Docs: sys"
[kerr-link]: https://docs.python.org/3/library/exceptions.html#KeyError "Link to Python Docs"
[oserr-link]: https://docs.python.org/3/library/exceptions.html#OSError "Link to Python Docs"
[valerr-link]: https://docs.python.org/3/library/exceptions.html#ValueError "Link to Python Docs"
[npf-link]: https://docs.scipy.org/doc/numpy/user/basics.types.html "Link to Numpy Docs: Data types"
[npltxt-link]: https://docs.scipy.org/doc/numpy/reference/generated/numpy.loadtxt.html "Link to Numpy Docs: loadtxt()"
[npr-link]: https://docs.scipy.org/doc/numpy/reference/generated/numpy.ravel.html "Link to Numpy Docs: ravel()"
