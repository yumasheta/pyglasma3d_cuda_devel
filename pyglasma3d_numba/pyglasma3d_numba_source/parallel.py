"""Parallel execution helpers.

A module providing a decorator 'make_parallel_loop' that is to be used
to decorate the body functions for a parallel loop and convert it to
a callable object that executes the loop in parallel.
The execution device is the device specified in the numba_target module,
or can be specified on a per call basis.

Additional helper functions to set values of fields in parallel are
included. These are also helpful for basic operations on arrays residing
 in device memory.
"""
# disable flake8 and pylint for this file
# flake8: noqa:
# pylint: disable=unused-argument

import math
from functools import wraps
from textwrap import dedent

import numpy as np

# numba module gets used in dynamic function definitions
# disable flake8 and pylint errors for that
# pylint: disable=unused-import
from .numba_target import default_jit, fastmath, numba, use_device

if use_device == "cuda":
    from .numba_target import cuda


# Unique counter for compiling numba or CUDA parallel loops
_unique_counter = 0


# parallel loops for use by decorator
# one for pure python, compiled numba and compiled cuda each


def launch_pure_python_no_sum(kernel_function, iter_min, iter_max, args):
    """Loop from `iter_min` to `iter_max`, executing `kernel_function`.

    This is a non-parallel version implemented in pure Python. No sums
    are calculated during parallel execution (no reductions!).

    The arguments of `kernel_function` are passed on with `args`.
    """
    # basic loop over kernel function
    for xi in range(iter_min, iter_max):
        kernel_function(xi, *args)


def launch_pure_python_one_sum(kernel_function, iter_min, iter_max, args):
    """Loop from `iter_min` to `iter_max`, executing `kernel_function`.

    This is a non-parallel version implemented in pure Python. One sum
    is calculated during the parallel execution (one reduction!).

    The arguments of `kernel_function` are passed on with `args`.
    """
    result = 0
    for xi in range(iter_min, iter_max):
        result += kernel_function(xi, *args)
    return result


def launch_pure_python_sums(kernel_function, iter_min, iter_max, args):
    """Loop from `iter_min` to `iter_max`, executing `kernel_function`.

    This is a non-parallel version implemented in pure Python. Many sums
    are calculated during the parallel execution (reductions!). Their
    number was set as an argument for the decorator
    `make_parallel_loop()`.

    The arguments of `kernel_function` are passed on with `args`.
    """
    result = [0.0 for _ in range(kernel_function.sums)]
    for xi in range(iter_min, iter_max):
        temp_tuple = kernel_function(xi, *args)
        for i in range(kernel_function.sums):
            result[i] += temp_tuple[i]
    return result


def prep_pure_python(kernel_function, iter_min, iter_max, args, sums):
    """Save sums as kernel_function attribute."""

    kernel_function.sums = sums


def launch_cuda_kernel_no_sum(kernel_function, iter_min, iter_max, args, tpb=256):
    """Loop from `iter_min` to `iter_max`, executing `kernel_function`.

    This runs in parallel on the compute device. No sums are calculated
    during parallel execution (no reductions!).

    The arguments of `kernel_function` are passed on with `args`.
    `tpb` is the number of threads per block and can be specified on a
    per call basis.
    """
    # TODO: what are the best parameters?
    # set cuda kernel execution parameters
    # threadsperblock = tpb = 256
    blockspergrid = math.ceil((iter_max - iter_min) / tpb)
    # Call the compiled cuda kernel function
    kernel_function.compiled_cuda_kernel[blockspergrid, tpb](iter_min, iter_max, *args)


def launch_cuda_kernel_one_sum(kernel_function, iter_min, iter_max, args, tpb=256):
    """Loop from `iter_min` to `iter_max`, executing `kernel_function`.

    This runs in parallel on the compute device. One sum is calculated
    during the parallel execution (one reduction!).

    The arguments of `kernel_function` are passed on with `args`.
    `tpb` is the number of threads per block and can be specified on a
    per call basis.
    """
    # TODO: what are the best parameters?
    # set cuda kernel execution parameters
    # threadsperblock = tpb = 256
    blockspergrid = math.ceil((iter_max - iter_min) / tpb)

    # for performing a reduction reset return array on device
    # consider using 'tpb' option to set optimal values for _zero_kernel
    # set_field_zero_kernel() signature is changed
    # pylint: disable=too-many-function-args
    set_field_zero_kernel(
        0, kernel_function.return_array.size, kernel_function.return_array
    )
    # Call the compiled cuda kernel function
    kernel_function.compiled_cuda_kernel[blockspergrid, tpb](
        iter_min, iter_max, kernel_function.return_array, *args
    )
    # copy result to host
    return_array = kernel_function.return_array.copy_to_host()

    return return_array[0]


def launch_cuda_kernel_sums(kernel_function, iter_min, iter_max, args, tpb=256):
    """Loop from `iter_min` to `iter_max`, executing `kernel_function`.

    This runs in parallel on the compute device. Many sums are
    calculated during the parallel execution (reductions!). Their number
    was set as an argument for the decorator `make_parallel_loop()`.

    The arguments of `kernel_function` are passed on with `args`.
    `tpb` is the number of threads per block and can be specified on a
    per call basis.
    """
    # TODO: what are the best parameters?
    # set cuda kernel execution parameters
    # threadsperblock = tpb = 256
    blockspergrid = math.ceil((iter_max - iter_min) / tpb)

    # for performing a reduction reset return array on device
    # consider using 'tpb' option to set optimal values for _zero_kernel
    # set_field_zero_kernel() signature is changed
    # pylint: disable=too-many-function-args
    set_field_zero_kernel(
        0, kernel_function.return_array.size, kernel_function.return_array
    )
    # Call the compiled cuda kernel function
    kernel_function.compiled_cuda_kernel[blockspergrid, tpb](
        iter_min, iter_max, kernel_function.return_array, *args
    )
    # copy result to host
    return_array = kernel_function.return_array.copy_to_host()

    return return_array


def prep_cuda_kernel(kernel_function, iter_min, iter_max, args, sums):
    """Compile a parallel loop with `kernel_function` as body.

    The loop will run in parallel on the compute device.

    The arguments of `kernel_function` are passed on with `args`.
    If there are sums to be calculated for the loop (reductions),
    `sums` is set to the number of sums.

    A pointer to the compiled function is saved as the attribute:
    `compiled_cuda_kernel` of `kernel_function`.
    """

    # treat as global for assignments to work
    global _unique_counter  # pylint: disable=global-statement
    _unique_counter += 1

    # Create string of arguments:
    # 'c1, c2, c3, [...]' for len(args)
    args_string = ", ".join("c" + str(i) for i in range(len(args)))

    # set 'original_name' identifier
    original_name = kernel_function.py_func.__name__

    # reduction in parallel loop
    if sums:
        # cuda functions (kernels) cannot return a value
        # they need to write to an array
        host_return_array = np.zeros(sums)
        # make the array available for each call as a function property
        kernel_function.return_array = cuda.to_device(host_return_array)
        # create string: r0, r1, ... for return_array.size
        result_vars = ", ".join("r" + str(i) for i in range(sums))
        # create string:
        # cuda.atomic.add(return_array, index, value)
        # as many times as return_array.size
        atomic_addition_command = "; ".join(
            "cuda.atomic.add(device_return_array, " + str(i) + ", r" + str(i) + ")"
            for i in range(sums)
        )
        # the definition of the cuda kernel as a string
        code = f"""\
        def {original_name}_cuda_kernel(
            iter_min, iter_max, device_return_array, {args_string}
        ):
            xi = cuda.grid(1)
            if xi < (iter_max - iter_min):
                {result_vars} = _kernel_function_{_unique_counter}(
                    xi + iter_min,
                    {args_string},
                )
                {atomic_addition_command}
        """

    else:
        # if no reduction is to be performed
        # the definition of the cuda kernel as a string
        code = f"""\
        def {original_name}_cuda_kernel(iter_min, iter_max, {args_string}):
            xi = cuda.grid(1)
            if xi < (iter_max - iter_min):
                _kernel_function_{_unique_counter}(xi + iter_min, {args_string})
        """

    locals_copy = locals()
    globals_copy = globals()
    globals_copy[f"_kernel_function_{_unique_counter}"] = kernel_function
    # execute the code in "code" to define kernel function object
    # use dedent to get correct indentation for code string
    exec(dedent(code), globals_copy, locals_copy)  # pylint: disable=exec-used
    cuda_kernel = locals_copy[original_name + "_cuda_kernel"]
    # save compiled 'cuda_kernel' as 'kernel_function' property
    kernel_function.compiled_cuda_kernel = cuda.jit(cuda_kernel, fastmath=fastmath)


def launch_compiled_numba(kernel_function, iter_min, iter_max, args):
    """Loop from `iter_min` to `iter_max`, executing `kernel_function`.

    This runs in parallel on the host device. Sums can be calculated
    during the parallel execution (reductions!). Their number was set as
    an argument for the decorator `make_parallel_loop()`.

    The arguments of `kernel_function` are passed on with `args`.
    """
    return kernel_function.compiled_numba_prange(iter_min, iter_max, *args)


def prep_compiled_numba(kernel_function, iter_min, iter_max, args, sums):
    """Compile a parallel loop with `kernel_function` as body.

    The loop will run in parallel on the host device.

    The arguments of `kernel_function` are passed on with `args`.
    If there are sums to be calculated for the loop (reductions),
    `sums` is set to the number of sums.

    A pointer to the compiled function is saved as the attribute:
    `compiled_numba_prange` of `kernel_function`.
    """

    # treat as global for assignments to work
    global _unique_counter  # pylint: disable=global-statement
    _unique_counter += 1

    # Create string of arguments 'c1, c2, c3, [...]' for len(args)
    args_string = ", ".join("c" + str(i) for i in range(len(args)))

    # set 'original_name' identifier
    original_name = kernel_function.py_func.__name__

    # reduction in parallel loop
    if sums:
        # create string for initialization: r1=0; r2=0; ...
        # for number of sums
        init_string = "; ".join("r" + str(i) + "=0" for i in range(sums))
        # create string for tmp return values from 'kernel_function':
        # rt0, rt1, ... for number of sums
        return_string_tmp = ", ".join("rt" + str(i) for i in range(sums))
        # create string for reduction: r0 += rt0 ... for number of sums
        reduce_string = "; ".join("r" + str(i) + "+=rt" + str(i) for i in range(sums))
        # create string of return values:
        # r1, r2, r3, ... for number of sums
        return_string = ", ".join("r" + str(i) for i in range(sums))
        # the definition of the numba prange function as a string
        code = f"""\
        def {original_name}_numba_prange(iter_min, iter_max, {args_string}):
            {init_string}
            for xi in numba.prange(iter_min, iter_max):
                {return_string_tmp} = _kernel_function_{_unique_counter}(
                    xi,
                    {args_string},
                )
                {reduce_string}
            return {return_string}
        """

    else:
        # no reduction
        code = f"""\
        def {original_name}_numba_prange(iter_min, iter_max, {args_string}):
            for xi in numba.prange(iter_min, iter_max):
                _kernel_function_{_unique_counter}(xi, {args_string})
        """

    locals_copy = locals()
    globals_copy = globals()
    globals_copy[f"_kernel_function_{_unique_counter}"] = kernel_function
    # execute the code in "code" to define numba prange function object
    # use dedent to get correct indentation for code string
    exec(dedent(code), globals_copy, locals_copy)  # pylint: disable=exec-used
    numba_prange = locals_copy[original_name + "_numba_prange"]
    # save compiled 'numba_prange' as 'kernel_function' property
    kernel_function.compiled_numba_prange = default_jit(numba_prange, parallel=True)
    # alternative:
    # numba.jit(numba_prange, nopython=True, nogil=True, fastmath=True,
    #           parallel=True)


def make_parallel_loop(*args, sums=None):
    """Decorate functions defining bodies of loops.

    Using this decorator will change the USAGE and ARGUMENTS of the
    decorated function!
    The decorated function needs to be a loop body accepting the current
    loop index as its first argument. After decoration, the returned
    function will be a full loop on its own. The first argument (loop
    index) will be replaced with 'start' and 'stop' arguments. All other
    arguments stay the same.
    Keyword arguments of the decorated function will be ignored!
    Depending on the exact case, new keyword arguments may be available.

    Accepts any number of positional arguments. But only the first one
    gets used. That should be the function to decorate. Any other
    positional arguments will be ignored.
    Additionally one optional keyword argument `sums` for the number of
    sums to calculate (reductions) can be supplied.

    :param args: optional positional arguments;
                 the function to decorate (kernel_function);
    :param int sums: positive integer to specify number of sums for
                     parallel loop reduction;
                     must match the number of return values from
                     the kernel_function;
                     for the reduction to work, the kernel_function
                     must return a tuple (or single value) which
                     contains the summands for each sum;
                     defaults to None
    :return: function pointer
    """

    # dict with the preparation functions
    prep_func_dict = {
        "cuda": prep_cuda_kernel,
        "python": prep_pure_python,
        "numba": prep_compiled_numba,
    }

    # dict with full loops; depends on sums
    if not sums:
        func_dict = {
            "cuda": launch_cuda_kernel_no_sum,
            "python": launch_pure_python_no_sum,
            "numba": launch_compiled_numba,
        }

    elif sums == 1:
        func_dict = {
            "cuda": launch_cuda_kernel_one_sum,
            "python": launch_pure_python_one_sum,
            "numba": launch_compiled_numba,
        }

    elif sums:
        # sums > 1
        func_dict = {
            "cuda": launch_cuda_kernel_sums,
            "python": launch_pure_python_sums,
            "numba": launch_compiled_numba,
        }

    def loop_decorator(kernel_function):
        """Decorate functions defining bodies of loops.

        :param kernel_function: numba.jit decorated kernel function with one
                                positional argument for the loop index and
                                optional, additional positional arguments;
                                keyword arguments are not allowed
        :type kernel_function: numba.dispatcher.Dispatcher
        :return: function pointer
        """

        # keep the original name and docstring of kernel function
        @wraps(kernel_function)
        def parallel_loop(iter_min, iter_max, *args, force_dev=None, **kwargs):
            """Perform parallel loop over a kernel function.

            On CPU use numba.prange(), on GPU use a compiled cuda kernel.

            The supplied kernel function has to be decorated with any numba
            jit decorator, including the custom `numba_target.default_jit`.

            :param int iter_min: starting index for iteration.
            :param int iter_max: maximum index for iteration
                                 (analogous to python default `range()`)
            :param args: optional positional arguments to pass on to the
                         kernel_function
            :param str force_dev: overrule global setting for parallel
                                  execution method;
                                  string: 'cuda', 'numba', 'python';
                                  defaults to None for using global setting
            :param kwargs: optional keyword arguments to pass on to the
                           kernel_function launcher;
                           supported values vary based on execution method
            """

            if force_dev and use_device != "python":
                # if overwrite is set, but keep python mode
                _use_device = force_dev
            else:
                # use new var for this scope
                _use_device = use_device

            try:
                # call compiled function
                return func_dict[_use_device](
                    kernel_function, iter_min, iter_max, args, **kwargs
                )

            except AttributeError:
                # if no compiled function is found, compile it first
                prep_func_dict[_use_device](
                    kernel_function, iter_min, iter_max, args, sums
                )
                # then call compiled function
                return func_dict[_use_device](
                    kernel_function, iter_min, iter_max, args, **kwargs
                )

            except KeyError as e:
                print(
                    f'\n\nThe optional argument "force_dev" with value:\n\t{e}\n'
                    "was supplied to the function:\n"
                    f'\t"{kernel_function.py_func.__name__}"\n'
                    "But this does not match any of the accepted values:\n"
                    "- cuda\n- numba\n- python\n\n"
                )
                raise e

            except TypeError as e:
                print(
                    "\n\nThe function call for:\n"
                    f'\t"{kernel_function.py_func.__name__}"\n'
                    f"failed because of unexpected arguments:\n\t{e}\n"
                    "please check the traceback below...\n\n"
                )
                raise e

        return parallel_loop

    if args:
        # the first element is the decorated function
        # so decorate it and return the decorated version
        return loop_decorator(args[0])

    # else:
    # args is empty; return the decorator
    return loop_decorator


# helper function to set / reset fields to specific values in parallel
# on the execution device specified


@make_parallel_loop
@default_jit
def set_field_zero_kernel(ix, field):
    """Set all values of slice field[iter_min:iter_max] to '0.0'.

    Equivalent to numpy: field[iter_min:iter_max]=0.0

    `make_parallel_loop` CHANGES THE USAGE AND ARGUMENTS!

    Signature:
        set_field_zero_kernel(iter_min, iter_max, field, force_dev=None)
    Params:
        int iter_min:
            starting index for iteration
        int iter_max:
            maximum index for iteration (same as python default range())
        numpy.ndarray field:
            array to set elements zero
        str force_dev:
            overrule global setting for parallel execution method;
            string: "cuda", "numba", "python";
            defaults to None for using global setting
    """

    field[ix] = 0.0


@make_parallel_loop
@default_jit
def set_field_to_constant_kernel(ix, field, const, step, offset):
    """Manipulate all values of slice field[start:stop].

    Set all values of slice field[start:stop] to const,
    if (index - start) is multiple of step.
    Equivalent to numpy: field[start:stop:step] = const

    `make_parallel_loop` CHANGES THE USAGE AND ARGUMENTS!

    Signature:
        set_field_to_constant_kernel(
            0, iter_max, field, const, step, offset, force_dev=None
        )

    Params:
        int 0:
            has to be set to `0`
        int iter_max:
            has to be the following value:
            `iter_max` = int(np.ceil( (`stop`-`start`) / `step`))
            where stop is the index after the last value of `field`
            to be modified
        numpy.ndarray field:
            array to set elements
        float64 const:
            the constant to set field elements to
        int step:
            step size for looping through `field`
        int offset:
            starting index for iteration: `start`
        str force_dev:
            overrule global setting for parallel execution method;
            string: "cuda", "numba", "python";
            defaults to None for using global setting
    """

    field[ix * step + offset] = const


@make_parallel_loop
@default_jit
def set_field_to_field_kernel(ix, field, source, step, offset):
    """Manipulate all values of slice field[start, stop].

    Set all values of slice field[start, stop] to values of source,
    if (index - start) is multiple of step.
    Equivalent to numpy: field[start:stop:step] = source

    `make_parallel_loop` CHANGES THE USAGE AND ARGUMENTS!

    Signature:
        set_field_to_field_kernel(
            0, iter_max, field, source, step, offset, force_dev=None
        )

    Params:
        int 0:
            has to be set to `0`
        int iter_max:
            the length of the source array: `source.size`
            matches the following value:
            `iter_max` = int(np.ceil( (`stop`-`start`) / `step`))
            where stop is the index after the last value of `field`
            to be modified
        numpy.ndarray field:
            array to set elements
        numpy.ndarray source:
            array to source elements
            `source.size` has to be equal to `iter_max`
        int step:
            step size for looping through `field`
        int offset:
            starting index for iteration: `start`
        str force_dev:
            overrule global setting for parallel execution method;
            string: "cuda", "numba", "python";
            defaults to None for using global setting
    """

    field[ix * step + offset] = source[ix]
