"""Consistency test.

This package should not be imported. It is designed to work as a
command line script by calling `consistency_tests.py`.
However, if you know what you are doing, the module `evaluation.py` can
technically be imported and used in other code.
"""

# set __all__ for star imports
__all__ = ["evaluation"]
