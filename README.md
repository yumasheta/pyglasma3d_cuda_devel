# pyglasma3d_cuda_devel

As of 09/2020, this project is no longer maintained.  
Development has moved to [openpixi/Pyglasma3d_Numba](https://gitlab.com/openpixi/pyglasma3d_numba).

- - -

The `pyglasma3d_cuda_devel` project is a port of the [pyglasma3d](https://gitlab.com/monolithu/pyglasma3d) project to Python3 with Numba and CUDA acceleration.

In addition to the `master` branch, there is the `cython_module_porting` branch.
It documents the development path that was used for porting modules from the original [pyglasma3d](https://gitlab.com/monolithu/pyglasma3d) version to Python3.
This path is built upon the possibility to swap out single functions of the original [pyglasma3d](https://gitlab.com/monolithu/pyglasma3d) code for new code of the `pyglasma3d_numba` version.
Supplementary to the `cython_module_porting` branch, there is the tag `initial_code_swapping_tests`.
It is a collection of early tests used to develop the code swapping method. The code is outdated, but the simple examples are much more illustrative for explaining the working principles of that method.