CONFIGURATION PARAMETERS:

--------------------------------------------------------------------------------

Arguments for run_benchmark.sh:
 -w '/home/ipp/tmp/benchmark_scratch' -o '/home/ipp/tmp/benchmark_results' -s 'v0.5' -d 'numba' --

--------------------------------------------------------------------------------

Repository archive version:
v0.5 [cuda|numba]

--------------------------------------------------------------------------------

Active virtual environment:
/home/ipp/tmp/benchmark_scratch/2020-07-17_18-02-38/pygl3d-bench-WDIR_2020-07-17_18-02-38/venv

--------------------------------------------------------------------------------

Python version:
[3, 8, 3, 'final', 0]

--------------------------------------------------------------------------------

pip package list:
certifi==2020.6.20
cycler==0.10.0
kiwisolver==1.2.0
llvmlite==0.31.0
matplotlib @ file:///tmp/build/80754af9/matplotlib-base_1592406092505/work
mkl-fft==1.1.0
mkl-random==1.1.1
mkl-service==2.3.0
numba==0.48.0
numpy==1.18.5
pyparsing==2.4.7
python-dateutil==2.8.1
scipy==1.4.1
sip==4.19.13
six==1.15.0
tornado==6.0.4

--------------------------------------------------------------------------------

System Parameters:

OS:
Linux 4.19.0-6-amd64 #1 SMP Debian 4.19.67-2+deb10u2 (2019-11-11) x86_64 GNU/Linux

CPU:
Architecture:        x86_64
CPU op-mode(s):      32-bit, 64-bit
Byte Order:          Little Endian
Address sizes:       39 bits physical, 48 bits virtual
CPU(s):              8
On-line CPU(s) list: 0-7
Thread(s) per core:  1
Core(s) per socket:  8
Socket(s):           1
NUMA node(s):        1
Vendor ID:           GenuineIntel
CPU family:          6
Model:               158
Model name:          Intel(R) Core(TM) i7-9700K CPU @ 3.60GHz
Stepping:            13
CPU MHz:             4738.515
CPU max MHz:         4900.0000
CPU min MHz:         800.0000
BogoMIPS:            7200.00
Virtualization:      VT-x
L1d cache:           32K
L1i cache:           32K
L2 cache:            256K
L3 cache:            12288K
NUMA node0 CPU(s):   0-7
Flags:               fpu vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush dts acpi mmx fxsr sse sse2 ss ht tm pbe syscall nx pdpe1gb rdtscp lm constant_tsc art arch_perfmon pebs bts rep_good nopl xtopology nonstop_tsc cpuid aperfmperf tsc_known_freq pni pclmulqdq dtes64 monitor ds_cpl vmx smx est tm2 ssse3 sdbg fma cx16 xtpr pdcm pcid sse4_1 sse4_2 x2apic movbe popcnt tsc_deadline_timer aes xsave avx f16c rdrand lahf_lm abm 3dnowprefetch cpuid_fault epb invpcid_single ssbd ibrs ibpb stibp ibrs_enhanced tpr_shadow vnmi flexpriority ept vpid ept_ad fsgsbase tsc_adjust bmi1 hle avx2 smep bmi2 erms invpcid mpx rdseed adx smap clflushopt intel_pt xsaveopt xsavec xgetbv1 xsaves dtherm ida arat pln pts hwp hwp_notify hwp_act_window hwp_epp md_clear flush_l1d arch_capabilities

MEMORY:
              total        used        free      shared  buff/cache   available
Mem:          32078         697       10976         338       20405       30595
Swap:         30517         293       30224

--------------------------------------------------------------------------------

Numba System INFO:

__Time Stamp__
2020-07-17 16:02:47.504935

__Hardware Information__
Machine                                       : x86_64
CPU Name                                      : skylake
Number of accessible CPU cores                : 8
Listed accessible CPUs cores                  : 0-7
CFS restrictions                              : None
CPU Features                                  : 
64bit adx aes avx avx2 bmi bmi2 clflushopt cmov cx16 f16c fma fsgsbase invpcid
lzcnt mmx movbe pclmul popcnt prfchw rdrnd rdseed sahf sgx sse sse2 sse3 sse4.1
sse4.2 ssse3 xsave xsavec xsaveopt xsaves

__OS Information__
Platform                                      : Linux-4.19.0-6-amd64-x86_64-with-glibc2.10
Release                                       : 4.19.0-6-amd64
System Name                                   : Linux
Version                                       : #1 SMP Debian 4.19.67-2+deb10u2 (2019-11-11)
Error: System name incorrectly identified or unknown.

__Python Information__
Python Compiler                               : GCC 7.3.0
Python Implementation                         : CPython
Python Version                                : 3.8.3
Python Locale                                 : en_US UTF-8

__LLVM information__
LLVM version                                  : 8.0.0

__CUDA Information__
Found 1 CUDA devices
id 0    b'GeForce RTX 2070 SUPER'                              [SUPPORTED]
                      compute capability: 7.5
                           pci device id: 0
                              pci bus id: 1
Summary:
	1/1 devices are supported
CUDA driver version                           : 10020
CUDA libraries:
Finding cublas from Conda environment
	named  libcublas.so.10.2.2.89
	trying to open library...	ok
Finding cusparse from Conda environment
	named  libcusparse.so.10.3.1.89
	trying to open library...	ok
Finding cufft from Conda environment
	named  libcufft.so.10.1.2.89
	trying to open library...	ok
Finding curand from Conda environment
	named  libcurand.so.10.1.2.89
	trying to open library...	ok
Finding nvvm from Conda environment
	named  libnvvm.so.3.3.0
	trying to open library...	ok
Finding libdevice from Conda environment
	searching for compute_20...	ok
	searching for compute_30...	ok
	searching for compute_35...	ok
	searching for compute_50...	ok

__ROC Information__
ROC available                                 : False
Error initialising ROC due to                 : No ROC toolchains found.
No HSA Agents found, encountered exception when searching:
Error at driver init: 
NUMBA_HSA_DRIVER /opt/rocm/lib/libhsa-runtime64.so is not a valid file path.  Note it must be a filepath of the .so/.dll/.dylib or the driver:

__SVML Information__
SVML state, config.USING_SVML                 : False
SVML library found and loaded                 : False
llvmlite using SVML patched LLVM              : True
SVML operational                              : False

__Threading Layer Information__
TBB Threading layer available                 : True
OpenMP Threading layer available              : True
Workqueue Threading layer available           : True

__Numba Environment Variable Information__
None set.

__Conda Information__
conda_build_version                           : 3.18.11
conda_env_version                             : 4.8.3
platform                                      : linux-64
python_version                                : 3.7.6.final.0
root_writable                                 : True

__Current Conda Env__
_libgcc_mutex             0.1                        main  
blas                      1.0                         mkl  
ca-certificates           2020.6.24                     0  
certifi                   2020.6.20                py38_0  
cudatoolkit               10.2.89              hfd86e86_1  
cycler                    0.10.0                   py38_0  
dbus                      1.13.16              hb2f20db_0  
expat                     2.2.9                he6710b0_2  
fontconfig                2.13.0               h9420a91_0  
freetype                  2.10.2               h5ab3b9f_0  
glib                      2.65.0               h3eb4bd4_0  
gst-plugins-base          1.14.0               hbbd80ab_1  
gstreamer                 1.14.0               hb31296c_0  
icu                       58.2                 he6710b0_3  
intel-openmp              2020.1                      217  
jpeg                      9b                   h024ee3a_2  
kiwisolver                1.2.0            py38hfd86e86_0  
ld_impl_linux-64          2.33.1               h53a641e_7  
libedit                   3.1.20191231         h14c3975_1  
libffi                    3.3                  he6710b0_2  
libgcc-ng                 9.1.0                hdf63c60_0  
libgfortran-ng            7.3.0                hdf63c60_0  
libpng                    1.6.37               hbc83047_0  
libstdcxx-ng              9.1.0                hdf63c60_0  
libuuid                   1.0.3                h1bed415_2  
libxcb                    1.14                 h7b6447c_0  
libxml2                   2.9.10               he19cac6_1  
llvmlite                  0.31.0           py38hd408876_0  
matplotlib                3.2.1                         0  
matplotlib-base           3.2.1            py38hef1b27d_0  
mkl                       2020.1                      217  
mkl-service               2.3.0            py38he904b0f_0  
mkl_fft                   1.1.0            py38h23d657b_0  
mkl_random                1.1.1            py38h0573a6f_0  
ncurses                   6.2                  he6710b0_1  
numba                     0.48.0           py38h0573a6f_0  
numpy                     1.18.5           py38ha1c710e_0  
numpy-base                1.18.5           py38hde5b4d6_0  
openssl                   1.1.1g               h7b6447c_0  
pcre                      8.44                 he6710b0_0  
pip                       20.1.1                   py38_1  
pyparsing                 2.4.7                      py_0  
pyqt                      5.9.2            py38h05f1152_4  
python                    3.8.3                hcff3b4d_2  
python-dateutil           2.8.1                      py_0  
qt                        5.9.7                h5867ecd_1  
readline                  8.0                  h7b6447c_0  
scipy                     1.4.1            py38h0b6359f_0  
setuptools                49.2.0                   py38_0  
sip                       4.19.13          py38he6710b0_0  
six                       1.15.0                     py_0  
sqlite                    3.32.3               h62c20be_0  
tbb                       2020.0               hfd86e86_0  
tk                        8.6.10               hbc83047_0  
tornado                   6.0.4            py38h7b6447c_1  
wheel                     0.34.2                   py38_0  
xz                        5.2.5                h7b6447c_0  
zlib                      1.2.11               h7b6447c_3  
--------------------------------------------------------------------------------

