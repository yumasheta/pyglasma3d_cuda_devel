this data is from:
pygl3d-bench_2020-07-09_21-18-19

and is used for the bachelor's thesis

the data is from a aws p3.2xlarge instance with:
- 8x 2.3 GHz (base) and 2.7 GHz (turbo) Intel Xeon E5-2686 v4 processor
- 1x NVIDIA Tesla V100 GPU 5120 CUDA Cores and 640 Tensor Cores

the benchmark ran inside a docker container