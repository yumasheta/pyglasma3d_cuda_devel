this data is from:
pygl3d-bench_2020-07-09_07-09-15

and is used for the bachelor's thesis

the data is from a aws g4dn.xlarge instance with:
- 4x 2.5 GHz Cascade Lake 2nd Generation Intel Xeon Scalable
- 1x NVIDIA T4 Tensor Core GPU

the benchmark ran inside a docker container