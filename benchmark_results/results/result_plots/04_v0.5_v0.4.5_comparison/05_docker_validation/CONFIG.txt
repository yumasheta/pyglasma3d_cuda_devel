CONFIGURATION PARAMETERS:

--------------------------------------------------------------------------------

Arguments for run_benchmark.sh:
 --output '/results' --wdir '/wdir' --env 'base' -s 'pyglasma3d_cuda_devel-v0.4.5-benchmark.tar.gz' -d 'cuda' --

--------------------------------------------------------------------------------

Repository archive version:
v0.4.5-benchmark [cuda|numba]

--------------------------------------------------------------------------------

Active virtual environment:
/opt/conda

--------------------------------------------------------------------------------

Python version:
[3, 8, 3, 'final', 0]

--------------------------------------------------------------------------------

pip package list:
brotlipy==0.7.0
certifi==2020.6.20
cffi==1.14.0
chardet==3.0.4
conda==4.8.3
conda-package-handling==1.7.0
cryptography==2.9.2
cycler==0.10.0
idna==2.9
kiwisolver==1.2.0
llvmlite==0.31.0
matplotlib @ file:///tmp/build/80754af9/matplotlib-base_1592406092505/work
mkl-fft==1.1.0
mkl-random==1.1.1
mkl-service==2.3.0
numba==0.48.0
numpy==1.18.5
pycosat==0.6.3
pycparser==2.20
pyOpenSSL==19.1.0
pyparsing==2.4.7
PySocks==1.7.1
python-dateutil==2.8.1
requests @ file:///tmp/build/80754af9/requests_1592841827918/work
ruamel-yaml==0.15.87
scipy==1.4.1
sip==4.19.13
six==1.15.0
tornado==6.0.4
tqdm==4.46.0
urllib3==1.25.9

--------------------------------------------------------------------------------

System Parameters:

OS:
Linux 5.7.7-arch1-1 #1 SMP PREEMPT Wed, 01 Jul 2020 14:53:16 +0000 x86_64 GNU/Linux

CPU:
Architecture:        x86_64
CPU op-mode(s):      32-bit, 64-bit
Byte Order:          Little Endian
CPU(s):              8
On-line CPU(s) list: 0-7
Thread(s) per core:  2
Core(s) per socket:  4
Socket(s):           1
NUMA node(s):        1
Vendor ID:           GenuineIntel
CPU family:          6
Model:               94
Model name:          Intel(R) Core(TM) i7-6700K CPU @ 4.00GHz
Stepping:            3
CPU MHz:             4700.239
CPU max MHz:         4700.0000
CPU min MHz:         800.0000
BogoMIPS:            8003.30
Virtualization:      VT-x
L1d cache:           32K
L1i cache:           32K
L2 cache:            256K
L3 cache:            8192K
NUMA node0 CPU(s):   0-7
Flags:               fpu vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush dts acpi mmx fxsr sse sse2 ss ht tm pbe syscall nx pdpe1gb rdtscp lm constant_tsc art arch_perfmon pebs bts rep_good nopl xtopology nonstop_tsc cpuid aperfmperf pni pclmulqdq dtes64 monitor ds_cpl vmx est tm2 ssse3 sdbg fma cx16 xtpr pdcm pcid sse4_1 sse4_2 x2apic movbe popcnt tsc_deadline_timer aes xsave avx f16c rdrand lahf_lm abm 3dnowprefetch cpuid_fault epb invpcid_single pti ssbd ibrs ibpb stibp tpr_shadow vnmi flexpriority ept vpid ept_ad fsgsbase tsc_adjust bmi1 hle avx2 smep bmi2 erms invpcid rtm mpx rdseed adx smap clflushopt intel_pt xsaveopt xsavec xgetbv1 xsaves dtherm ida arat pln pts hwp hwp_notify hwp_act_window hwp_epp md_clear flush_l1d

MEMORY:
              total        used        free      shared  buff/cache   available
Mem:          64261        1144       62200           8         917       62472
Swap:         10239           0       10239

--------------------------------------------------------------------------------

Numba System INFO:

__Time Stamp__
2020-07-18 00:30:24.502245

__Hardware Information__
Machine                                       : x86_64
CPU Name                                      : skylake
Number of accessible CPU cores                : 8
Listed accessible CPUs cores                  : 0-7
CFS restrictions                              : None
CPU Features                                  : 
64bit adx aes avx avx2 bmi bmi2 clflushopt cmov cx16 f16c fma fsgsbase invpcid
lzcnt mmx movbe pclmul popcnt prfchw rdrnd rdseed rtm sahf sgx sse sse2 sse3
sse4.1 sse4.2 ssse3 xsave xsavec xsaveopt xsaves

__OS Information__
Platform                                      : Linux-5.7.7-arch1-1-x86_64-with-glibc2.10
Release                                       : 5.7.7-arch1-1
System Name                                   : Linux
Version                                       : #1 SMP PREEMPT Wed, 01 Jul 2020 14:53:16 +0000
Error: System name incorrectly identified or unknown.

__Python Information__
Python Compiler                               : GCC 7.3.0
Python Implementation                         : CPython
Python Version                                : 3.8.3
Python Locale                                 : en_US UTF-8

__LLVM information__
LLVM version                                  : 8.0.0

__CUDA Information__
Found 1 CUDA devices
id 0     b'GeForce GTX 1080'                              [SUPPORTED]
                      compute capability: 6.1
                           pci device id: 0
                              pci bus id: 1
Summary:
	1/1 devices are supported
CUDA driver version                           : 10020
CUDA libraries:
Finding cublas from Conda environment
	named  libcublas.so.10.2.2.89
	trying to open library...	ok
Finding cusparse from Conda environment
	named  libcusparse.so.10.3.1.89
	trying to open library...	ok
Finding cufft from Conda environment
	named  libcufft.so.10.1.2.89
	trying to open library...	ok
Finding curand from Conda environment
	named  libcurand.so.10.1.2.89
	trying to open library...	ok
Finding nvvm from Conda environment
	named  libnvvm.so.3.3.0
	trying to open library...	ok
Finding libdevice from Conda environment
	searching for compute_20...	ok
	searching for compute_30...	ok
	searching for compute_35...	ok
	searching for compute_50...	ok

__ROC Information__
ROC available                                 : False
Error initialising ROC due to                 : No ROC toolchains found.
No HSA Agents found, encountered exception when searching:
Error at driver init: 
NUMBA_HSA_DRIVER /opt/rocm/lib/libhsa-runtime64.so is not a valid file path.  Note it must be a filepath of the .so/.dll/.dylib or the driver:

__SVML Information__
SVML state, config.USING_SVML                 : False
SVML library found and loaded                 : False
llvmlite using SVML patched LLVM              : True
SVML operational                              : False

__Threading Layer Information__
TBB Threading layer available                 : True
OpenMP Threading layer available              : True
Workqueue Threading layer available           : True

__Numba Environment Variable Information__
None set.

__Conda Information__
conda_build_version                           : not installed
conda_env_version                             : 4.8.3
platform                                      : linux-64
python_version                                : 3.8.3.final.0
root_writable                                 : True

__Current Conda Env__
_libgcc_mutex             0.1                        main  
blas                      1.0                         mkl  
brotlipy                  0.7.0           py38h7b6447c_1000  
ca-certificates           2020.6.24                     0  
certifi                   2020.6.20                py38_0  
cffi                      1.14.0           py38he30daa8_1  
chardet                   3.0.4                 py38_1003  
conda                     4.8.3                    py38_0  
conda-package-handling    1.6.1            py38h7b6447c_0  
cryptography              2.9.2            py38h1ba5d50_0  
cudatoolkit               10.2.89              hfd86e86_1  
cycler                    0.10.0                   py38_0  
dbus                      1.13.16              hb2f20db_0  
expat                     2.2.9                he6710b0_2  
fontconfig                2.13.0               h9420a91_0  
freetype                  2.10.2               h5ab3b9f_0  
glib                      2.65.0               h3eb4bd4_0  
gst-plugins-base          1.14.0               hbbd80ab_1  
gstreamer                 1.14.0               hb31296c_0  
icu                       58.2                 he6710b0_3  
idna                      2.9                        py_1  
intel-openmp              2020.1                      217  
jpeg                      9b                   h024ee3a_2  
kiwisolver                1.2.0            py38hfd86e86_0  
ld_impl_linux-64          2.33.1               h53a641e_7  
libedit                   3.1.20181209         hc058e9b_0  
libffi                    3.3                  he6710b0_1  
libgcc-ng                 9.1.0                hdf63c60_0  
libgfortran-ng            7.3.0                hdf63c60_0  
libpng                    1.6.37               hbc83047_0  
libstdcxx-ng              9.1.0                hdf63c60_0  
libuuid                   1.0.3                h1bed415_2  
libxcb                    1.14                 h7b6447c_0  
libxml2                   2.9.10               he19cac6_1  
llvmlite                  0.31.0           py38hd408876_0  
matplotlib                3.2.1                         0  
matplotlib-base           3.2.1            py38hef1b27d_0  
mkl                       2020.1                      217  
mkl-service               2.3.0            py38he904b0f_0  
mkl_fft                   1.1.0            py38h23d657b_0  
mkl_random                1.1.1            py38h0573a6f_0  
ncurses                   6.2                  he6710b0_1  
numba                     0.48.0           py38h0573a6f_0  
numpy                     1.18.5           py38ha1c710e_0  
numpy-base                1.18.5           py38hde5b4d6_0  
openssl                   1.1.1g               h7b6447c_0  
pcre                      8.44                 he6710b0_0  
pip                       20.1.1                   py38_1  
pycosat                   0.6.3            py38h7b6447c_1  
pycparser                 2.20                       py_0  
pyopenssl                 19.1.0                   py38_0  
pyparsing                 2.4.7                      py_0  
pyqt                      5.9.2            py38h05f1152_4  
pysocks                   1.7.1                    py38_0  
python                    3.8.3                hcff3b4d_0  
python-dateutil           2.8.1                      py_0  
qt                        5.9.7                h5867ecd_1  
readline                  8.0                  h7b6447c_0  
requests                  2.24.0                     py_0  
ruamel_yaml               0.15.87          py38h7b6447c_0  
scipy                     1.4.1            py38h0b6359f_0  
setuptools                47.3.1                   py38_0  
sip                       4.19.13          py38he6710b0_0  
six                       1.15.0                     py_0  
sqlite                    3.31.1               h62c20be_1  
tbb                       2020.0               hfd86e86_0  
time                      1.8                  h516909a_0    conda-forge
tk                        8.6.8                hbc83047_0  
tornado                   6.0.4            py38h7b6447c_1  
tqdm                      4.46.0                     py_0  
urllib3                   1.25.9                     py_0  
wheel                     0.34.2                   py38_0  
xz                        5.2.5                h7b6447c_0  
yaml                      0.1.7                had09818_2  
zlib                      1.2.11               h7b6447c_3  
--------------------------------------------------------------------------------

GPU NVSMI LOG:

Timestamp                           : Sat Jul 18 00:30:25 2020
Driver Version                      : 440.100
CUDA Version                        : 10.2

Attached GPUs                       : 1
GPU 00000000:01:00.0
    Product Name                    : GeForce GTX 1080
    Product Brand                   : GeForce
    Display Mode                    : Enabled
    Display Active                  : Enabled
    Persistence Mode                : Enabled
    Accounting Mode                 : Disabled
    Accounting Mode Buffer Size     : 4000
    Driver Model
        Current                     : N/A
        Pending                     : N/A
    Serial Number                   : N/A
    GPU UUID                        : GPU-c14218f7-0f34-6546-3acb-2e8c8bcba9b5
    Minor Number                    : 0
    VBIOS Version                   : 86.04.17.00.1B
    MultiGPU Board                  : No
    Board ID                        : 0x100
    GPU Part Number                 : N/A
    Inforom Version
        Image Version               : G001.0000.01.03
        OEM Object                  : 1.1
        ECC Object                  : N/A
        Power Management Object     : N/A
    GPU Operation Mode
        Current                     : N/A
        Pending                     : N/A
    GPU Virtualization Mode
        Virtualization Mode         : None
        Host VGPU Mode              : N/A
    IBMNPU
        Relaxed Ordering Mode       : N/A
    PCI
        Bus                         : 0x01
        Device                      : 0x00
        Domain                      : 0x0000
        Device Id                   : 0x1B8010DE
        Bus Id                      : 00000000:01:00.0
        Sub System Id               : 0x85931043
        GPU Link Info
            PCIe Generation
                Max                 : 3
                Current             : 1
            Link Width
                Max                 : 16x
                Current             : 8x
        Bridge Chip
            Type                    : N/A
            Firmware                : N/A
        Replays Since Reset         : 0
        Replay Number Rollovers     : 0
        Tx Throughput               : 0 KB/s
        Rx Throughput               : 0 KB/s
    Fan Speed                       : 100 %
    Performance State               : P8
    Clocks Throttle Reasons
        Idle                        : Active
        Applications Clocks Setting : Not Active
        SW Power Cap                : Not Active
        HW Slowdown                 : Not Active
            HW Thermal Slowdown     : Not Active
            HW Power Brake Slowdown : Not Active
        Sync Boost                  : Not Active
        SW Thermal Slowdown         : Not Active
        Display Clock Setting       : Not Active
    FB Memory Usage
        Total                       : 8116 MiB
        Used                        : 383 MiB
        Free                        : 7733 MiB
    BAR1 Memory Usage
        Total                       : 256 MiB
        Used                        : 6 MiB
        Free                        : 250 MiB
    Compute Mode                    : Default
    Utilization
        Gpu                         : 1 %
        Memory                      : 11 %
        Encoder                     : 0 %
        Decoder                     : 0 %
    Encoder Stats
        Active Sessions             : 0
        Average FPS                 : 0
        Average Latency             : 0
    FBC Stats
        Active Sessions             : 0
        Average FPS                 : 0
        Average Latency             : 0
    Ecc Mode
        Current                     : N/A
        Pending                     : N/A
    ECC Errors
        Volatile
            Single Bit            
                Device Memory       : N/A
                Register File       : N/A
                L1 Cache            : N/A
                L2 Cache            : N/A
                Texture Memory      : N/A
                Texture Shared      : N/A
                CBU                 : N/A
                Total               : N/A
            Double Bit            
                Device Memory       : N/A
                Register File       : N/A
                L1 Cache            : N/A
                L2 Cache            : N/A
                Texture Memory      : N/A
                Texture Shared      : N/A
                CBU                 : N/A
                Total               : N/A
        Aggregate
            Single Bit            
                Device Memory       : N/A
                Register File       : N/A
                L1 Cache            : N/A
                L2 Cache            : N/A
                Texture Memory      : N/A
                Texture Shared      : N/A
                CBU                 : N/A
                Total               : N/A
            Double Bit            
                Device Memory       : N/A
                Register File       : N/A
                L1 Cache            : N/A
                L2 Cache            : N/A
                Texture Memory      : N/A
                Texture Shared      : N/A
                CBU                 : N/A
                Total               : N/A
    Retired Pages
        Single Bit ECC              : N/A
        Double Bit ECC              : N/A
        Pending Page Blacklist      : N/A
    Temperature
        GPU Current Temp            : 34 C
        GPU Shutdown Temp           : 99 C
        GPU Slowdown Temp           : 96 C
        GPU Max Operating Temp      : N/A
        Memory Current Temp         : N/A
        Memory Max Operating Temp   : N/A
    Power Readings
        Power Management            : Supported
        Power Draw                  : 20.03 W
        Power Limit                 : 198.00 W
        Default Power Limit         : 198.00 W
        Enforced Power Limit        : 198.00 W
        Min Power Limit             : 90.00 W
        Max Power Limit             : 238.00 W
    Clocks
        Graphics                    : 139 MHz
        SM                          : 139 MHz
        Memory                      : 405 MHz
        Video                       : 544 MHz
    Applications Clocks
        Graphics                    : N/A
        Memory                      : N/A
    Default Applications Clocks
        Graphics                    : N/A
        Memory                      : N/A
    Max Clocks
        Graphics                    : 2062 MHz
        SM                          : 2062 MHz
        Memory                      : 5005 MHz
        Video                       : 1708 MHz
    Max Customer Boost Clocks
        Graphics                    : N/A
    Clock Policy
        Auto Boost                  : N/A
        Auto Boost Default          : N/A

--------------------------------------------------------------------------------

