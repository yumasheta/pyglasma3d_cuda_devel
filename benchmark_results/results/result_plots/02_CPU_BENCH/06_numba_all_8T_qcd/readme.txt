numba benchmarks for v0.4.5-benchmark
ran on qcd
with taskset limit on 8 Threads to match 6700K and 9700K in Threadcound:

CPU
8700K sustained all core boost: 4.3Ghz
      single core boost: 4.7GHz
      

data is from:
pygl3d-bench_2020-07-19_19-47-34
