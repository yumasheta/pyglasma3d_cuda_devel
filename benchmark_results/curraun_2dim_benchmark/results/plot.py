#! /usr/bin/env python

"""Simple script to plot curraun bench results in same style."""

import json
import sys

import matplotlib.pyplot as plt
from matplotlib.legend_handler import HandlerBase, HandlerPatch
from matplotlib.patches import Ellipse, Patch
from matplotlib.text import Text


class DeviceLegend(Patch):
    """Placeholder class for custom legend labels as colored dots."""

    def get_path(self):
        """Override to fix linter error."""


class DeviceLegendHandler(HandlerPatch):
    """Legend handler to use with DeviceLegend class."""

    def create_artists(
        self, legend, orig_handle, xdescent, ydescent, width, height, fontsize, trans
    ):
        """Create legend handler for device labels."""
        center = 0.5 * width - 0.5 * xdescent, 0.5 * height - 0.5 * ydescent
        p = Ellipse(
            xy=center, width=(width + xdescent) * 0.9, height=(width + xdescent) * 0.9
        )
        self.update_prop(p, orig_handle, legend)
        p.set_transform(trans)
        return [p]


class DeviceLegendTextHandler(HandlerBase):
    """Custom legend labels with text as marker."""

    def create_artists(
        self, legend, orig_handle, xdescent, ydescent, width, height, fontsize, trans
    ):
        """Create legend text handler for device labels."""
        tx = Text(
            width / 2,
            height / 2,
            orig_handle,
            fontsize=fontsize,
            ha="center",
            va="center",
        )
        return [tx]


def autolabel(rects, axis, labeloffset=+5):
    """Attach a label with value above each bar."""

    for rect in rects:
        height = rect.get_height() + rect.get_y()
        axis.annotate(
            # f"{height:.2e}" + r"$\times 10^\mu$",
            f"{height:.1e}"[:-4]
            + r"$\times 10^{"
            + str(int(f"{height:.1e}"[-3:]))
            + r"}$",
            xy=(rect.get_x() + rect.get_width() / 2, height),
            xytext=(0, labeloffset),
            textcoords="offset points",
            ha="center",
            va="bottom",
        )


# load the results data from json file
with open("curraun_results.json") as f:
    results = json.load(f)

hw_labels = tuple(
    sorted(frozenset(l["label"] for bench in results.values() for l in bench))
)

# background color, 255 steps; only use 70 - 170 range
bg_colors = plt.get_cmap("YlGnBu")
# only 7 colors for step variation
colors = plt.get_cmap("Dark2")
colors = dict((hw_label, colors(7 - i)) for i, hw_label in enumerate(hw_labels))
# device colors
color_device = {"cuda": "lime", "cython": "aqua", "numba": "tomato"}

# get different benches
bg_areas = tuple(results.keys())

# create plot
fig, (ax, plotax) = plt.subplots(nrows=2, figsize=(8, 8), sharex=True)
fig.patch.set_visible(False)
ax.set_box_aspect(1 / 2)
plotax.set_box_aspect(0.6 / 2)
plotax.set_yscale("log")

bars = list()
device_labels = list()

bar_width = list()
bg_start = None
bg_vspans = list()
# loop index for bg areas
bg_index = 0
# xpos shift accumulates
xpos_shift = 0.0

for k, (name, bench) in enumerate(results.items()):

    bar_width.append(0.8 / len(bench))

    for i, device in enumerate(bench):

        xpos = k + i * bar_width[-1] + xpos_shift

        # color background on grid size change
        if name != bg_areas[bg_index]:
            # new xpos, width might have changed
            xpos_shift += (bar_width[-1] - bar_width[-2]) / 2
            xpos = k + i * bar_width[-1] + xpos_shift
            # add previous bg area
            bg_vspans.append(
                ax.axvspan(
                    bg_start if bg_start else -0.1 - bar_width[0] / 2,
                    xpos - 0.1 - bar_width[-1] / 2,
                    alpha=1,
                    color=bg_colors(int(70 + 100 / len(bg_areas) * bg_index)),
                    fill=False,
                    zorder=-100,
                    hatch="xx",
                )
            )
            bg_start = xpos - 0.1 - bar_width[-1] / 2
            bg_index += 1

        bars.append(
            plotax.bar(
                xpos,
                device["timings"],
                bar_width[-1],
                color=colors[device["label"]],
                edgecolor="black",
                label=device["label"],
            )
        )

        # for debugging
        # ax.bar(
        #     xpos,
        #     device["timings"],
        #     width[-1],
        #     color=colors[device["label"]],
        #     edgecolor="black",
        #     label=device["label"],
        # )

        # for device color
        # first clear background
        ax.add_artist(Ellipse((xpos, 0), 1, 1, color="w", alpha=1,))
        # add colored ellipse
        ax.add_artist(
            Ellipse(
                (xpos, 0),
                1,
                1,
                facecolor=color_device[device["device"]],
                edgecolor="none",
                alpha=0.7,
            )
        )
        device_labels.append(
            ax.text(xpos, 0, s=device["device"][:2], ha="center", va="center",)
        )


# bg color last area
bg_vspans.append(
    ax.axvspan(
        bg_start,
        len(results) + xpos_shift - bar_width[-1] / 2 - 0.1,
        alpha=0.3,
        color=bg_colors(170),
        zorder=-100,
        fill=False,
        hatch="xx",
    )
)

# width of legend area on the left side
xshift = 0.42 * ax.get_xlim()[1]
ax.set_xlim(left=-0.1 - bar_width[0] / 2 - xshift)
plotax.set_xlim(left=-0.1 - bar_width[0] / 2 - xshift)

# additional height for bench labels
ylims = (0, 1)
ax.set_ylim(ylims)
yfactor = 0.2
ax.set_ylim(top=ylims[1] * (yfactor + 1))

# loop over areas to make sure the number of labels matches
for i, a in enumerate(bg_vspans):
    corners = a.get_xy()
    ax.text(
        (corners[2][0] - corners[0][0]) / 2 + corners[0][0],
        ylims[1] * (yfactor / 2 + 1),
        s=bg_areas[i],
        ha="center",
        va="center",
        color=a.get_ec(),
        bbox={"ec": a.get_ec(), "fc": "w", "boxstyle": "Round"},
        backgroundcolor="w",
    )

bbox = (
    -xshift / 2 - 0.1 - bar_width[0] / 2,
    # center between top border and bar breakdown
    ylims[1] / 2 * (1 + (yfactor + 1) * 0.2),
)
bbox_trans = ax.transData
legend_loc = "center"

# put bar height as labels
for b in bars:
    autolabel(b, plotax)

ax.set_xticks([])
ax.set_yticks([])
ax.set_ylabel("Time in Seconds")
# remove legend doubles
labels = list()
artists = list()

# add section title for hardware:
artists.append(Patch(facecolor="w"))
labels.append("Hardware:")
for i in range(len(results)):
    label = bars[i].get_label()
    if label not in labels:
        labels.append(label)
        artists.append(Patch(facecolor=colors[label]))

# in x axis units
circle_size = min(bar_width) * 0.55
# factor to account for aspect ratio and axis scale
size = ax.get_box_aspect()
fig_aspect = 1 / size
ylims = ax.get_ylim()
xlims = ax.get_xlim()
new_ylim0 = (ylims[0] / fig_aspect * (xlims[1] - xlims[0]) - circle_size * ylims[1]) / (
    (xlims[1] - xlims[0]) / fig_aspect + circle_size
) - ylims[1] * 0.055
ylims = (new_ylim0, ylims[1])
factor = (ylims[1] - ylims[0]) / (xlims[1] - xlims[0]) * fig_aspect
# extend yaxis
ax.set_ylim(bottom=ylims[0])
# get all circles
circles = [child for child in ax.get_children() if isinstance(child, Ellipse)]
# set circle locations
for c in circles:
    x, _ = c.get_center()
    c.set_center((x, -circle_size * factor / 2 - ylims[1] * 0.02))
    c.width = circle_size
    c.height = circle_size * factor
for l in device_labels:
    txt, _ = l.get_position()
    l.set_position((txt, -circle_size * factor / 2 - ylims[1] * 0.02))
# the compute devices
artists.append(Patch(facecolor="w"))
labels.append("Compute Device:")
# for key, col in color_device.items():
for key in sorted(color_device.keys()):
    # artists.append(Patch(color=color_device[key], alpha=0.7))
    artists.append(
        (
            DeviceLegend(facecolor=color_device[key], alpha=0.7, edgecolor="none"),
            key[:2],
        )
    )
    labels.append(f"{key}")

# double xaxis lines
ax.axhline(0.0, c="black", lw=0.5)
ax.axhline(-ax.get_ylim()[1] * 0.01, c="black", lw=0.5)

ax.legend(
    artists,
    # max str length == 8
    labels,
    labelspacing=1.2,
    loc=legend_loc,
    bbox_to_anchor=bbox,
    bbox_transform=bbox_trans,
    handler_map={DeviceLegend: DeviceLegendHandler(), str: DeviceLegendTextHandler()},
)

ax.set_title("Curraun Benchmark")
# plotax.axis("off")
plotax.spines['top'].set_color('none')
plotax.spines['right'].set_color('none')
plotax.spines['bottom'].set_color('none')
plotax.set_frame_on(False)
fig.tight_layout()

try:
    fig.savefig(sys.argv[1])
except IndexError:
    plt.show()
