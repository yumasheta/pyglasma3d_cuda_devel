## Benchmarks for curraun and curraun_cy

The three benchmark scripts provided here are used to benchmark the 2+1D version  
of Pyglasma3d called 'curraun'. The scripts will not work with Pyglasma3d.  
The benchmarks focus on the time for evolve steps.

[Curraun_cy](https://gitlab.com/dmueller/curraun_cy) is the Cython version that is  
required by the `benchmark_for_curraun_cy.py` script.

[Curraun](https://gitlab.com/openpixi/curraun) is the Numba version with CUDA  
acceleration that is required by the `benchmark_for_curraun.py` and `cupy_test.py`  
scripts. The latter is a test to show the improvements for moving the calculations for  
the initial conditions to the GPU.

### Results

The results are located in the `results` folder and are checked into version control  
for documentation purposes.