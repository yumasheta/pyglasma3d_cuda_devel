from __future__ import print_function

import os
import time
import datetime
import importlib
import platform

import curraun
import curraun.core as core
import numpy as np

FILENAME = "benchmark.dat"
MAX_TIMEOUT = 10
SU3_EXP_MIN_TERMS = 20  # Simulate more work (since our simulation only contains zero)

"""
    Standard parameters (can be overwritten via passing arguments)
"""

p = {
    "L": 40.0,  # 10.0,           # transverse size [fm]
    "N": 1024,  # 64,            # lattice size
    "DTS": 32,  # 2,              # time steps per transverse spacing
    "TMAX": 10.0,  # 1.0,            # max. proper time (tau) [fm/c]
    "G": 2.0,  # YM coupling constant
    "MU": 0.5,  # MV model parameter [GeV]
    "M": 0.2,  # IR regulator [GeV]
    "UV": 10.0,  # UV regulator [GeV]
    "NS": 1,  # number of color sheets
    "NE": 50,  # number of events
}

# derived parameters
a = p["L"] / p["N"]
DT = 1.0 / p["DTS"]
maxt = int(p["TMAX"] / a) * p["DTS"]

speedup_base = -1


def time_simulation(target_string, spec_string):

    np.random.seed(1)

    # initialization
    s = curraun.core.Simulation(p["N"], DT, p["G"])

    print("Memory of data: {} GB".format(s.get_ngb()))
    if True:  # not use_numba:
        # Evolve once for Just-In-Time compilation
        curraun.core.evolve_leapfrog(s)

    init_time = time.time()

    for t in range(maxt):
        curraun.core.evolve_leapfrog(s)
        total_time = time.time() - init_time
        if total_time > MAX_TIMEOUT:
            break

    total_time = time.time() - init_time
    number_of_steps = t + 1
    seconds_per_step = total_time / number_of_steps
    estimated_total_time = seconds_per_step * maxt * p["NE"]
    estimated_total_time_formatted = str(
        datetime.timedelta(seconds=estimated_total_time)
    )

    global speedup_base
    if speedup_base == -1:
        speedup_base = seconds_per_step
    speedup = speedup_base / seconds_per_step
    print("Target: {}".format(target_string))
    print("Specification: {}".format(spec_string))
    print("Number of steps calculated: {}".format(number_of_steps))
    print("Total time: {:12.6f}".format(total_time))
    print("Seconds per simulation step: {:12.6f}".format(seconds_per_step))
    print("Speedup: {:8.3f}".format(speedup))
    print("Estimated total time: {}".format(estimated_total_time_formatted))
    print("---------------------------------------")

    with open(FILENAME, "a") as myfile:
        myfile.write("{0: <14},".format(target_string))
        myfile.write("{},".format(spec_string))
        myfile.write("{:12.6f},".format(seconds_per_step))
        myfile.write("{:8.3f},".format(speedup))
        myfile.write('"{}"\n'.format(estimated_total_time_formatted))


def time_cython(spec_string):
    # os.environ["MY_NUMBA_TARGET"] = "python"
    # time_simulation("Python", spec_string)

    time_simulation("Cython benchmark", spec_string)


current_date = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
hostname = platform.node()

print("---------------------------------------")
print("Date: {}".format(current_date))
print("Hostname: {}".format(hostname))

with open(FILENAME, "a") as myfile:
    myfile.write("---------------------------------------\n")
    myfile.write("Date: {}\n".format(current_date))
    myfile.write("Hostname: {}\n".format(hostname))
    myfile.write("---------------------------------------\n")

time_cython("cython benchmark")
