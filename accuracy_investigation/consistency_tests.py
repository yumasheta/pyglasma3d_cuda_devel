#!/usr/bin/env python

"""A python script to plot and test difference data.

This python script is meant to be executed from the command line, either
directly `./consistency_tests.py` or via `python consistency_tests.py`.
For available command line options use the '-h' or '--help' flags.

The command line argument '-f' / '--files' is required and takes two
paths to the data files to analyze as string arguments.

The differences are calculated using the `evaluation.py` module.
"""

import math
import os
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import matplotlib.ticker as ticker
import matplotlib.animation as animation

from cycler import cycler

if __name__ != "__main__":
    raise ImportError(
        "\nWARNING!\nThis `test_consistency.py` script "
        "cannot be imported or called as a secondary module.\n",
        name="consistency_tests.py",
    )

try:
    import evaluation

except ModuleNotFoundError as e:
    raise ModuleNotFoundError(
        f'\nThe module "{e.name}" could not be found in '
        f'the current working directory:\n"{os.getcwd()}"\n'
        "This could be, because this script was called from "
        "the wrong working directory.\nPlease make sure to be "
        'located in the base folder of the "test_consistency" '
        "package.\n",
        name=e.name,
        path=e.path,
    )


# fixed sequence of files to compare

# no fastmath vs original
# nf_og = (
#     ("mv_simple_og_cy_txt.dat", "mv_simple_py3_port_txt.dat"),
#     ("mv_simple_og_cy_txt.dat", "mv_simple_cy_edit_no_swapping.dat"),
#     ("mv_simple_og_cy_txt.dat", "mv_simple_cy_edit_nf_s3-2.dat"),
#     ("mv_simple_og_cy_txt.dat", "mv_simple_cy_edit_nf_s3-3.dat"),
#     ("mv_simple_og_cy_txt.dat", "mv_simple_numba_no_fastmath_txt.dat"),
#     ("mv_simple_og_cy_txt.dat", "mv_simple_cuda_no_fastmath_txt.dat"),
# )

# names = (
#     "Py3 Port",
#     "Cy Edit (no swapping)",
#     "Cy Edit (lattice swap)",
#     "Cy Edit (solver swap)",
#     "Numba (final)",
#     "Cuda (final)",
# )

# with fastmath vs original, only start where fastmath has effect
# nf_og = (
#     ("mv_simple_og_cy_txt.dat", "mv_simple_cy_edit_no_swapping.dat"),
#     ("mv_simple_og_cy_txt.dat", "mv_simple_cy_edit_s3-2.dat"),
#     ("mv_simple_og_cy_txt.dat", "mv_simple_cy_edit_s3-3.dat"),
#     ("mv_simple_og_cy_txt.dat", "mv_simple_numba_fastmath_txt.dat"),
#     ("mv_simple_og_cy_txt.dat", "mv_simple_cuda_fastmath_txt.dat"),
# )

# names = (
#     "Cy Edit (no swapping)",
#     "Cy Edit (lattice swap)",
#     "Cy Edit (solver swap)",
#     "Numba (final)",
#     "Cuda (final)",
# )

# run of new code on py2
# nf_og = (
#     ("mv_simple_og_cy_txt.dat", "mv_simple_py3_port_on_py2_txt.dat"),
#     ("mv_simple_og_cy_txt.dat", "mv_simple_py3_port_txt.dat"),
#     ("mv_simple_og_cy_txt.dat", "mv_simple_py3_port_on_py2_s2-1.dat"),
#     ("mv_simple_og_cy_txt.dat", "mv_simple_py3_port_s2-1_txt.dat"),
# )

# names = (
#     "Py3 Port on Py2",
#     "Py3 Port",
#     "Cy Edit (no swapping) on Py2",
#     "Cy Edit (no swapping)",
# )

# numba and cuda vs og with and without fastmath
# nf_og = (
#     ("mv_simple_og_cy_txt.dat", "mv_simple_numba_fastmath_txt.dat"),
#     ("mv_simple_og_cy_txt.dat", "mv_simple_cuda_fastmath_txt.dat"),
#     ("mv_simple_og_cy_txt.dat", "mv_simple_numba_no_fastmath_txt.dat"),
#     ("mv_simple_og_cy_txt.dat", "mv_simple_cuda_no_fastmath_txt.dat"),
# )

# names = (
#     "Numba",
#     "Cuda",
#     "Numba no fastmath",
#     "Cuda no fastmath",
# )

# numba vs cuda and without fastmath
# nf_og = (
#     ("mv_simple_cuda_fastmath_txt.dat", "mv_simple_numba_fastmath_txt.dat"),
#     ("mv_simple_cuda_no_fastmath_txt.dat", "mv_simple_numba_no_fastmath_txt.dat"),
# )

# names = (
#     "Numba vs Cuda",
#     "Numba vs Cuda no fastmath",
# )

# print / plot data

fig = plt.figure(figsize=(20, 12))
gs = gridspec.GridSpec(nrows=2, ncols=2, hspace=1 / 3)
plt_time = fig.add_subplot(gs[0, :])
plt_hist = fig.add_subplot(gs[1, 0])
plt_hist.set_prop_cycle(cycler(color="bgrcmyk"))
plt_scat = fig.add_subplot(gs[1, 1])


def draw_plots(l):
    count, (og, dat) = l
    # build a dict with filenames and paths for the supplied files
    files_dict = {"source1": og, "source2": dat}

    # calculate data from evaluation
    all_results, hist, scatter = evaluation.diff_statistics(files_dict)

    # plot diff for each sim step

    plt_time.plot(
        [all_results[i][0] for i in range(len(all_results))], label="mean", color="b",
    )
    # plt_time.plot([all_results[i][1]
    # for i in range(len(all_results))], label='std')
    plt_time.plot(
        [all_results[i][2] for i in range(len(all_results))], label="min", color="y"
    )
    plt_time.fill_between(
        list(range(len(all_results))),
        y1=[all_results[i][0] for i in range(len(all_results))],
        y2=[all_results[i][0] + all_results[i][1] for i in range(len(all_results))],
        label="mean - \u03C3",
        alpha=0.5,
        color="#1f77b4",
    )
    plt_time.axhline(
        float(f"1e-15"), ls="--", lw=2, c="m", label=f"limit: 15",
    )
    plt_time.set_yscale("log")
    plt_time.grid(which="both", axis="y")
    plt_time.set_xlim(0, len(all_results) - 1)
    plt_time.set_ylim(1e3, 1e-18)
    plt_time.set_xlabel("Simulation Step")
    plt_time.set_ylabel("Matching Digits")
    # calculate ticks for fixed number of ticks
    tickstep = math.ceil(len(all_results) / 50)
    xticks = list(range(0, len(all_results) - 1, tickstep))
    xticks.append(len(all_results) - 1)
    xticklabels = list(range(1, len(all_results), tickstep))
    xticklabels.append(len(all_results))
    yticks = plt_time.get_yticks()
    yticklabels = [f"{-1 * math.log10(x) if x !=1.0 else 0:.0f}" for x in yticks]
    plt_time.set_yticklabels(yticklabels)
    plt_time.set_xticks(xticks)
    plt_time.set_xticklabels(xticklabels)
    handles, labels = plt_time.get_legend_handles_labels()
    order = [0, 3, 1, 2]
    n_handles, n_labels = list(), list()
    for h, l in zip(handles, labels):
        if l not in n_labels:
            n_labels.append(l)
            n_handles.append(h)
    plt_time.legend(
        [n_handles[i] for i in order], [n_labels[i] for i in order], loc="lower right"
    )
    plt_time.set_title("Matching Digits for each Simulation Step")
    plt_time.grid(b=True, axis="y", ls="--")

    # plot histogram of diff values

    _, _, patches = plt_hist.hist(
        range(len(hist[0])),
        bins=range(0, len(hist[1])),
        weights=hist[0],
        histtype="step",
        label=f"{count}: {names[count-1]}",
        alpha=0.5,
        lw=4,
    )
    count += 1
    plt_hist.axvline(19 - 15, ls="--", lw=2, c="m")
    plt_hist.set_xlim(0, len(hist[0]))
    plt_hist.set_xticks(range(len(hist[1])))
    # set xticklabels
    xticklabels = [f"{-1 * math.log10(x) if x !=1.0 else 0:.0f}" for x in hist[1][1:]]
    xticklabels.insert(0, "all")
    plt_hist.set_xticklabels(xticklabels)
    plt_hist.set_xlabel("Matching Digits")
    plt_hist.set_title("Histogram of Matching Digits")
    plt_hist.legend()
    plt_hist.grid(b=True, axis="y", ls="--")

    # scatter plot diff values and data magnitudes

    plt_scat.scatter(
        scatter[0], scatter[1], marker="2", alpha=0.5, color="#1f77b4",
    )
    plt_scat.loglog()
    plt_scat.axhline(float(f"1e-15"), ls="--", lw=2, c="m")
    plt_scat.set_ylim(1e-18, 1e3)
    max_mag = max(scatter[0])
    min_mag = min(x if x != 0.0 else max_mag for x in scatter[0]) / 10
    plt_scat.set_xlim(min_mag)
    # calculate ticks for fixed number of ticks
    mag_diff = int(math.log10(max_mag) - math.log10(min_mag))
    tickstep = math.ceil(mag_diff / 10)
    xticklabels = [
        math.pow(10, x)
        for x in range(int(math.log10(min_mag)), int(math.log10(max_mag)), tickstep)
    ]
    xticklabels.append(math.pow(10, max_mag))
    plt_scat.set_xticks(xticklabels)
    plt_scat.set_xticklabels(xticklabels)
    plt_scat.xaxis.set_major_formatter(ticker.FormatStrFormatter("%.0e"))
    yticks = plt_scat.get_yticks()
    yticklabels = [f"{-1 * math.log10(x) if x != 1.0 else 0:.0f}" for x in yticks]
    plt_scat.set_yticklabels(yticklabels)
    plt_scat.set_xlabel("Magnitudes of Data Values")
    plt_scat.set_ylabel("Matching Digits")
    plt_scat.set_title("Matching Digits vs Data Value Magnitudes")
    plt_scat.grid(b=True, axis="y", ls="--")


ani = animation.FuncAnimation(
    fig,
    draw_plots,
    frames=enumerate(nf_og, 1),
    interval=1500,
    save_count=len(nf_og),
    repeat=False,
    init_func=lambda: None,
)

# ani.save("numba_vs_cuda.gif", writer="imagemagick")
plt.show()
