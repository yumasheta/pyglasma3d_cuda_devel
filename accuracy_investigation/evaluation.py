"""Calculate difference values of data files.

A module to calculate differences of two supplied data sets and express
the differences in terms of the digits. The magnitudes of the difference
values represent the number of digits the data sets match.

includes:
function diff_statistics(): calculate value differences of input data
generator function read_textfile_iter(): iterate over a given file and
                                         return a numpy array with data
"""

import sys
import numpy as np


def read_textfile_iter(file):
    """Iterate over a text file containing numerical data.

    Iterate over the numerical data of the supplied `file` in specified
    chunk sizes and return a 1D `np.ndarray` with all the values from
    the current iter step.
    The data file has to have the number of lines to read per iteration
    as the only value on its first line and the number of total
    iterations as the only value on its second line.
    The data values on each line have to be separated by whitespaces.
    There have to be 5 values per line.

    :param str file: Path to the data file.
    :return: A 1D numpy array with all values for one iteration step.
    """
    try:
        with open(file) as f:
            # read header data for sim size and iter count
            nl = int(f.readline())
            mt = int(f.readline())

            # loop as many times as there were iterations calculated
            for _ in range(mt):

                data = np.loadtxt(f, dtype=np.float64, max_rows=nl).ravel()

                # check if correct number of lines is loaded
                if data.size != 5 * nl:
                    sys.exit(
                        f"\nERROR {__file__}:\nThe iteration count and number of lines "
                        "per iteration do not match with the number of values given in "
                        f"{file}!"
                    )

                yield data

    except ValueError as e:
        sys.exit(f'\n\nan error occurred while reading the file:\t"{file}"\n{e}\n\n')

    except OSError as e:
        sys.exit(
            f'\n\ncannot open the supplied file:\n"{e.filename}"\n'
            "make sure the path is valid!\n\n"
        )


def diff_statistics(files_dict, skip_scatter=False):
    """Calculate differences on supplied input files.

    The differences of the supplied data files are calculated by simple
    subtraction. Thereafter they differences are normed according to the
    magnitudes of the input data. The difference values thus can be
    interpreted as differences in digits rather than absolute value
    differences.
    For the calculations the `read_textfile_iter()` function is used.

    3 different statistics on the differences are calculated:
    i) for each iteration on the input files the mean, std and max
       of the value differences are appended the numpy array `results`
    ii) for each iteration on the input files a histogram of the
        differences is calculated and the frequency values are added up
    iii) if `skip_scatter` is set to False, a 2D numpy array is built
         that contains all magnitudes in its first index [0] and all
         difference values for the data in its second index 1.


    :param dict files_dict: A dictionary containing the names and paths
                            to the files to compare:
                            ``files_dict = { "source1": path1,
                                             "source2": path2 }``
                            The keys "source1" and "source2" are
                            required.
    :param bool skip_scatter: If set to True, the third statistical
                              evaluation (see iii)) is skipped. Use this
                              if dealing with large amount of values per
                              iteration step. This will prevent storing
                              all difference and data values in memory.
    :return: A Tuple with:
             its first element as a 2D numpy array with the values
             calculated in i)
             its second element as a tuple with the histogram calculated
             in ii) and a list of the bin edges [in that order]
             its third element as the 2D numpy array built in iii)
    """

    # collect mean, max, std
    results = np.zeros((0, 3))

    if skip_scatter:
        scatter = None
    else:
        # pairs of data1 and diff
        scatter = np.zeros((2, 0))

    # histogram for each sim step
    # bins edges are:
    # [0, 1e-18, 1e-17, 1e-16, 1e-15, 1e-14, 1e-13, 1e-12, 1e-11, 1e-10,
    # 1e-09, 1e-08, 1e-07, 1e-06, 1e-05, 1e-4, 1e-3, 1e-2, 1e-1, 1,
    # 1e+1, 1e+3]
    bins = [pow(10, x) for x in range(-18, 2)]
    bins.insert(0, 0)
    bins.append(1e3)
    hist = np.zeros(len(bins) - 1, dtype=np.int)

    try:

        # get the filepaths from the source dict
        file1 = files_dict["source1"]
        file2 = files_dict["source2"]

        # iterate over data file in chunks and and work on the data
        for data1, data2 in zip(read_textfile_iter(file1), read_textfile_iter(file2)):

            # calculate differences
            diff = data2 - data1
            # mask data1 where it is 0
            data1 = np.ma.masked_equal(data1, 0, copy=False)

            # change data1 values inplace to their magnitudes
            # ( 1e+-(10*x) ) as pure powers of 10
            # due to bug: pylint: disable=no-member
            data1 = np.log10(np.abs(data1, out=data1), where=~data1.mask)
            # pylint: enable=no-member
            np.power(10, np.floor(data1, out=data1), out=data1)
            # this version has a bug with the np.log10() function
            # np.power(10, np.floor(np.log10(np.abs(data1, out=data1),
            # out=data1, where=~data1.mask), out=data1), out=data1)

            # normalize diff by the magnitude of data1, except where
            # data1 is masked (where it is 0)
            diff = diff / data1
            # remove diff's mask it has inherited from data1
            diff.mask = np.ma.nomask
            # make data2 a masked array
            data2 = np.ma.asarray(data2)
            # mask data2 where data1 is not zero
            data2.mask = ~data1.mask
            # mask data2 where it is 0 as well
            data2 = np.ma.masked_equal(data2, 0, copy=False)

            # change data2 values inplace to their magnitudes
            # ( 1e+-(10*x) ) as pure powers of 10
            # due to bug: pylint: disable=no-member
            data2 = np.log10(np.abs(data2, out=data2), where=~data2.mask)
            # pylint: enable=no-member
            np.power(10, np.floor(data2, out=data2), out=data2)
            # this version has a bug with the np.log10() function
            # np.power(10, np.floor(np.log10(np.abs(data2, out=data2),
            # out=data2, where=~data2.mask), out=data2), out=data2)

            # normalize diff by the magnitude of data2, except where
            # data2 is masked (where it is 0 or data1 was not 0)
            diff = diff / data2
            # remove diff's mask it has inherited from data1
            diff.mask = False
            # now take the absolute value of diff
            np.abs(diff, out=diff)

            # calculate statistics and append to results array
            results = np.append(
                results, ((np.mean(diff), np.std(diff), np.amax(diff)),), axis=0
            )

            # sort diff array into bins and add up bin population
            new_hist, _ = np.histogram(diff, bins=bins)
            hist += new_hist

            if diff.size != np.sum(new_hist):
                print(
                    "\nWARNING:\tsome values are outside of the bin edges and "
                    "were discarded during the binning process\n"
                )

            if scatter is not None:
                # set data1 to 0.0 where masked
                # necessary because of a bug in np.log10 where
                # all masked values are set to 1.0
                data1[data1.mask] = 0.0
                data1.mask = False
                for d, v in zip(diff, data1):
                    scatter = np.append(scatter, ((v,), (d,)), axis=1)

    except KeyError as e:
        sys.exit(f"supplied files_dict misses required key: {e}\naborting...")

    return results, (hist, bins), scatter
