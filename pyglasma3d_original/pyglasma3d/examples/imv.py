from __future__ import print_function

import numpy as np
import time

import pyglasma3d.cy.mv as mv
import pyglasma3d.cy.gauss as gauss

import pyglasma3d.cy.interpolate as interpolate
from pyglasma3d.core import Simulation, Observables

"""
    A script for Au-Au collisions in the MV-model similar to what we did in our last publication, but with
    incoherent longitudinal structure.
    The main input parameters are the collision energy \sqrt{s}, the infrared regulator m and the
    box dimensions (LT, LL).
"""

run = 'imv'    # data will end up in `./output/T_<run>.dat`

# grid size (make sure that ny == nz)
nx, ny, nz = 512, 64, 64

# transverse and longitudinal box widths [fm]
LT = 0.5
LL = 1.5

# collision energy [MeV]
sqrts = 200.0 * 1000.0

# infrared and ultraviolet regulator [MeV]
m = 400.0
uv = 20.0 * 1000.0

# longitudinal coherence length parameter (kappa = 1.0 for 2D MV, kappa ~ 0.1 for realistic structure)
kappa = 1.0

# ratio between dt and aL [int, multiple of 2]
steps = 2

"""
    The rest of the parameters are computed automatically.
"""

# constants
hbarc   = 197.3270      # hbarc [MeV*fm]
RAu     = 7.27331       # Gold nuclear radius [fm]

# determine lattice spacings and energy units
aT_fm   = LT / ny
E0      = hbarc / aT_fm
aT      = 1.0
aL_fm   = LL / nx
aL      = aL_fm / aT_fm
a       = [aL, aT, aT]
dt      = aL / steps

# determine initial condition parameters
gamma   = sqrts / 2000.0
Qs      = np.sqrt((sqrts/1000.0) ** 0.25) * 1000.0
alphas  = 12.5664 / (18.0 * np.log(Qs / 217.0))
g       = np.sqrt(12.5664 * alphas)
mu      = Qs / (g * g * 0.75) / E0
uvt     = uv / E0
ir      = m / E0
sigma   = RAu / (2.0 * gamma) / aL_fm * aL
sigma_c = sigma / aL
clen    = sigma * kappa
clen_c  = clen / aL

# actual correlation length
V = 4.0 * np.pi / 3.0 * RAu ** 3.0
N = 197
V_nucleon = V / N
R_nucleon = pow(3.0 / (4.0 * np.pi) * V_nucleon, 1.0/3.0)
clen_actual = R_nucleon / (2.0 * gamma) / aL_fm * aL
clen_ration_actual = clen_actual / sigma

# number of evolution steps (max_iters) and output file path
max_iters = int(nx * 2.0/3.0)
file_path = './output/T_' + run + '.dat'

"""
    Initialization
"""

# initialize simulation object
dims = [nx, ny, nz]
s = Simulation(dims=dims, iter_dims=[1, nx-1, 0, ny, 0, nz], a=a, dt=dt, g=g)
s.debug = True

t = time.time()
v = mv.initialize_mv_inc(s, x0=nx * 0.25 * aL, mu=mu, sigma=sigma, kappa=kappa, mass=ir, uvt=uvt, orientation=+1)
interpolate.initialize_charge_planes(s, +1, 2, nx / 2)

v = mv.initialize_mv(s, x0=nx * 0.75 * aL, mu=mu, sigma=sigma, mass=ir, uvt=uvt, orientation=-1)
interpolate.initialize_charge_planes(s, -1, nx / 2, nx - 2)
print("Initial conditions complete: ", round(time.time()-t, 3))
s.init()

"""
    Simulation loop
"""

import matplotlib.pyplot as plt
plt.ion()

t = time.time()
for it in range(max_iters):
    # this for loop moves the nuclei exactly one grid cell
    for step in range(steps):

        print("Gauss constraint squared", gauss.gauss_sq_total(s))
        EL, BL, ET, BT, SL = Observables.energy_momentum_tensor(s)
        plt.clf()
        plt.axis([0, dims[0], 0, 1])
        plt.autoscale(True, axis='y')
        xaxis = np.arange(1, dims[0] - 1)
        plt.plot(xaxis, EL, c='red')
        plt.plot(xaxis, BL, c='pink')
        plt.plot(xaxis, ET, c='blue')
        plt.plot(xaxis, BT, c='cyan')
        plt.yscale('log')
        plt.axes().set_ylim([10 ** (-13), 10 ** -1])
        print("evolution + plotting", time.time() - t, "s")
        plt.grid()
        plt.pause(0.000000001)

        t = time.time()
        s.evolve()
        print(s.t, "Complete cycle", round(time.time()-t, 3))

    t = time.time()
    #s.write_energy_momentum(max_iters, file_path)
    #print(s.t, "Writing energy momentum tensor to file.", round(time.time()-t, 3))