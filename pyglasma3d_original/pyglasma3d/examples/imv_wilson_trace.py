from __future__ import print_function

import matplotlib.pyplot as plt
import numpy as np

import pyglasma3d.cy.mv as mv
from pyglasma3d.core import Simulation

nl = 64
nt = 192
dims = [nl, nt, nt]
iter_dims = [1, nl-1, 0, nt, 0, nt]
a = [0.125, 1.0, 1.0]
dt = a[0]/8
g = 2.0

print("creating simulation object")
s = Simulation(dims, iter_dims, a, dt, g)
print("size of simulation object:", s.get_nbytes() / 1024.0 ** 3, "gb")

# number of events to average over per plot point
num_events = 3

# mv model parameter
mu = 0.237944
Qs = g ** 2.0 * mu
# ir regulator scan range
min_m = 0.00000000001 * Qs
max_m = 0.4 * Qs
m_step = (max_m - min_m) / 4.0
m_range = np.linspace(min_m, max_m, 5)

# make plot interactive, so that we can update as the simulation runs
plt.ion()

# setup arrays for numerical results
trv_m = []
trv_avg = []
trv_std = []

for m in m_range:
    trv_events = np.array([], dtype=np.double)
    for it in range(num_events):
        # set all fields to zero
        s.reset()
        # initialize mv model, wilson line as return variable
        #np.random.seed(1)
        v1 = mv.initialize_mv_inc(s, x0=dims[0] * 0.5 * a[0], mu=mu, sigma=4.0 * a[0], clen=0.0 * a[0], mass=m, uvt=np.pi, orientation=+1)
        #v1 = mv.initialize_mv(s, x0=dims[0]*0.5, mu=mu, sigma=4.0, mass=m, uvt=np.pi, orientation=+1)

        #EL, BL, ET, BT, SL = Observables.compute_projected_energy_density(s)
        plt.clf()
        #plt.plot(EL)
        #plt.plot(BL)
        #plt.plot(ET)
        #plt.plot(BT)


        # take trace divided by number of colors (two)
        trv1 = v1[0:-1:4].reshape((nl, nt, nt))

        # compute average value across transverse plane behind nucleus
        trv_events = np.append(trv_events, np.mean(trv1[0]))

    # average events ...
    trv_m.append(m)
    trv_avg.append(np.mean(trv_events))
    trv_std.append(np.std(trv_events))


    # .. and plot the result

    plt.clf()
    plt.rc('text', usetex=True)
    plt.rc('font', family='serif', )
    plt.title(r"$tr(V)$ expectation value")
    plt.xlabel(r"$m$")
    plt.ylabel(r"$tr(V)/2$")
    plt.axis([min_m, max_m, -0.6, 1.0])
    plt.errorbar(trv_m, trv_avg, yerr=trv_std, fmt='o')

    # plot analytical result

    m_rng = np.linspace(min_m, max_m, 100)
    m_sq = np.power(m_rng, 2)
    l0 = np.ones(100) / (4.0 * np.pi * m_sq)
    trv_ana = np.exp(- 3.0 * (l0 * Qs ** 2) / 8.0)
    plt.plot(m_rng, trv_ana)

    # plot analytical result (coherent)
    trv_ana = (1.0 - (l0 * Qs ** 2)/4.0) * np.exp(- (l0 * Qs ** 2)/8.0)
    plt.plot(m_rng, trv_ana)

    plt.pause(0.001)
plt.ioff()
plt.show()
