from __future__ import print_function

import numpy
import time

from pyglasma3d.core import Simulation
from pyglasma3d.cy import gauss

n = 64
dims = [n, n, n]
iter_dims = [0, n, 0, n, 0, n]
a = [1.0, 1.0, 1.0]
dt = 0.5
g = 2.0

print("creating simulation object")
s = Simulation(dims, iter_dims, a, dt, g)
print("size of simulation object:", s.get_nbytes() / 1024.0 ** 3, "gb")

# random magnetic field as initial condition
numpy.random.seed(1337)
amp = 0.1
s.e = numpy.array(numpy.random.uniform(-amp, amp, 3*3*s.N), dtype=numpy.double)/dt * g
s.init()
s.e = numpy.zeros(3*3*s.N, dtype=numpy.double)



# evolve system a few steps
print("start evolution")

max_it = 10000

#plt.ion()
#plt.axis([0, dims[0], 0, amp ** 2 * 2])
#xaxis = numpy.arange(0, dims[0])

for it in range(0, max_it):
    # evolve system
    t = time.time()
    s.evolve()
    diff = round(time.time() - t, 4)
    #print("step", it+1, "/", max_it, ",", diff, "s per update")

    # compute projected energy density components and gauss constraint
    t = time.time()
    #EL, BL, ET, BT = pyglasma3d.Observables.compute_projected_energy_density(s)
    if it % 100 == 0:

        # check unitarity
        unt = numpy.zeros(s.N*3, dtype=numpy.double)
        for i in range(4):
            unt += numpy.power(s.u0[i:s.N*3*4:4], 2)

        g_value = gauss.gauss_sq_total(s)
        #print("ENERGY", numpy.sum(EL + BL + ET + BT))
        print(it, g_value, numpy.max(unt), numpy.min(unt))
        diff = round(time.time() - t, 4)
        #print("Observable calculation:", diff, "s")
        t = time.time()

        # plot projected energy density components
        #plt.pause(0.001)
        #plt.clf()
        #plt.axis([0, dims[0], 0, amp ** 2 * 4])
        #plt.scatter(xaxis, EL, c='red')
        #plt.scatter(xaxis, BL, c='green')
        #plt.scatter(xaxis, ET, c='blue')
        #plt.scatter(xaxis, BT, c='cyan')
        #diff = round(time.time() - t, 4)

