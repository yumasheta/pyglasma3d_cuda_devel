from __future__ import print_function

import numpy as np
import time

import pyglasma3d.cy.mv as mv
import pyglasma3d.cy.interpolate as interpolate
import pyglasma3d.utils.data_io as data_io
import pyglasma3d.cy.gauss as gauss

from pyglasma3d.core import Simulation

"""
    this setup uses the same options as the mv_simple script
    but it does not print an output file
    it only prints the information relevant for the gauss constraint and gauss violation
    to the terminal
    
    to get a file from the output use:
    python3 -m pyglasma3d.examples.mv_simple_gauss > output/mv_simple_gauss_txt.dat
"""

run = 'mv_trial_simple_gauss'    # data will end up in `./output/T_<run>.dat`


# grid size (make sure that ny == nz)
nx, ny, nz = 128, 32, 32

# transverse and longitudinal box widths [fm]
LT = 6.0
LL = 6.0

# collision energy [MeV]
sqrts = 200.0 * 1000.0

# infrared and ultraviolet regulator [MeV]
m = 200.0
uv = 10.0 * 1000.0

# ratio between dt and aL [int, multiple of 2]
steps = 4

# option for debug
debug = True

"""
    The rest of the parameters are computed automatically.
"""

# constants
hbarc   = 197.3270      # hbarc [MeV*fm]
RAu     = 7.27331       # Gold nuclear radius [fm]

# determine lattice spacings and energy units
aT_fm   = LT / ny
E0      = hbarc / aT_fm
aT      = 1.0
aL_fm   = LL / nx
aL      = aL_fm / aT_fm
a       = [aL, aT, aT]
dt      = aL / steps

# determine initial condition parameters
gamma   = sqrts / 2000.0
Qs      = np.sqrt((sqrts/1000.0) ** 0.25) * 1000.0
alphas  = 12.5664 / (18.0 * np.log(Qs / 217.0))
g       = np.sqrt(12.5664 * alphas)
mu      = Qs / (g * g * 0.75) / E0
uvt     = uv / E0
ir      = m / E0
sigma   = RAu / (2.0 * gamma) / aL_fm * aL
sigma_c = sigma / aL

# number of evolution steps (max_iters) and output file path
max_iters = 1 * nx // 3
file_path = './output/T_' + run + '.dat'

"""
    Initialization
"""

# changed from the original version
# set random seed for run to run comparability
np.random.seed(10)

# initialize simulation object
dims = [nx, ny, nz]
s = Simulation(dims=dims, a=a, dt=dt, g=g, iter_dims=[1, nx-1, 0, ny, 0, nz])
s.debug = debug

t = time.time()
s.log("Initializing left nucleus.")
v = mv.initialize_mv(s, x0=nx * 0.25 * aL, mu=mu, sigma=sigma, mass=ir, uvt=uvt, orientation=+1)
interpolate.initialize_charge_planes(s, +1, 0, nx // 2)
#s.log("Initialized left nucleus.", round(time.time()-t, 3))

t = time.time()
s.log("Initializing right nucleus.")
v = mv.initialize_mv(s, x0=nx * 0.75 * aL, mu=mu, sigma=sigma, mass=ir, uvt=uvt, orientation=-1)
interpolate.initialize_charge_planes(s, -1, nx // 2, nx)
#s.log("Initialized right nucleus.", round(time.time()-t, 3))

s.init()


"""
    Simulation loop
"""

t = time.time()
for it in range(max_iters):
    # this for loop moves the nuclei exactly one grid cell
    for step in range(steps):
        # t = time.time()
        s.evolve()
        # print(s.t, "Complete cycle.", round(time.time()-t, 3))

    print(s.t)
    # t = time.time()
    print("Gauss constraint squared", gauss.gauss_sq_total(s))
    print("Gauss violation", gauss.rel_gauss_violation(s))
    # print("timedelta: ", round(time.time()-t, 3))

    # t = time.time()

    #s.write_energy_momentum(max_iters, file_path)
    #print(s.t, "Writing energy momentum tensor to file.", round(time.time()-t, 3))


"""
    convert binary output file to readable txt
"""

#data_io.write('./output/T_' + run + '_textfile.dat', **data_io.read(file_path))

