#!/usr/bin/python

from __future__ import print_function

import matplotlib.pyplot as plt
import numpy
import sys
import time

import pyglasma3d.cy.gauss as gauss
import pyglasma3d.cy.mv as mv

import pyglasma3d.cy.interpolate as interpolate
from pyglasma3d.core import Observables
from pyglasma3d.core import Simulation

# simulation parameters
nx, ny, nz = 128*5, 32, 32
dims = [nx, ny, nz]
iter_dims = [2, nx-2, 0, ny, 0, nz]
a = [0.5, 1.0, 1.0]
dt = 0.25
g = 2.0
numpy.random.seed(1446)
if len(sys.argv) > 1:
    file_path = './data/test' + sys.argv[1] + '.dat'
else:
    file_path = './data/test.dat'

DEBUG = True

# start of simulation
print("creating simulation object")
s = Simulation(dims, iter_dims, a, dt, g)
print("size of simulation object:", round(s.get_nbytes() / 1024.0 ** 3, 4), "gb")


t = time.time()
# initialize mv model, wilson line as return variable
t = time.time()
v1 = mv.initialize_mv(s, x0=dims[0] * 0.25 * a[0], mu=0.04, sigma=4.0, mass=0.1, uvt=1.0, orientation=+1)
interpolate.initialize_charge_planes(s, +1, 1, dims[0] / 2)
print("Initialization of first nucleus", round(time.time() - t, 3), "s")
t = time.time()
v2 = mv.initialize_mv(s, x0=dims[0] * 0.75 * a[0], mu=0.04, sigma=4.0, mass=0.1, uvt=1.0, orientation=-1)
interpolate.initialize_charge_planes(s, -1, dims[0] / 2, dims[0] - 1)
print("Initialization of second nucleus", round(time.time() - t, 3), "s")
t = time.time()
interpolate.interpolate_to_charge_density(s)
print("size of simulation object:", round(s.get_nbytes() / 1024.0 ** 3, 4), "gb")

# run before starting evolution, after initial conditions have been applied
s.init()

# initialize interactive plot
plt.ion()
t = time.time()
iters = int(dims[0] * a[0] / dt)

for it in range(iters):
    # evolve
    s.evolve()
    print("iteration " + str(it) + "/" + str(iters) + " : " + str(round(time.time() - t, 3)) + " s")
    # compute projected energy densities
    if DEBUG and it % 10 == 0:
        print("Gauss constraint squared", gauss.gauss_sq_total(s))
        GS = gauss.compute_projected_gauss_density_sq(s)
        EL, BL, ET, BT, SL = Observables.energy_momentum_tensor(s)
        plt.clf()
        plt.axis([0, dims[0], 0, 1])
        plt.autoscale(True, axis='y')
        xaxis = numpy.arange(1, dims[0]-1)
        plt.plot(xaxis, EL, c='red')
        plt.plot(xaxis, BL, c='green')
        plt.plot(xaxis, ET, c='blue')
        plt.plot(xaxis, BT, c='cyan')
        plt.plot(xaxis, numpy.abs(SL)/2, c='black')
        plt.plot(xaxis, GS*1000000000000000.0, c='pink')
        print("evolution + plotting", time.time()-t, "s")
        plt.pause(0.000000001)
    t = time.time()
