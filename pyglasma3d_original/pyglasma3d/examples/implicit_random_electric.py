from __future__ import print_function

import numpy
import time
import pyglasma3d.core
from pyglasma3d.core import SimulationImplicit as Simulation
from pyglasma3d.cy import gauss

n = 32
dims = [n, n, n]
iter_dims = [0, n, 0, n, 0, n]
a = [0.125, 1.0, 1.0]
dt = a[0]
g = 2.0

print("creating simulation object")
s = Simulation(dims, iter_dims, a, dt, g)
print("size of simulation object:", s.get_nbytes() / 1024.0 ** 3, "gb")

# random electric field as initial condition
numpy.random.seed(1337)
amp = 0.01
s.e = numpy.array(numpy.random.uniform(-amp, amp, 3*3*s.N), dtype=numpy.double)/dt * g
s.ep = s.e.copy()
s.init()
s.e = numpy.zeros(3*3*s.N)
s.ep = numpy.zeros(3*3*s.N)

s.damping = 0.3
s.iterations = 5
s.mode = 0

max_it = 100

#plt.ion()
for it in range(0, max_it):
    s.evolve()
    print(s.gauss_constraint())
