#!/usr/bin/python

from __future__ import print_function

import matplotlib.pyplot as plt
import numpy
import numpy as np
import sys
import time

import pyglasma3d.cy.gauss as gauss
import pyglasma3d.cy.mv as mv

import pyglasma3d.cy.interpolate as interpolate
from pyglasma3d.core import Observables
from pyglasma3d.core import SimulationImplicit as Simulation
import pyglasma3d.core

#from pyglasma3d.core import Simulation as Simulation

# simulation parameters
nx, ny, nz = 128, 16, 16
dims = [nx, ny, nz]
iter_dims = [1, nx-1, 0, ny, 0, nz]
a = [0.25, 1.0, 1.0]
dt = a[0]
g = 2.0
sigma = 1*a[0]

numpy.random.seed(1)
if len(sys.argv) > 1:
    file_path = './data/test' + sys.argv[1] + '.dat'
else:
    file_path = './data/test.dat'

DEBUG = True

# start of simulation
print("creating simulation object")
s = Simulation(dims, iter_dims, a, dt, g)
s.debug = True

s.iterations = 15
s.damping = 0.0
s.mode = 1

print("size of simulation object:", round(s.get_nbytes() / 1024.0 ** 3, 4), "gb")

t = time.time()
# initialize mv model, wilson line as return variable
t = time.time()
v1 = mv.initialize_mv(s, x0=dims[0] * 0.25 * a[0], mu=0.2, sigma=sigma, mass=0.1, uvt=1.0, orientation=+1)
interpolate.initialize_charge_planes(s, +1, 1, dims[0] / 2)
print(s.gauss_constraint())
print("Initialization of first nucleus", round(time.time() - t, 3), "s")

t = time.time()
v2 = mv.initialize_mv(s, x0=dims[0] * 0.75 * a[0], mu=0.2, sigma=sigma, mass=0.1, uvt=1.0, orientation=-1)
interpolate.initialize_charge_planes(s, -1, dims[0] / 2, dims[0] - 1)
print(s.gauss_constraint())
print("Initialization of second nucleus", round(time.time() - t, 3), "s")

t = time.time()
interpolate.interpolate_to_charge_density(s)
print(s.gauss_constraint())
print("size of simulation object:", round(s.get_nbytes() / 1024.0 ** 3, 4), "gb")

# run before starting evolution, after initial conditions have been applied
print("Before init", s.gauss_constraint())
s.init()
print("After init", s.gauss_constraint())

print("Before first evolution step", s.gauss_constraint())
s.evolve()
print("After first step", s.gauss_constraint())
#exit()
# initialize interactive plot
plt.ion()
iters = int(dims[0] * a[0] / dt / 2)

for it in range(iters):
    s.evolve()
    if DEBUG and it % 1 == 0:
        gs1 = gauss.gauss_sq_total(s)
        GS = gauss.compute_projected_gauss_density_sq(s)
        EL, BL, ET, BT, SL = Observables.energy_momentum_tensor(s)

        gs_new = np.mean(np.reshape(s.gauss_density(), (nx, ny, nz, 3)), axis=(1, 2))[:, 0]

        r = s.r[1:-2:3*ny*nz]
        j = s.j[1::3*ny*nz]
        plt.clf()
        plt.axis([0, dims[0], 0, 1])
        plt.autoscale(True, axis='y')
        xaxis = numpy.arange(1, dims[0]-1)
        plt.plot(xaxis, EL, c='red')
        plt.plot(xaxis, BL, c='green')
        plt.plot(xaxis, ET, c='blue')
        plt.plot(xaxis, BT, c='cyan')
        plt.plot(gs_new*1000, c='orange')

        print(s.gauss_constraint(), gauss.gauss_sq_total(s), pyglasma3d.core.semiimplicit.gauss_constraint_violation(s))

        plt.pause(0.1)
    t = time.time()
plt.pause(10000)