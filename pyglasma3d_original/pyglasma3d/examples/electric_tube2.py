#!/usr/bin/python

from __future__ import print_function
import matplotlib.pyplot as plt
import numpy as np
import time
import pyglasma3d.cy.energy as energy
from pyglasma3d.core import SimulationImplicit as Simulation


# simulation parameters
nx, ny, nz = 1, 256, 256
dims = [nx, ny, nz]
iter_dims = [0, nx, 0, ny, 0, nz]
a = [0.5, 1.0, 1.0]
dt = a[0]
g = 2.0

# start of simulation
s1 = Simulation(dims, iter_dims, a, dt, g)
s1.iterations = 5
s1.damping = 0.5
s1.mode = 1

s2 = Simulation(dims, iter_dims, a, dt, g)
s2.iterations = 5
s2.damping = 0.5
s2.mode = 3

s2_step = 2
s2.dt /= s2_step

n1, n2, n3 = ny, nz, nx
i1, i2, i3 = 1, 2, 0
m = np.array([[0, 1, 0], [0, 0, 1], [1, 0, 0]]).T


def init(s):
    # initial conditions, a tube of electric field in e_z
    amp = 0.1

    """
    c = 0
    di = 2

    for p1 in xrange(n1):
        for p2 in xrange(n2):
            d1, d2 = a[i1] * (p1 - n1 / 2.0), a[i2] * (p2 - 1 * n2 / 4.0)
            d = np.sqrt(d1 ** 2 + d2 ** 2) / np.sqrt((a[i1] * n1 / 2) ** 2 + (a[i2] * n2 / 2) ** 2)
            for p3 in xrange(n3):
                ix, iy, iz = np.dot(m, [p1, p2, p3])
                index = nz * (ny * ix + iy) + iz
                i = 3 * (3 * index + di) + c
                field = np.exp(- 512.0 * d ** 2)
                s.e[i] += field * amp
    """
    c = 1
    di = 0

    for p1 in xrange(n1):
        for p2 in xrange(n2):
            d1, d2 = a[i1] * (p1 - n1 / 2.0), a[i2] * (p2 - 2 * n2 / 4.0)
            d = np.sqrt(d1 ** 2 + d2 ** 2) / np.sqrt((a[i1] * n1 / 2) ** 2 + (a[i2] * n2 / 2) ** 2)
            for p3 in xrange(n3):
                ix, iy, iz = np.dot(m, [p1, p2, p3])
                index = nz * (ny * ix + iy) + iz
                i = 3 * (3 * index + di) + c
                field = np.exp(- 2 * 1024.0 * d ** 2)
                s.e[i] += field * amp

    s.init()

init(s1)
init(s2)

plt.ion()
iters = 40*4
"""
print(s.gauss())
s.evolve()
print(s.gauss())
for it in range(iters):
    print("Step", it)
    s.evolve_initial()
    plt.clf()
    e = energy.energy_3d(s)
    plt.imshow(e[0, :, :], aspect=a[i1] / a[i2], interpolation='none')
    plt.pause(0.000001)
    for iterations in range(20):
        print("Iterate", iterations)
        plt.clf()
        e = energy.energy_3d(s)
        plt.imshow(e[0, :, :], aspect=a[i1]/a[i2], interpolation='none')
        plt.pause(0.000001)
        s.evolve_iterate()
        #if it % 10 == 0:
        #    print(s.gauss())
plt.pause(1000)
"""

for step in range(1000):
    s1.evolve_initial()
    s1.evolve_iterate()

    for s in range(s2_step):
        s2.evolve_initial()
        s2.evolve_iterate()

    plt.clf()
    e1 = np.sqrt(energy.energy_3d(s1)[0, :, :])
    e2 = np.sqrt(energy.energy_3d(s2)[0, :, :])
    e1 /= np.sum(e1)
    e2 /= np.sum(e2)
    e = np.concatenate((e1[:, 0:n2 / 2], e2[:, n2 / 2:]), axis=1)
    plt.imshow(e, aspect=a[i1], interpolation='none')
    plt.pause(0.000001)

