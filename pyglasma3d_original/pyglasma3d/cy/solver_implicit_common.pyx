#cython: boundscheck=False, wraparound=False, nonecheck=False, cdivision=True
"""
    A module implementing various 'common' operations for implicit solvers.
    In particular: the initial step from leapfrog, but with the fields used
    in the SimulationImplicit class (i.e. u0, u1, u2, e0, e1) and the
    evolution step of the links, which is the same for the implicit, semi-implicit
    and the semi-implicit (early) solver.
"""

cimport lattice as la

cimport cython
cimport openmp
import cython
import numpy as np
cimport numpy as cnp
cimport openmp
from libc.math cimport sin, cos, exp, sqrt
from cython.parallel cimport prange, parallel
from cython.parallel import prange, parallel
from libc.stdlib cimport malloc, free

# combined field and link update
def evolve_initial(s):
    # convert python objects to cython things
    cdef cnp.ndarray[double, ndim=1, mode="c"] u0 = s.up
    cdef cnp.ndarray[double, ndim=1, mode="c"] u1 = s.u0
    cdef cnp.ndarray[double, ndim=1, mode="c"] u2 = s.u1
    cdef cnp.ndarray[double, ndim=1, mode="c"] e0 = s.ep
    cdef cnp.ndarray[double, ndim=1, mode="c"] e1 = s.e
    cdef cnp.ndarray[double, ndim=1, mode="c"] j = s.j
    cdef cnp.ndarray[long, ndim=1, mode="c"] dims = s.dims
    cdef cnp.ndarray[long, ndim=1, mode="c"] acc = s.acc
    cdef cnp.ndarray[long, ndim=1, mode="c"] iter_dims = s.iter_dims
    cdef cnp.ndarray[double, ndim=1, mode="c"] spacings = s.a
    cdef cnp.ndarray[long, ndim=1, mode="c"] neighbours = s.staple_neighbours
    cdef double dt = s.dt

    cdef long ix, iy, iz, di, dj, cell_index, field_index, group_index

    # temporary buffers
    cdef double *group_buffer

    # need to turn iteration bounds to integers due to nogil, otherwise the compiler crashes
    cdef long nx0, nx1, ny0, ny1, nz0, nz1
    nx0, nx1, ny0, ny1, nz0, nz1 = iter_dims

    cdef double plaq_factor[3]
    for dj in range(3):
        plaq_factor[dj] = dt / (spacings[dj] * spacings[dj])

    cdef double current_factor = - s.g * spacings[0] * dt
    cdef long num_transverse = dims[1] * dims[2]

    with nogil, parallel():
        group_buffer = <double *> malloc(4 * sizeof(double))

        for cell_index in prange(nx0 * num_transverse, nx1 * num_transverse, schedule='guided'):
            for di in range(3):
                # field index for electric field (and current)
                field_index = la.get_field_index(cell_index, di)

                # sum over plaquettes (positive and negative), make sure buffer is reset!
                leapfrog_plaquettes(cell_index, di, &u1[0], &neighbours[7*(3*cell_index + di)], &plaq_factor[0], &e1[field_index])
                la.algebra_add(&e1[field_index], &e0[field_index], 1)

                if di == 0:
                    # add current to electric field
                    # current j consists only of jx. 3*c_i gives the right index in this case.
                    la.algebra_add(&e1[field_index], &j[3 * cell_index], current_factor)

                # update link
                group_index = la.get_group_index(cell_index, di)

                # exponentiate electric field "exp(-i dt E)" and write to buffer
                la.mexp(&e1[field_index], - dt, group_buffer)

                # multiply "exp(-i dt E)" with old gauge link and write to new gauge link
                la.mul2_fast(group_buffer, &u1[group_index], &u2[group_index])

        free(group_buffer)

def evolve_links(s):
    # convert python objects to cython things
    cdef cnp.ndarray[double, ndim=1, mode="c"] u0 = s.up
    cdef cnp.ndarray[double, ndim=1, mode="c"] u1 = s.u0
    cdef cnp.ndarray[double, ndim=1, mode="c"] u2 = s.u1
    cdef cnp.ndarray[double, ndim=1, mode="c"] e0 = s.ep
    cdef cnp.ndarray[double, ndim=1, mode="c"] e1 = s.e
    cdef cnp.ndarray[double, ndim=1, mode="c"] j = s.j
    cdef cnp.ndarray[long, ndim=1, mode="c"] dims = s.dims
    cdef cnp.ndarray[long, ndim=1, mode="c"] acc = s.acc
    cdef cnp.ndarray[long, ndim=1, mode="c"] iter_dims = s.iter_dims
    cdef cnp.ndarray[double, ndim=1, mode="c"] spacings = s.a
    cdef cnp.ndarray[long, ndim=1, mode="c"] neighbours = s.staple_neighbours
    cdef double dt = s.dt

    cdef long ix, iy, iz, d, cell_index, field_index, group_index

    # temporary group element where we put the exponentiated electric field
    cdef double *group_buffer

    # need to turn iteration bounds to integers due to nogil, otherwise the compiler crashes
    cdef long nx0, nx1, ny0, ny1, nz0, nz1
    nx0, nx1, ny0, ny1, nz0, nz1 = iter_dims

    # parallelize along x direction
    for ix in prange(nx0, nx1, nogil=True):
        # allocate the buffer thread-locally
        group_buffer = <double *> malloc(4 * sizeof(double))
        for iy in range(ny0, ny1):
            for iz in range(nz0, nz1):
                cell_index = la.get_index_nm(ix, iy, iz, &dims[0])
                for d in range(3):
                    field_index = la.get_field_index(cell_index, d)
                    group_index = la.get_group_index(cell_index, d)

                    # exponentiate electric field "exp(-i dt E)" and write to buffer
                    la.mexp(&e1[field_index], - dt, group_buffer)

                    # multiply "exp(-i dt E)" with old gauge link and write to new gauge link
                    la.mul2(group_buffer, &u1[group_index], 1, 1, &u2[group_index])
        free(group_buffer)

# evolution without exponentiation
def evolve_links2(s):
    # convert python objects to cython things
    cdef cnp.ndarray[double, ndim=1, mode="c"] u0 = s.up
    cdef cnp.ndarray[double, ndim=1, mode="c"] u1 = s.u0
    cdef cnp.ndarray[double, ndim=1, mode="c"] u2 = s.u1
    cdef cnp.ndarray[double, ndim=1, mode="c"] e0 = s.ep
    cdef cnp.ndarray[double, ndim=1, mode="c"] e1 = s.e
    cdef cnp.ndarray[double, ndim=1, mode="c"] j = s.j
    cdef cnp.ndarray[long, ndim=1, mode="c"] dims = s.dims
    cdef cnp.ndarray[long, ndim=1, mode="c"] acc = s.acc
    cdef cnp.ndarray[long, ndim=1, mode="c"] iter_dims = s.iter_dims
    cdef cnp.ndarray[double, ndim=1, mode="c"] spacings = s.a
    cdef cnp.ndarray[long, ndim=1, mode="c"] neighbours = s.staple_neighbours
    cdef double dt = s.dt

    cdef long ix, iy, iz, d, cell_index, field_index, group_index, k

    # temporary group element where we put the exponentiated electric field
    cdef double *group_buffer

    # need to turn iteration bounds to integers due to nogil, otherwise the compiler crashes
    cdef long nx0, nx1, ny0, ny1, nz0, nz1
    nx0, nx1, ny0, ny1, nz0, nz1 = iter_dims

    # parallelize along x direction
    for ix in prange(nx0, nx1, nogil=True):
        # allocate the buffer thread-locally
        group_buffer = <double *> malloc(4 * sizeof(double))
        for iy in range(ny0, ny1):
            for iz in range(nz0, nz1):
                cell_index = la.get_index_nm(ix, iy, iz, &dims[0])
                for d in range(3):
                    field_index = la.get_field_index(cell_index, d)
                    group_index = la.get_group_index(cell_index, d)

                    for k in range(3):
                        group_buffer[k+1] = - 0.5 * dt * e1[field_index+k]

                    # unitarize
                    group_buffer[0] = 0.25 * la.algebra_sq(&e1[field_index]) * dt ** 2
                    group_buffer[0] = sqrt(1.0 - group_buffer[0])

                    # multiply "exp(-i dt E)" with old gauge link and write to new gauge link
                    la.mul2(group_buffer, &u1[group_index], 1, 1, &u2[group_index])
        free(group_buffer)

# compute electric field from gauge links
def evolve_u_inv2(s):
    # convert python objects to cython things
    cdef cnp.ndarray[double, ndim=1, mode="c"] links0 = s.u0
    cdef cnp.ndarray[double, ndim=1, mode="c"] links1 = s.u1
    cdef cnp.ndarray[double, ndim=1, mode="c"] electric = s.e
    cdef cnp.ndarray[double, ndim=1, mode="c"] current = s.j
    cdef cnp.ndarray[long, ndim=1, mode="c"] dims = s.dims
    cdef cnp.ndarray[long, ndim=1, mode="c"] acc = s.acc
    cdef cnp.ndarray[long, ndim=1, mode="c"] iter_dims = s.iter_dims
    cdef cnp.ndarray[double, ndim=1, mode="c"] spacings = s.a
    cdef double dt = s.dt
    cdef double inv_dt = -1.0 / dt

    cdef long ix, iy, iz, d, cell_index, field_index, group_index

    # temporary group element where we put the exponentiated electric field
    cdef double *buffer

    # need to turn iteration bounds to integers due to nogil, otherwise the compiler crashes
    cdef long nx0, nx1, ny0, ny1, nz0, nz1
    nx0, nx1, ny0, ny1, nz0, nz1 = iter_dims

    # parallelize along x direction
    for ix in prange(nx0, nx1, nogil=True):
        # allocate the buffer thread-locally
        buffer = <double *> malloc(4 * sizeof(double))
        for iy in range(ny0, ny1):
            for iz in range(nz0, nz1):
                cell_index = la.get_index_nm(ix, iy, iz, &dims[0])
                for d in range(3):
                    field_index = la.get_field_index(cell_index, d)
                    group_index = la.get_group_index(cell_index, d)
                    la.mul2(&links1[group_index], &links0[group_index], 1, -1, buffer)
                    la.proj(buffer, &electric[field_index])
                    la.algebra_mul(&electric[field_index], inv_dt)
        free(buffer)


"""
    C functions
"""

cdef void leapfrog_plaquettes(long x, long d, double*links, long* neighbours, double*plaq_factor, double *result) nogil:
    cdef double buffer1[4]
    cdef double buffer2[4]
    cdef double buffer_S[4]
    cdef long i, ci1, ci2, ci3, ci4, index_c
    index_c = 0
    ci1 = neighbours[0]
    la.group_zero(&buffer_S[0])
    for i in range(3):
        if i != d:
            ci2 = neighbours[1 + index_c * 3]
            ci3 = neighbours[2 + index_c * 3]
            ci4 = neighbours[3 + index_c * 3]
            la.mul2(&links[4 * (3 * ci1 + i)], &links[4 * (3 * ci2 + d)], 1, -1, &buffer1[0])
            la.mul2(&buffer1[0], &links[4 * (3 * x + i)], 1, -1, &buffer2[0])
            la.group_add(&buffer_S[0], &buffer2[0], plaq_factor[i])
            la.mul2(&links[4 * (3 * ci3 + i)], &links[4 * (3 * ci4 + d)], -1, -1, &buffer1[0])
            la.mul2_fast(&buffer1[0], &links[4 * (3 * ci4 + i)], &buffer2[0])
            la.group_add(&buffer_S[0], &buffer2[0], plaq_factor[i])
            index_c += 1
    la.mul2_fast(&links[4 * (3 * x + d)], &buffer_S[0], &buffer1[0])
    la.proj(&buffer1[0], &result[0])


# compute staple sum for implicit scheme
cdef void implicit_plaquettes(double *u0, double *u1, double *u2,
                              long x, long i, long *dims, long *acc, double *f,
                              double *r) nogil:
    cdef long xs, xs2, j, oj, xr, gr

    cdef double[4] buffer0
    cdef double[4] buffer1
    cdef double[4] buffer2
    cdef double[4] buffer3

    xs = la.shift2(x, i, 1, dims, acc)

    la.group_zero(buffer3)
    for oj in range(-1, 3, 2):
        for j in range(3):
            if j != i:
                # type 1 staple (U*M)
                link(xs, j, oj, dims, acc, &xr, &gr)
                m(u0, u2, x, i, j, 1, oj, dims, acc, buffer1)
                la.mul2(&u1[gr], buffer1, oj, -1, buffer0)
                la.group_add(buffer3, buffer0, f[j])

                # type 2 staple (M*U)
                xs2 = la.shift2(x, j, -oj, dims, acc)
                link(xs2, j, oj, dims, acc, &xr, &gr)
                m(u0, u2, xs2, j, i, oj, 1, dims, acc, buffer1)
                la.mul2(buffer1, &u1[gr], -1, oj, buffer0)
                la.group_add(buffer3, buffer0, f[j])

    la.mul2(&u1[la.get_group_index(x, i)], buffer3, 1, 1, buffer0)
    la.proj(buffer0, r)

# lattice field strength (calligraphic C)
cdef void c(double *u, long x, long i, long j, long oi, long oj, long *dims, long *acc, double *r) nogil:
    cdef double[4] buffer0
    l(u, x, i, j, oi, oj, dims, acc, r)
    l(u, x, j, i, oj, oi, dims, acc, buffer0)
    la.group_add(r, buffer0, -1)

# time-averaged field strength (calligraphic M)
cdef void m(double *up, double *uf, long x, long i, long j, long oi, long oj, long *dims, long *acc, double *r) nogil:
    cdef double[4] buffer0

    la.group_zero(r)

    c(up, x, i, j, oi, oj, dims, acc, buffer0)
    la.group_add(r, buffer0, 0.5)

    c(uf, x, i, j, oi, oj, dims, acc, buffer0)
    la.group_add(r, buffer0, 0.5)

# semi-time-averaged field strength (calligraphic W_x,1i)
cdef void w(double *u0, double *u1, double *u2,
            long x, long i, long oi,
            long *dims, long *acc, double *r) nogil:
    cdef double[4] buffer0
    cdef double[4] buffer1
    cdef long xs, xr, gr
    # Ub_x,1 * U_x+1, oi * i
    u_bar(u0, u2, x, 0, 1, dims, acc, buffer0)
    xs = la.shift2(x, 0, 1, dims, acc)
    link(xs, i, oi, dims, acc, &xr, &gr)
    la.mul2(&buffer0[0], &u1[gr], 1, oi, r)

    # U_x, oi *i * Ub_x+oi*i, 1
    link(x, i, oi, dims, acc, &xr, &gr)
    u_bar(u0, u2, xr, 0, 1, dims, acc, buffer0)
    la.mul2(&u1[gr], &buffer0[0], oi, 1, buffer1)

    # subtract second term from first
    la.group_add(r, buffer1, -1)

# 'L' path: U_{x,i} U_{x+i,j}
cdef void l(double *u, long x, long i, long j, long oi, long oj, long* dims, long* acc, double *r) nogil:
    cdef long x0, g0, x1, g1
    link(x,  i, oi, dims, acc, &x0, &g0)
    link(x0, j, oj, dims, acc, &x1, &g1)

    la.mul2(&u[g0], &u[g1], oi, oj, r)

# group index of a link starting at x, in direction oi * i
cdef void link(long x, long i, long oi, long* dims, long* acc, long *xr, long *gr) nogil:
    cdef long di, wdi
    xr[0] = x
    di = x / acc[i + 1]
    wdi = di % dims[i]
    if oi > 0:
        if wdi == dims[i] - 1:
            xr[0] -= acc[i]
        xr[0] += acc[i+1]
        gr[0] = 4*(3*x + i)
    else:
        if wdi == 0:
            xr[0] += acc[i]
        xr[0] -= acc[i+1]
        gr[0] = 4*(3*xr[0] + i)

# time-averaged link (u bar)
cdef void u_bar(double *up, double *uf,long x, long i, long oi, long *dims, long *acc, double *r) nogil:
    cdef long xr, gr
    link(x, i, oi, dims, acc, &xr, &gr)

    la.group_zero(r)
    la.group_add(r, &up[gr], 0.5)
    la.group_add(r, &uf[gr], 0.5)

# temporal plaquette U_x,0i
cdef void tplaq(double *u0, double *u1, long x, long i, long oi, long *dims, long *acc, double *r) nogil:
    cdef long x0, g0
    link(x, i, oi, dims, acc, &x0, &g0)
    la.mul2(&u1[g0], &u0[g0], oi, -oi, r)



"""
A 'convenience' function to compute U_x+i, j * F^t_x,ij - F^t_x-j,ij * U_x-j,j,
where F can stand for C, M or W depending on the mode parameter.
    
    Modes:
    0: use C
    1: use M
    2: use W
"""
cdef void staples_a(long x, long i, long j, long oi, long oj, int mode,
                    double *u0, double *u1, double *u2,
                    long*dims, long*acc,
                    double*r) nogil:

    cdef double[4] F1, F2, buffer0
    cdef long xs1, xs2, xr, gr

    la.group_zero(r)
    xs1 = la.shift2(x, i, oi, dims, acc)
    xs2 = la.shift2(x, j, -oj, dims, acc)

    # compute F
    if mode == 0:
        c(u1, x,       i, j, oi, oj, dims, acc, F1)
        c(u1, xs2,     i, j, oi, oj, dims, acc, F2)
    elif mode == 1:
        m(u0, u2, x,   i, j, oi, oj, dims, acc, F1)
        m(u0, u2, xs2, i, j, oi, oj, dims, acc, F2)
    elif mode == 2:
        if i == 0:
            w(u0, u1, u2, x,   j, oj, dims, acc, F1)
            w(u0, u1, u2, xs2, j, oj, dims, acc, F2)
        elif j == 0:
            w(u0, u1, u2, x,   i, oi, dims, acc, buffer0)
            la.group_zero(F1)
            la.group_add(F1, buffer0, -1)
            w(u0, u1, u2, xs2, i, oi, dims, acc, buffer0)
            la.group_zero(F2)
            la.group_add(F2, buffer0, -1)

    # + U_x+i,j * F^t_x,ij
    link(xs1, j, oj, dims, acc, &xr, &gr)
    la.mul2(&u1[gr], F1, oj, -1, buffer0)
    la.group_add(r, buffer0, +1)

    # - F^t_x-j,ij * U_x-j,j
    link(xs2, j, oj, dims, acc, &xr, &gr)
    la.mul2(F2, &u1[gr], -1, oj, buffer0)
    la.group_add(r, buffer0, -1)

"""
A 'convenience' function to compute AVG(U_x+i,j * C^t_x,ij - C^t_x-j,ij * U_x-j,j),
where AVG averages over u0 and u2.    
"""
cdef void staples_b(long x, long i, long j, long oi, long oj,
                    double *u0, double *u1, double *u2,
                    long*dims, long*acc,
                    double*r) nogil:
    cdef double[4] F1, F2, buffer0
    la.group_zero(r)

    staples_a(x, i, j, oi, oj, 0, u0, u0, u0, dims, acc, buffer0)
    la.group_add(r, buffer0, 0.5)

    staples_a(x, i, j, oi, oj, 0, u2, u2, u2, dims, acc, buffer0)
    la.group_add(r, buffer0, 0.5)

"""
A 'convenience' function to compute bU_x+i,j * C^t_x,ij - C^t_x-j,ij * bU_x-j,j,
where bU averages over u0 and u2 (u_bar).    
"""
cdef void staples_c(long x, long i, long j, long oi, long oj,
                    double *u0, double *u1, double *u2,
                    long*dims, long*acc,
                    double*r) nogil:
    cdef double[4] F1, F2, buffer0, buffer1
    cdef long xs1, xs2, xr, gr, mode
    cdef double* uc

    la.group_zero(r)
    xs1 = la.shift2(x, i, oi, dims, acc)
    xs2 = la.shift2(x, j, -oj, dims, acc)

    c(u1, x,   i, j, oi, oj, dims, acc, F1)
    c(u1, xs2, i, j, oi, oj, dims, acc, F2)

    # + U_x+i, j * F^t_x,ij
    u_bar(u0, u2, xs1, j, oj, dims, acc, buffer1)
    la.mul2(buffer1, F1, oi, -1, buffer0)
    la.group_add(r, buffer0, + 1)

    # - F^t_x-j,ij * U_x-j,j
    u_bar(u0, u2, xs2, j, oj, dims, acc, buffer1)
    la.mul2(F2, buffer1, -1, oj, buffer0)
    la.group_add(r, buffer0, - 1)
