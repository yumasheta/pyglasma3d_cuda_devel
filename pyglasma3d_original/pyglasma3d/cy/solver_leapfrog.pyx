#cython: boundscheck=False, wraparound=False, nonecheck=False, cdivision=True
"""
    A module related to the leapfrog YM solver
"""
from lattice cimport *

cimport cython
cimport openmp
import cython
import numpy as np
cimport numpy as cnp
cimport openmp
from libc.math cimport sin, cos, exp, sqrt
from cython.parallel cimport prange, parallel
from cython.parallel import prange, parallel
from libc.stdlib cimport malloc, free

# gauge link update
def evolve_u(s):
    # convert python objects to cython things
    cdef cnp.ndarray[double, ndim=1, mode="c"] links0 = s.u0
    cdef cnp.ndarray[double, ndim=1, mode="c"] links1 = s.u1
    cdef cnp.ndarray[double, ndim=1, mode="c"] electric = s.e
    cdef cnp.ndarray[double, ndim=1, mode="c"] current = s.j
    cdef cnp.ndarray[long, ndim=1, mode="c"] dims = s.dims
    cdef cnp.ndarray[long, ndim=1, mode="c"] acc = s.acc
    cdef cnp.ndarray[long, ndim=1, mode="c"] iter_dims = s.iter_dims
    cdef cnp.ndarray[double, ndim=1, mode="c"] spacings = s.a
    cdef double dt = s.dt

    cdef long ix, iy, iz, d, cell_index, field_index, group_index

    # temporary group element where we put the exponentiated electric field
    cdef double *buffer

    # need to turn iteration bounds to integers due to nogil, otherwise the compiler crashes
    cdef long nx0, nx1, ny0, ny1, nz0, nz1
    nx0, nx1, ny0, ny1, nz0, nz1 = iter_dims

    # parallelize along x direction
    for ix in prange(nx0, nx1, nogil=True):
        # allocate the buffer thread-locally
        buffer = <double *> malloc(4 * sizeof(double))
        for iy in range(ny0, ny1):
            for iz in range(nz0, nz1):
                cell_index = get_index_nm(ix, iy, iz, &dims[0])
                for d in range(3):
                    field_index = get_field_index(cell_index, d)
                    group_index = get_group_index(cell_index, d)

                    # exponentiate electric field "exp(-i dt E)" and write to buffer
                    mexp(&electric[field_index], - dt, buffer)

                    # multiply "exp(-i dt E)" with old gauge link and write to new gauge link
                    mul2(buffer, &links0[group_index], 1, 1, &links1[group_index])
        free(buffer)

# combined field and link update
def evolve(s):
    # convert python objects to cython things
    cdef cnp.ndarray[double, ndim=1, mode="c"] links0 = s.u0
    cdef cnp.ndarray[double, ndim=1, mode="c"] links1 = s.u1
    cdef cnp.ndarray[double, ndim=1, mode="c"] electric = s.e
    cdef cnp.ndarray[double, ndim=1, mode="c"] current = s.j
    cdef cnp.ndarray[long, ndim=1, mode="c"] dims = s.dims
    cdef cnp.ndarray[long, ndim=1, mode="c"] acc = s.acc
    cdef cnp.ndarray[long, ndim=1, mode="c"] iter_dims = s.iter_dims
    cdef cnp.ndarray[double, ndim=1, mode="c"] spacings = s.a
    cdef cnp.ndarray[long, ndim=1, mode="c"] neighbours = s.staple_neighbours
    cdef double dt = s.dt

    cdef long ix, iy, iz, di, dj, cell_index, field_index, group_index

    # temporary buffers
    cdef double *group_buffer1
    cdef double *group_buffer2
    cdef double *algebra_buffer

    # need to turn iteration bounds to integers due to nogil, otherwise the compiler crashes
    cdef long nx0, nx1, ny0, ny1, nz0, nz1
    nx0, nx1, ny0, ny1, nz0, nz1 = iter_dims

    cdef double plaq_factor[3]
    for dj in range(3):
        plaq_factor[dj] = dt / (spacings[dj] * spacings[dj])

    cdef double current_factor = - s.g * spacings[0] * dt
    cdef long num_transverse = dims[1] * dims[2]

    with nogil, parallel():
        group_buffer = <double *> malloc(4 * sizeof(double))
        algebra_buffer = <double *> malloc(3 * sizeof(double))

        for cell_index in prange(nx0 * num_transverse, nx1 * num_transverse, schedule='guided'):
            for di in range(3):
                # sum over plaquettes (positive and negative), make sure buffer is reset!
                #compute_staple_sum(cell_index, di, &links0[0], &dims[0], &acc[0], &plaq_factor[0], algebra_buffer)
                compute_staple_sum2(cell_index, di, &links0[0], &neighbours[7*(3*cell_index + di)], &plaq_factor[0], algebra_buffer)

                # field index for electric field (and current)
                field_index = get_field_index(cell_index, di)

                # add sum over plaquettes to electric field
                algebra_add(&electric[field_index], algebra_buffer, 1)
                if di == 0:
                    # add current to electric field
                    # current j consists only of jx. 3*c_i gives the right index in this case.
                    algebra_add(&electric[field_index], &current[3 * cell_index], current_factor)

                # update link
                group_index = get_group_index(cell_index, di)

                # exponentiate electric field "exp(-i dt E)" and write to buffer
                mexp(&electric[field_index], - dt, group_buffer)

                # multiply "exp(-i dt E)" with old gauge link and write to new gauge link
                mul2_fast(group_buffer, &links0[group_index], &links1[group_index])

        free(group_buffer)
        free(algebra_buffer)

# compute electric field from gauge links
def evolve_u_inv(s):
    # convert python objects to cython things
    cdef cnp.ndarray[double, ndim=1, mode="c"] links0 = s.u0
    cdef cnp.ndarray[double, ndim=1, mode="c"] links1 = s.u1
    cdef cnp.ndarray[double, ndim=1, mode="c"] electric = s.e
    cdef cnp.ndarray[double, ndim=1, mode="c"] current = s.j
    cdef cnp.ndarray[long, ndim=1, mode="c"] dims = s.dims
    cdef cnp.ndarray[long, ndim=1, mode="c"] acc = s.acc
    cdef cnp.ndarray[long, ndim=1, mode="c"] iter_dims = s.iter_dims
    cdef cnp.ndarray[double, ndim=1, mode="c"] spacings = s.a
    cdef double dt = s.dt
    cdef double inv_dt = -1.0 / dt

    cdef long ix, iy, iz, d, cell_index, field_index, group_index

    # temporary group element where we put the exponentiated electric field
    cdef double *buffer

    # need to turn iteration bounds to integers due to nogil, otherwise the compiler crashes
    cdef long nx0, nx1, ny0, ny1, nz0, nz1
    nx0, nx1, ny0, ny1, nz0, nz1 = iter_dims

    # parallelize along x direction
    for ix in prange(nx0, nx1, nogil=True):
        # allocate the buffer thread-locally
        buffer = <double *> malloc(4 * sizeof(double))
        for iy in range(ny0, ny1):
            for iz in range(nz0, nz1):
                cell_index = get_index_nm(ix, iy, iz, &dims[0])
                for d in range(3):
                    field_index = get_field_index(cell_index, d)
                    group_index = get_group_index(cell_index, d)
                    mul2(&links1[group_index], &links0[group_index], 1, -1, buffer)
                    mlog(buffer, &electric[field_index])
                    algebra_mul(&electric[field_index], inv_dt)
        free(buffer)

# compute staple sum for optimized eom
cdef void compute_staple_sum(long x, long d, double*links, long*dims, long*acc, double*plaq_factor,
                             double *result) nogil:
    cdef double buffer1[4]
    cdef double buffer2[4]
    cdef double buffer_S[4]
    cdef long i, ci1, ci2, ci3, ci4
    ci1 = shift2(x, d, 1, dims, acc)
    group_zero(&buffer_S[0])
    for i in range(3):
        if i != d:
            ci2 = shift2(x, i, 1, dims, acc)
            ci3 = shift2(ci1, i, -1, dims, acc)
            ci4 = shift2(x, i, -1, dims, acc)
            mul2(&links[4 * (3 * ci1 + i)], &links[4 * (3 * ci2 + d)], 1, -1, &buffer1[0])
            mul2(&buffer1[0], &links[4 * (3 * x + i)], 1, -1, &buffer2[0])
            group_add(&buffer_S[0], &buffer2[0], plaq_factor[i])
            mul2(&links[4 * (3 * ci3 + i)], &links[4 * (3 * ci4 + d)], -1, -1, &buffer1[0])
            mul2_fast(&buffer1[0], &links[4 * (3 * ci4 + i)], &buffer2[0])
            group_add(&buffer_S[0], &buffer2[0], plaq_factor[i])
    mul2_fast(&links[4 * (3 * x + d)], &buffer_S[0], &buffer1[0])
    proj(&buffer1[0], &result[0])

cdef void compute_staple_sum2(long x, long d, double*links, long* neighbours, double*plaq_factor, double *result) nogil:
    cdef double buffer1[4]
    cdef double buffer2[4]
    cdef double buffer_S[4]
    cdef long i, ci1, ci2, ci3, ci4, index_c
    index_c = 0
    ci1 = neighbours[0]
    group_zero(&buffer_S[0])
    for i in range(3):
        if i != d:
            ci2 = neighbours[1 + index_c * 3]
            ci3 = neighbours[2 + index_c * 3]
            ci4 = neighbours[3 + index_c * 3]
            mul2(&links[4 * (3 * ci1 + i)], &links[4 * (3 * ci2 + d)], 1, -1, &buffer1[0])
            mul2(&buffer1[0], &links[4 * (3 * x + i)], 1, -1, &buffer2[0])
            group_add(&buffer_S[0], &buffer2[0], plaq_factor[i])
            mul2(&links[4 * (3 * ci3 + i)], &links[4 * (3 * ci4 + d)], -1, -1, &buffer1[0])
            mul2_fast(&buffer1[0], &links[4 * (3 * ci4 + i)], &buffer2[0])
            group_add(&buffer_S[0], &buffer2[0], plaq_factor[i])
            index_c += 1
    mul2_fast(&links[4 * (3 * x + d)], &buffer_S[0], &buffer1[0])
    proj(&buffer1[0], &result[0])

def compute_staple_neighbours(s):
    cdef cnp.ndarray[long, ndim=1, mode="c"] neighbours = s.staple_neighbours
    cdef long n = s.dims[0] * s.dims[1] * s.dims[2]
    cdef cnp.ndarray[long, ndim=1, mode="c"] dims = s.dims
    cdef cnp.ndarray[long, ndim=1, mode="c"] acc = s.acc

    cdef long x, d, i, ci1, ci2, ci3, ci4, index_c
    for x in range(n):
        for d in range(3):
            index_c = 0
            ci1 = shift2(x, d, 1, &dims[0], &acc[0])
            neighbours[7*(3*x + d) + 0] = ci1
            for i in range(3):
                if i != d:
                    ci2 = shift2(x, i, 1, &dims[0], &acc[0])
                    ci3 = shift2(ci1, i, -1, &dims[0], &acc[0])
                    ci4 = shift2(x, i, -1, &dims[0], &acc[0])

                    neighbours[7*(3*x + d) + 1 + index_c * 3] = ci2
                    neighbours[7*(3*x + d) + 2 + index_c * 3] = ci3
                    neighbours[7*(3*x + d) + 3 + index_c * 3] = ci4

                    index_c += 1


