#cython: boundscheck=False, wraparound=False, nonecheck=False, cdivision=True

"""
    A module implementing the leapfrog solver for lattice Yang-Mills theory. This module uses the same design
    as the implicit solvers and is mainly used for direct comparison and debugging.
"""
cimport lattice as la

cimport solver_implicit_common as ccommon
import solver_implicit_common as common

from cython.parallel cimport prange, parallel
from cython.parallel import prange, parallel
import numpy as np
cimport numpy as cnp
from libc.stdlib cimport malloc, free

def evolve_iterate(s, iterations, damping):
    pass

def evolve_initial(s):
    # leapfrog is explicit, initial guess is final
    evolve_electric(s, 0)
    common.evolve_links2(s)

def evolve_electric(s, damping):
    # convert python objects to cython things
    cdef cnp.ndarray[double, ndim=1, mode="c"] u0 = s.up
    cdef cnp.ndarray[double, ndim=1, mode="c"] u1 = s.u0
    cdef cnp.ndarray[double, ndim=1, mode="c"] u2 = s.u1
    cdef cnp.ndarray[double, ndim=1, mode="c"] e0 = s.ep
    cdef cnp.ndarray[double, ndim=1, mode="c"] e1 = s.e
    cdef cnp.ndarray[double, ndim=1, mode="c"] j = s.j
    cdef cnp.ndarray[long, ndim=1, mode="c"] dims = s.dims
    cdef cnp.ndarray[long, ndim=1, mode="c"] acc = s.acc
    cdef cnp.ndarray[long, ndim=1, mode="c"] iter_dims = s.iter_dims
    cdef cnp.ndarray[double, ndim=1, mode="c"] spacings = s.a
    cdef cnp.ndarray[long, ndim=1, mode="c"] neighbours = s.staple_neighbours
    cdef double dt = s.dt

    cdef long ix, iy, iz, di, dj, cell_index, field_index, group_index

    # temporary buffers
    cdef double *group_buffer
    cdef double *algebra_buffer

    # need to turn iteration bounds to integers due to nogil, otherwise the compiler crashes
    cdef long nx0, nx1, ny0, ny1, nz0, nz1
    nx0, nx1, ny0, ny1, nz0, nz1 = iter_dims

    cdef double plaq_factor[3]
    for dj in range(3):
        plaq_factor[dj] = - dt / spacings[dj] ** 2

    cdef double current_factor = - s.g * spacings[0] * dt
    cdef long num_transverse = dims[1] * dims[2]

    with nogil, parallel():
        group_buffer = <double *> malloc(4 * sizeof(double))
        algebra_buffer = <double *> malloc(3 * sizeof(double))

        for cell_index in prange(nx0 * num_transverse, nx1 * num_transverse, schedule='guided'):
            for di in range(3):
                la.algebra_zero(algebra_buffer)

                # field index for electric field
                field_index = la.get_field_index(cell_index, di)
                group_index = la.get_group_index(cell_index, di)

                # eom in terms of C, analogous to the implicit schemes
                leapfrog_eom_terms(&u0[0], &u1[0], &u2[0], cell_index, di, &dims[0], &acc[0], &plaq_factor[0], &algebra_buffer[0])

                # direct solver
                la.algebra_mul(&e1[field_index], 0.0)
                la.algebra_add(&e1[field_index], &e0[field_index], 1.0)
                la.algebra_add(&e1[field_index], algebra_buffer, 1.0)

                # include current for longitudinal components
                if di == 0:
                    la.algebra_add(&e1[field_index], &j[3 * cell_index], current_factor)

        free(group_buffer)
        free(algebra_buffer)

# Gauss constraint, averaged across the lattice
def gauss_constraint(s):
    cdef cnp.ndarray[double, ndim=1, mode="c"] g_density
    g_density = gauss_density(s)
    cdef double result = 0.0
    cdef long n = s.N
    cdef long x

    with nogil:
        for x in prange(n):
            result += la.algebra_sq(&g_density[3*x])

    return result / n

# Gauss density
def gauss_density(s):
    cdef:
        cnp.ndarray[double, ndim=1, mode="c"]   u0 = s.up
        cnp.ndarray[double, ndim=1, mode="c"]   u1 = s.u0
        cnp.ndarray[double, ndim=1, mode="c"]   u2 = s.u1
        cnp.ndarray[double, ndim=1, mode="c"]   e0 = s.ep
        cnp.ndarray[double, ndim=1, mode="c"]   e1 = s.e
        cnp.ndarray[double, ndim=1, mode="c"]   rho = s.r
        cnp.ndarray[long, ndim=1, mode="c"]     dims = s.dims
        cnp.ndarray[long, ndim=1, mode="c"]     acc = s.acc
        cnp.ndarray[long, ndim=1, mode="c"]     iter_dims = s.iter_dims
        cnp.ndarray[double, ndim=1, mode="c"]   a = s.a

        long nx0, nx1, ny0, ny1, nz0, nz1
        double g = s.g
        double a0 = s.dt

        long x, i
        double * buffer0
        double * buffer1
        double * buffer2

        cnp.ndarray[double, ndim=1, mode="c"] g_density = np.zeros(3*s.N, dtype=np.double)

    nx0, nx1, ny0, ny1, nz0, nz1 = iter_dims

    with nogil, parallel():
        buffer0 = <double *> malloc(4 * sizeof(double))
        buffer1 = <double *> malloc(4 * sizeof(double))
        buffer2 = <double *> malloc(4 * sizeof(double))

        for x in prange((nx0+2) * dims[1] * dims[2], (nx1-2) * dims[1] * dims[2]):
            la.algebra_zero(buffer2)
            # Standard electric contribution D_i E_x,i - rho
            # ... Using temporal plaquettes
            for i in range(3):
                ccommon.tplaq(&u1[0], &u2[0], x, i, +1, &dims[0], &acc[0], buffer0)
                ccommon.tplaq(&u1[0], &u2[0], x, i, -1, &dims[0], &acc[0], buffer1)
                la.group_add(buffer0, buffer1, 1)
                la.proj(buffer0, buffer1)
                la.algebra_add(buffer2, buffer1, -a0 / (a0 * a[i]) ** 2 / g)

            # Charge density
            la.algebra_add(buffer2, &rho[3*x], -1.0)

            # Write to result vector
            la.algebra_add(&g_density[3*x], buffer2, 1)

        free(buffer0)
        free(buffer1)
        free(buffer2)

    return g_density


"""
    C functions
"""

cdef void leapfrog_eom_terms(double *u0, double *u1, double *u2,
                                  long x, long i,
                                  long *dims, long *acc, double *factors,
                                  double *r) nogil:
    cdef double[4] buffer0, buffer1, buffer2, buffer3
    cdef long xs, xr, gr, j
    cdef double *uc

    la.group_zero(buffer3)

    for j in range(3):
        if j != i:
            # + U_x+1,j * C^t_x,1j - C^t_x-j,1j * U_x-j,j
            ccommon.staples_a(x, i, j, 1, 1, 0, u0, u1, u2, dims, acc, buffer2)
            la.group_add(buffer3, buffer2, factors[j])

    # project and put into result vector
    la.mul2(&u1[la.get_group_index(x, i)], buffer3, 1, 1, buffer2)
    la.proj(buffer2, r)