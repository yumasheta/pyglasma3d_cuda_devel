#cython: boundscheck=False, wraparound=False, nonecheck=False, cdivision=True
"""
    SU(2) group and algebra functions
    Grid functions
"""
cimport cython
import cython
import numpy as np
cimport numpy as cnp
from libc.math cimport sin, cos, exp, sqrt, acos
from cython.parallel cimport prange, parallel
from cython.parallel import prange, parallel
from libc.stdlib cimport malloc, free

"""
    SU(2) group & algebra functions
"""

# su2 multiplication (cython)
cdef void mul2(double* a, double* b, long ta, long tb, double* r) nogil:
    r[0] = a[0]        * b[0] - (ta * a[1]) * (tb * b[1]) - (ta * a[2]) * (tb * b[2]) - (ta * a[3]) * (tb * b[3])
    r[1] = (ta * a[1]) * b[0] + a[0]        * (tb * b[1]) + (ta * a[3]) * (tb * b[2]) - (ta * a[2]) * (tb * b[3])
    r[2] = (ta * a[2]) * b[0] - (ta * a[3]) * (tb * b[1]) + a[0]        * (tb * b[2]) + (ta * a[1]) * (tb * b[3])
    r[3] = (ta * a[3]) * b[0] + (ta * a[2]) * (tb * b[1]) - (ta * a[1]) * (tb * b[2]) + a[0]        * (tb * b[3])

# fast su2 multiplication (cython)
cdef void mul2_fast(double* a, double* b, double* r) nogil:
    r[0] = a[0] * b[0] - a[1] * b[1] - a[2] * b[2] - a[3] * b[3]
    r[1] = a[1] * b[0] + a[0] * b[1] + a[3] * b[2] - a[2] * b[3]
    r[2] = a[2] * b[0] - a[3] * b[1] + a[0] * b[2] + a[1] * b[3]
    r[3] = a[3] * b[0] + a[2] * b[1] - a[1] * b[2] + a[0] * b[3]

# su2 multiplication (python)
def mul2_p(double[::1] a, double[::1] b, long ta, long tb):
    #cdef cnp.ndarray r = np.zeros(4, dtype=np.double)
    cdef double r[4]
    mul2(&a[0], &b[0], ta, tb, r)
    return r

# fast su2 multiplication (python)
def mul2_fast_p(double[::1] a, double[::1] b):
    #cdef cnp.ndarray r = np.zeros(4, dtype=np.double)
    cdef double r[4]
    mul2_fast(&a[0], &b[0], r)
    return r

# product of 4 matrices (cython)
cdef inline void mul4(double* a, double* b, double* c, double* d, long ta, long tb, long tc, long td, double* r) nogil:
    # a^(t).b^(t).c^(t).d^(t)
    cdef double ab[4]
    cdef double cd[4]
    mul2(a, b, ta, tb, ab)
    mul2(c, d, tc, td, cd)
    mul2_fast(ab, cd, r)

# product of 4 matrices (python)
def mul4_p(double[::1] a, double[::1] b, double[::1] c, double[::1] d, long ta, long tb, long tc, long td):
    #cdef cnp.ndarray r = np.empty(4, dtype=np.double)
    cdef double r[4]
    mul4(&a[0], &b[0], &c[0], &d[0], ta, tb, tc, td, r)
    return r

# exponential map (cython)
cdef inline void mexp(double* a, double f, double* r) nogil:
    cdef double norm = sqrt(a[0] * a[0] + a[1] * a[1] + a[2] * a[2])
    cdef double sin_factor
    cdef long i
    if norm > 10E-18:
        sin_factor = sin(f * norm * 0.5)
        r[0] = cos(f * norm * 0.5)
        for i in range(3):
            r[i+1] = sin_factor * a[i] / norm
    else:
        r[0] = sqrt(1.0 - 0.25 * norm * norm)
        for i in range(3):
            r[i+1] = 0.5 * a[i]

# log map (cython)
cdef inline void mlog(double* u, double* r) nogil:
    cdef double norm = 1.0 - u[0] * u[0]
    cdef double acosf
    if norm > 10E-18:
        norm = sqrt(norm)
        acosf = 2.0 * acos(u[0]) / norm
        r[0] = u[1] * acosf
        r[1] = u[2] * acosf
        r[2] = u[3] * acosf
    else:
        proj(u, r)

# exponential map (python)
def mexp_p(double[::1] a, double f):
    #cdef cnp.ndarray r = np.empty(4, dtype=np.double)
    cdef double r[4]
    mexp(&a[0], f, r)
    return r

# projection map (cython)
cdef inline void proj(double* u, double* r) nogil:
    r[0] = 2.0 * u[1]
    r[1] = 2.0 * u[2]
    r[2] = 2.0 * u[3]

# projection map (python)
def proj_p(double[::1] u):
    cdef double r[3]
    proj(&u[0], r)
    return r

# adjoint action a -> u^t a u
cdef void act(double *u, double *a, long t, double *r) nogil:
    cdef long i
    cdef double buffer1[4]
    cdef double buffer2[4]
    buffer1[0] = 0.0
    for i in range(1, 4):
        buffer1[i] = a[i-1] * 0.5

    mul2(u, buffer1, -t, 1, buffer2)
    mul2(buffer2, u, 1, t, buffer1)
    proj(buffer1, r)

"""
    Useful functions for temporary fields (setting to zero, unit and addition)
"""

# set group element to zero
cdef inline void group_zero(double* g) nogil:
    cdef long i
    for i in range(4):
        g[i] = 0.0

# set group element to unit
cdef inline void group_unit(double* g) nogil:
    g[0] = 1.0
    cdef long i
    for i in range(1,4):
        g[i] = 0.0

# group add: g0 += f * g1
cdef inline void group_add(double* g0, double* g1, double f) nogil:
    cdef long i
    for i in range(4):
        g0[i] += f * g1[i]

# group set: g0 <- g1
cdef inline void group_set(double* g0, double* g1) nogil:
    cdef long i
    for i in range(4):
        g0[i] = g1[i]

# hermitian conjugate g0 <- g0^t
cdef inline void group_hc(double *g0) nogil:
    cdef long i
    for i in range(1, 4):
        g0[i] = -g0[i]

# set algebra element to zero
cdef inline void algebra_zero(double* a) nogil:
    cdef long i
    for i in range(3):
        a[i] = 0.0

# algebra add: a0 += f * a1
cdef inline void algebra_add(double* a0, double* a1, double f) nogil:
    cdef long i
    for i in range(3):
        a0[i] += f * a1[i]

# algebra add: a0 += f * a1
cdef inline void algebra_mul(double* a, double f) nogil:
    cdef long i
    for i in range(3):
        a[i] *= f

# algebra squared
cdef inline double algebra_sq(double* a) nogil:
    return a[0] * a[0] + a[1] * a[1] + a[2] * a[2]

# algebra dot
cdef inline double algebra_dot(double* a, double* b) nogil:
    return a[0] * b[0] + a[1] * b[1] + a[2] * b[2]

"""
    Plaquette functions
"""

# compute 'positive' plaquette U_{x, i, j}
cdef void plaq_pos(double* u, long x, long i, long j, long* dims, long* acc, double* r) nogil:
    cdef long x0, x1, x2, x3, g0, g1, g2, g3
    x1 = shift2(x, i, 1, dims, acc)
    x2 = shift2(x, j, 1, dims, acc)

    g0 = get_group_index(x, i)
    g1 = get_group_index(x1, j)
    g2 = get_group_index(x2, i)
    g3 = get_group_index(x, j)

    # U_{x, i} * U_{x+i, j} * U_{x+j, i}^t * U_{x, j}^t
    mul4(&u[g0], &u[g1], &u[g2], &u[g3], 1, 1, -1, -1, r)

# compute 'negative' plaquette U_{x, i, -j}
cdef void plaq_neg(double* u, long x, long i, long j, long* dims, long* acc, double* r) nogil:
    cdef long x0, x1, x2, x3, g0, g1, g2, g3
    x0 = x
    x1 = shift2(shift2(x0, i, 1, dims, acc), j, -1, dims, acc)
    x2 = shift2(x1, i, -1, dims, acc)
    x3 = x2

    g0 = get_group_index(x0, i)
    g1 = get_group_index(x1, j)
    g2 = get_group_index(x2, i)
    g3 = get_group_index(x3, j)

    # U_{x, i} * U_{x+i-j, j}^t * U_{x-j, i}^t * U_{x-j, j}
    mul4(&u[g0], &u[g1], &u[g2], &u[g3], 1, -1, -1, 1, r)

# positive plaquette (python)
def plaq_pos_p(double[::1] u, long x, long i, long j, long[::1] dims, long[::1] acc):
    cdef double result[4]
    plaq_pos(&u[0], x, i, j, &dims[0], &acc[0], result)
    return result

# negative plaquette (python)
def plaq_neg_p(double[::1] u, long x, long i, long j, long[::1] dims, long[::1] acc):
    cdef double result[4]
    plaq_neg(&u[0], x, i, j, &dims[0], &acc[0], result)
    return result

"""
    Parallel transport
"""

cdef void transport_neg(double *links, double *field, long x, long d, long *dims, long* acc, double *r) nogil:
    cdef:
        long xs, field_index, group_index
    xs = shift2(x, d, -1, dims, acc)
    field_index = get_field_index(xs, d)
    group_index = get_group_index(xs, d)
    act(&links[group_index], &field[field_index], 1, r)

"""
    Handy grid functions
"""

@cython.returns(cython.long)
def get_index_p(long[::1] pos, long[::1] dims):
    return get_index(pos[0], pos[1], pos[2], &dims[0])

def get_point_p(long x, long[::1] dims):
    cdef long point[3]
    get_point(x, &dims[0], point)
    return point

@cython.returns(cython.long)
def shift_p(long x, long d, long o, long[::1] dims):
    return <long> shift(x, d, o, &dims[0])

@cython.returns(cython.long)
def shift2_p(long x, long d, long o, long[::1] dims, long[::1] acc):
    return <long> shift2(x, d, o, &dims[0], &acc[0])

# compute index from grid point
cdef long get_index(long ix, long iy, long iz, long* dims) nogil:
    return dims[2] * (dims[1] * mod(ix, dims[0]) + mod(iy, dims[1])) + mod(iz, dims[2])

# index from grid point (no modulo)
cdef long get_index_nm(long ix, long iy, long iz, long* dims) nogil:
    return dims[2] * (dims[1] * ix + iy) + iz

# compute grid point from index
cdef void get_point(long x, long* dims, long* r) nogil:
    cdef long i
    for i in range(2, -1, -1):
        r[i] = x % dims[i]
        x -= r[i]
        x /= dims[i]

# inefficient index shifting
cdef long shift(long x, long i, long o, long* dims) nogil:
    cdef long pos[3]
    get_point(x, dims, pos)
    pos[i] = (pos[i] + o + dims[i]) % dims[i]
    return get_index_nm(pos[0], pos[1], pos[2], dims)

# efficient index shifting
cdef long shift2(long x, long i, long o, long* dims, long* acc) nogil:
    cdef long res, di, wdi
    res = x
    di = x / acc[i + 1]
    wdi = di % dims[i]
    if o > 0:
        if wdi == dims[i] - 1:
            res -= acc[i]
        res += acc[i+1]
    else:
        if wdi == 0:
            res += acc[i]
        res -= acc[i+1]
    return res

cdef inline long mod(long i, long n) nogil:
    return (i + n) % n

cdef inline long get_field_index(long i, long d) nogil:
    return 3*(3*i + d)

cdef inline long get_group_index(long i, long d) nogil:
    return 4*(3*i + d)
