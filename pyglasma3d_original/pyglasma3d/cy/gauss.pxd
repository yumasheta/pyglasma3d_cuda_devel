"""
    Gauss constraint functions
"""
from lattice cimport *

cdef void gauss(double *links, double *electric, double *rho, long x, double * spacings, long *dims, long* acc, double g, double *r) nogil
cdef void gauss_density(double *links, double *electric, double *rho, double *spacings, long *dims, long *acc, long[::1] iter_dims, double g, double *result)
