import numpy as np
from pyglasma3d.utils.data_io import read as read_data
import matplotlib.pyplot as plt

data = [
        read_data('./chr_semi_256.dat'),
        read_data('./chr_lpfrg_256.dat'),
        read_data('./chr_lpfrg_512.dat'),
        read_data('./chr_lpfrg_1024.dat'),
        read_data('./chr_lpfrg_2048.dat'),
        ]

energy = []
for d in data:
    energy.append(np.sum(d['EL']+d['BL']+d['ET']+d['BT'], axis=1))

rel_energy = []
for e in energy:
    re = (e - e[0]) / e[0]
    rel_energy.append(re)
    print(re[-1]*100)

# write data
n = len(rel_energy[0])
output = [np.arange(0.0, 2.0, 2.0 / n)[:n]]
skips = [1, 1, 2, 4, 8]
for i in range(len(rel_energy)):
    re = rel_energy[i]
    output.append(np.array(re)[::skips[i]][:n])
output = np.array(output)
np.savetxt('increase.dat', output.T)

for re in rel_energy:
    x = np.arange(0.0, 3.0, 3.0 / len(re))
    plt.plot(x, re)

plt.show()