from __future__ import print_function

import numpy as np
import time

import pyglasma3d.cy.mv as mv
import pyglasma3d.cy.interpolate as interpolate

from pyglasma3d.core import Simulation

"""
    Collisions of two "nuclei"
    One of the nuclei is perturbed: the original Gaussian shape is slightly deformed and superimposed with a second,
    smaller Gaussian profile, which is slightly shifted.

    The script simulates a certain number of events and performs data averaging after finishing.
"""

use_perturbation = True
run = 'mv_perturb_t001'    # data will end up in `./output/T_<run>_e<num>.dat`
num_events = 1

# grid size (make sure that ny == nz)
nx, ny, nz = 256, 128, 128
steps = 2
a = [0.5, 1.0, 1.0]
dt = a[0] / steps
g = 2.0
np.random.seed(2448)

mu = 0.04
sigma = 4.0
ir = 0.01
uvt = 1.0

aL = a[0]
aT = a[1]

max_iters = int(nx * 0.5)

"""
    Loop over events
"""
for ev in range(num_events):

    """
        Initialization
    """

    # initialize simulation object
    dims = [nx, ny, nz]
    s = Simulation(dims=dims, a=a, dt=dt, g=g, iter_dims=[1, nx-1, 0, ny, 0, nz])
    s.debug = True

    s.log("STARTING EVENT NO. " + str(ev))

    t = time.time()
    s.log("Initializing left nucleus.")
    v = mv.initialize_mv(s, x0=nx * 0.25 * aL, mu=mu, sigma=sigma, mass=ir, uvt=uvt, orientation=+1)
    interpolate.initialize_charge_planes(s, +1, 0, nx / 2)
    s.log("Initialized left nucleus.", round(time.time()-t, 3))

    if use_perturbation:
        t = time.time()
        s.log("Initializing right (perturbed) nucleus.")
        v = mv.initialize_mv_perturbed(s, x0=nx * 0.75 * aL, mu=mu, sigma=sigma, mass=ir, uvt=uvt, orientation=-1,
                                       amplitude_perturbed=0.2, shift_perturbed=3*sigma, sigma_perturbed=sigma)
        interpolate.initialize_charge_planes(s, -1, nx / 2, nx)
        s.log("Initialized right (perturbed) nucleus.", round(time.time()-t, 3))
    else:
        t = time.time()
        s.log("Initializing right (unperturbed) nucleus.")
        v = mv.initialize_mv(s, x0=nx * 0.75 * aL, mu=mu, sigma=sigma, mass=ir, uvt=uvt, orientation=-1)
        interpolate.initialize_charge_planes(s, -1, nx / 2, nx)
        s.log("Initialized right (unperturbed) nucleus.", round(time.time()-t, 3))

    s.init()

    """
        Simulation loop
    """

    file_path = './output/T_' + run + '_e' + str(ev) + '.dat'

    t = time.time()
    for it in range(max_iters):
        # this for loop moves the nuclei exactly one grid cell
        for step in range(steps):
            t = time.time()
            s.evolve()
            print(s.t, "Complete cycle.", round(time.time()-t, 3))

        t = time.time()

        s.write_energy_momentum(max_iters, file_path)
        print(s.t, "Writing energy momentum tensor to file.", round(time.time()-t, 3))

"""
    Data averaging
"""

# import all output files and average
file_path = './output/T_' + run + '_e' + str(0) + '.dat'
f = open(file_path, "r")
# read header
header = np.fromfile(f, count=2, dtype=np.int64)

# read rest of data
f.seek(16)

data_avg = np.fromfile(f, dtype=np.double)
f.close()

for ev in range(1, num_events):
    file_path = './output/T_' + run + '_e' + str(ev) + '.dat'
    f = open(file_path, "r")

    # read rest of data
    f.seek(16)

    data = np.fromfile(f, dtype=np.double)
    data_avg += data
    f.close()

data_avg /= num_events

# write to new file
file_path = './output/T_' + run + '_avg' + '.dat'
f = open(file_path, "w+")
f.write(header)
f.write(data_avg)
f.close()
