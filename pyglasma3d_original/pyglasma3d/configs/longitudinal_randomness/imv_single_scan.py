from __future__ import print_function

import numpy as np
import time

import pyglasma3d.cy.mv as mv
import pyglasma3d.cy.interpolate as interpolate

from pyglasma3d.core import Simulation

"""
    A script for studying the numerical errors in simulations of nuclei with random longitudinal structure.
    
    A single left-moving nucleus is initialized and traverses the simulation box. Field components are recorded so
    that we can study the superfluous longitudinal fields that are created due to numerical dispersion.
    
    The longitudinal correlation length is a varying parameter.
"""

run = 'imv_single_scan'    # data will end up in `./output/T_<run>_e<num>.dat`
num_events = 10

# correlation length (ratio) parameters
clen_min = 0.2
clen_max = 2.0
clens = np.linspace(clen_max, clen_min, num=num_events, endpoint=True)


# grid size (make sure that ny == nz)
nx, ny, nz = 1024, 128, 128

# transverse and longitudinal box widths [fm]
LT = 3.0
LL = 3.0

# collision energy [MeV]
sqrts = 100.0 * 1000.0

# infrared and ultraviolet regulator [MeV]
m = 200.0
uv = 10.0 * 1000.0

# ratio between dt and aL [int, multiple of 2]
steps = 2

# option for debug
debug = True

"""
    The rest of the parameters are computed automatically.
"""

# constants
hbarc   = 197.3270      # hbarc [MeV*fm]
RAu     = 7.27331       # Gold nuclear radius [fm]

# determine lattice spacings and energy units
aT_fm   = LT / ny
E0      = hbarc / aT_fm
aT      = 1.0
aL_fm   = LL / nx
aL      = aL_fm / aT_fm
a       = [aL, aT, aT]
dt      = aL / steps

# determine initial condition parameters
gamma   = sqrts / 2000.0
Qs      = np.sqrt((sqrts/1000.0) ** 0.25) * 1000.0
alphas  = 12.5664 / (18.0 * np.log(Qs / 217.0))
g       = np.sqrt(12.5664 * alphas)
mu      = Qs / (g * g * 0.75) / E0
uvt     = uv / E0
ir      = m / E0
sigma   = RAu / (2.0 * gamma) / aL_fm * aL
sigma_c = sigma / aL

# number of evolution steps (max_iters) and output file path
max_iters = 2 * nx / 3
file_path = './output/T_' + run + '.dat'


"""
    Loop over events
"""
for ev in range(num_events):

    """
        Initialization
    """

    # correlation length
    clen_value = clens[ev] * sigma

    # initialize simulation object
    dims = [nx, ny, nz]
    s = Simulation(dims=dims, a=a, dt=dt, g=g, iter_dims=[1, nx-1, 0, ny, 0, nz])
    s.debug = True

    s.log("STARTING EVENT NO. " + str(ev))

    t = time.time()
    s.log("Initializing nucleus.")

    v = mv.initialize_mv_inc(s, x0=nx * 0.75 * aL, mu=mu, sigma=sigma, clen=clen_value, mass=ir, uvt=uvt, orientation=-1)
    interpolate.initialize_charge_planes(s, -1, nx / 2, nx)

    s.log("Initialized nucleus.", round(time.time()-t, 3))
    s.init()

    """
        Simulation loop
    """

    file_path = './output/T_' + run + '_e' + str(ev) + '.dat'

    t = time.time()
    for it in range(max_iters):
        # this for loop moves the nuclei exactly one grid cell
        for step in range(steps):
            t = time.time()
            s.evolve()
            print(s.t, "Complete cycle.", round(time.time()-t, 3))

        t = time.time()

        s.write_energy_momentum(max_iters, file_path)
        print(s.t, "Writing energy momentum tensor to file.", round(time.time()-t, 3))