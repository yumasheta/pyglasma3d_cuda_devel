"""
    A script for downsampling energy-momentum tensor output files
"""
import numpy as np
import data_io
import sys


def downsample(input_path, output_path, step):
    data = data_io.read(input_path)

    MT = data['MT']
    NL = data['NL']
    nMT = MT / step
    nNL = NL / step

    nEL = rebin(data['EL'], (nMT, nNL))
    nBL = rebin(data['BL'], (nMT, nNL))
    nET = rebin(data['ET'], (nMT, nNL))
    nBT = rebin(data['BT'], (nMT, nNL))
    nSL = rebin(data['SL'], (nMT, nNL))

    data_io.write(output_path, nEL, nBL, nET, nBT, nSL)


def downsample2(input_path, step):
    data = data_io.read(input_path)

    MT = data['MT']
    NL = data['NL']
    nMT = MT / step
    nNL = NL / step

    nEL = rebin(data['EL'], (nMT, nNL))
    nBL = rebin(data['BL'], (nMT, nNL))
    nET = rebin(data['ET'], (nMT, nNL))
    nBT = rebin(data['BT'], (nMT, nNL))
    nSL = rebin(data['SL'], (nMT, nNL))

    return [nEL, nBL, nET, nBT, nSL]


def rebin(a, shape):
    sh = shape[0], a.shape[0] // shape[0], shape[1], a.shape[1] // shape[1]
    return a.reshape(sh).mean(-1).mean(1)


# command line call
if __name__ == "__main__":
    input_path = sys.argv[1]
    output_path = sys.argv[2]
    step = int(sys.argv[3])

    downsample(input_path, output_path, step)
