import numpy as np
import scipy.interpolate

"""
    A script for generating rapidity profiles from the standard T_\mu\nu output.
"""


def energy_profile(data, tau_range, eta_range, center='eL', symmetric=True):
    # read data from dict
    NL = data['NL']
    MT = data['MT']
    EL = data['EL']
    BL = data['BL']
    ET = data['ET']
    BT = data['BT']
    SL = data['SL']

    # t and z coordinates
    z_axis = np.linspace(0, NL, NL)
    t_axis = np.linspace(0, MT, MT)

    # rest-frame energy density
    eL = EL + BL
    eT = ET + BT
    eLoc = eT ** 2 - SL ** 2
    eLoc[eLoc < 0] = 0.0
    eLoc = eL + np.sqrt(eLoc)

    # collision center coordinates
    if center is 'eL':
        c = np.where(np.max(eL) == eL)
        t0, z0 = c[0][0], c[1][0]
    elif center is 'e':
        c = np.where(np.max(eL+eT) == eL+eT)
        t0, z0 = c[0][0], c[1][0]
    else:
        print("Unknown collision center type.")
        return

    # shift coordinates and interpolate
    t_axis -= t0
    z_axis -= z0
    eLoc_interp = scipy.interpolate.interp2d(z_axis, t_axis, eLoc, kind='cubic')

    profiles = []
    for tau in tau_range:
        eLoc_data = [eLoc_interp(tau * np.sinh(eta), tau * np.cosh(eta)) for eta in eta_range] / eLoc_interp(0.0, tau)
        if symmetric:
            eLoc_data = (eLoc_data[::] + eLoc_data[::-1]) / 2
        profiles.append(eLoc_data)
    return profiles


def anisotropy_profile(data, tau_range, eta_range, center='eL', symmetric=True):
    # read data from dict
    NL = data['NL']
    MT = data['MT']
    EL = data['EL']
    BL = data['BL']
    ET = data['ET']
    BT = data['BT']
    SL = data['SL']

    # t and z coordinates
    z_axis = np.linspace(0, NL, NL)
    t_axis = np.linspace(0, MT, MT)

    # rest-frame energy density and pressure
    eL = EL + BL
    eT = ET + BT
    tmp = eT ** 2 - SL ** 2
    tmp[tmp < 0] = 0.0
    eLoc = eL + np.sqrt(tmp)
    pL_loc = eL - np.sqrt(tmp)
    pT_loc = eL


    # collision center coordinates
    if center is 'eL':
        c = np.where(np.max(eL) == eL)
        t0, z0 = c[0][0], c[1][0]
    elif center is 'e':
        c = np.where(np.max(eL+eT) == eL+eT)
        t0, z0 = c[0][0], c[1][0]
    else:
        print("Unknown collision center type.")
        return

    # shift coordinates and interpolate
    t_axis -= t0
    z_axis -= z0
    pLr = scipy.interpolate.interp2d(z_axis, t_axis, pL_loc/eLoc, kind='cubic')
    pTr = scipy.interpolate.interp2d(z_axis, t_axis, pT_loc/eLoc, kind='cubic')

    profiles_pL = []
    profiles_pT = []
    for tau in tau_range:
        d = [pLr(tau * np.sinh(eta), tau * np.cosh(eta)) for eta in eta_range]
        if symmetric:
            d = (d[::] + d[::-1]) / 2
        profiles_pL.append(d)

        d = [pTr(tau * np.sinh(eta), tau * np.cosh(eta)) for eta in eta_range]
        if symmetric:
            d = (d[::] + d[::-1]) / 2
        profiles_pT.append(d)

    return profiles_pL, profiles_pT
