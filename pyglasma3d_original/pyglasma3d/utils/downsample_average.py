from downsample import downsample2
from data_io import write
import sys
import os
import numpy as np

# command line call
if __name__ == "__main__":
    # input format
    input_path = sys.argv[1]
    output_path = sys.argv[2]
    step = int(sys.argv[3])

    # get files based on input path (i.e. starting with)
    parent = os.path.dirname(input_path)

    files = []
    for f in os.listdir(parent):
        if os.path.join(parent, f).startswith(input_path):
            files.append(os.path.join(parent, f))

    # downsample each file and average
    data = downsample2(files[0], step)
    for fi in range(1, len(files)):
        f = files[fi]
        data += downsample2(f, step)
    data = np.multiply(data, 1.0/len(files))
    write(output_path, data[0], data[1], data[2], data[3], data[4])

