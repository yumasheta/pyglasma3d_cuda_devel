# a short example regarding reading and plotting the binary format
import numpy as np
from data_io import read as read_data
import matplotlib.pyplot as plt
import pyglasma3d.utils.rapidity


data = [
        #read_data('/home/dmueller/Documents/data/imv_kappa01_e0.dat'),
        #read_data('/home/dmueller/Documents/data/imv_kappa01_e5.dat'),
        #read_data('/home/dmueller/Documents/data/imv_kappa01.dat'),
        #read_data('/home/dmueller/Documents/data/imv_kappa10.dat'),
        #read_data('/home/dmueller/PycharmProjects/pyglasma3d/output/T_batchtest_mpi_avg.dat'),
        #read_data('/home/dmueller/PycharmProjects/pyglasma3d/output/T_batchtest_no_mpi_avg.dat'),
        read_data('/home/dmueller/PycharmProjects/pyglasma3d/pyglasma3d/examples/output/cherenkov_leapfrog.dat'),
        ]

# some general definitions
NL = data[0]['NL']
MT = data[0]['MT']
z_axis = np.linspace(0, NL, NL)
t_axis = np.linspace(0, MT, MT)
z_ones = np.ones(NL)
t_ones = np.ones(MT)

# energy rapidity profiles
tau_range = np.arange(100, 200, 10)
eta_range = np.arange(-1, 1, 0.001)

fig1 = plt.figure(1)
plt.title("Rapidity profile of rest-frame energy density")
for d in data:
    profiles = pyglasma3d.utils.rapidity.energy_profile(d,
                                                        tau_range,
                                                        eta_range,
                                                        center='eL',
                                                        symmetric=True)

    tau_fm = 4.0 / data[0]['NL'] * tau_range

    for profile in profiles:
        plt.plot(eta_range, profile, c='blue')

plt.plot(eta_range, np.exp(-0.5*(eta_range/2.34)**2), c='red')
plt.plot(eta_range, np.exp(-0.5*(eta_range/1.66)**2), c='red')
plt.axes().set_ylim(0.5, 1.2)
fig1.show()

# pressure anisotropy
z_avg = 20
fig2 = plt.figure(2)
plt.title("Pressure to energy density ratio at mid-rapidity")
for d in data:
    pT = np.mean((d['EL'] + d['BL'])[:, (NL-z_avg)/2:(NL+z_avg)/2], axis=1)
    pL = np.mean((d['ET'] + d['BT'])[:, (NL-z_avg)/2:(NL+z_avg)/2], axis=1) - pT
    e = pL + 2*pT

    plt.plot(t_axis, pT/e, c="red")
    plt.plot(t_axis, pL/e, c="blue")

plt.plot(t_axis, t_ones * 0.0, c="black")
plt.plot(t_axis, t_ones * 0.33333, c="black")
plt.plot(t_axis, t_ones * 0.5, c="black")
plt.axis([0, MT, -0.1, 1.1])
fig2.show()

# snapshot of energy density components
fig3 = plt.figure(3)
plt.title("Field components as a function of z")

eMax = np.max(data[0]['EL'] + data[0]['BL'] + data[0]['ET'] + data[0]['BT'])
for d in data:
    for t0 in [0, MT/2, 3*MT/4, MT-1]:
        t = int(t0)
        plt.plot((d['EL'] + d['BL'])[t]/eMax, c="red")
        plt.plot((d['ET'] + d['BT'])[t]/eMax, c="blue")

plt.axis([0, NL, 10 ** (-10), 1])
plt.yscale('log')
fig3.show()

# test movie
"""
plt.ion()
for d in data:
    plt.clf()
    E = np.sum(d['EL']+d['BL']+d['ET']+d['BT'], axis=1)
    while False:
        s = 5
        for t in range(s, MT, s):
            plt.clf()
            plt.plot((d['EL'])[t] / eMax, c="red")
            plt.plot((d['BL'])[t] / eMax, c="purple")
            plt.plot((d['ET'])[t] / eMax, c="blue")
            plt.plot((d['BT'])[t] / eMax, c="green")
            plt.axis([0, NL, 10 ** (-10), 1])
            plt.yscale('log')
            plt.pause(0.001)
            print(t, (E[t]-E[t-s])/s)
plt.ioff()
"""

# magnetic suppression
z_avg = 1

fig4 = plt.figure(4)
plt.title("Field ratios at mid-rapidity (1)")
for d in data:
    plt.plot(t_axis, np.mean((d['BL']/d['EL'])[:, (NL-z_avg)/2:(NL+z_avg)/2], axis=1), c="red")
    plt.plot(t_axis, np.mean((d['ET']/d['BT'])[:, (NL-z_avg)/2:(NL+z_avg)/2], axis=1), c="blue")

plt.plot(t_axis, t_ones * 1.0, c="black")
plt.axis([0, MT, 0.0, 1.4])
fig4.show()

# weak fields?
z_avg = 20

fig5 = plt.figure(5)
plt.title("Field ratios at mid-rapidity (2)")
for d in data:
    plt.plot(t_axis, np.mean((d['BL']/d['ET'])[:, (NL-z_avg)/2:(NL+z_avg)/2], axis=1), c="red")
    plt.plot(t_axis, np.mean((d['EL']/d['BT'])[:, (NL-z_avg)/2:(NL+z_avg)/2], axis=1), c="blue")

plt.plot(t_axis, t_ones * 1.0, c="black")
plt.axis([0, MT, 0.0, 1.4])
fig5.show()

# energy production / check for instability
fig6 = plt.figure(6)
plt.title("Total energy as a function of time")
for d in data:
    E = np.sum(d['EL']+d['BL']+d['ET']+d['BT'], axis=1)
    dE = np.diff(E) / E[0]
    plt.plot(t_axis[0:-1], dE)
plt.axis([0, MT, 0.0, 1.0])
plt.autoscale(axis='y')
fig6.show()


plt.show()

