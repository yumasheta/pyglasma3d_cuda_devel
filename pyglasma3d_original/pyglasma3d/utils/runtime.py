"""
    pyglasma3d expected run time on VSC
"""

# input
nodes = 8
nx, ny, nz = 2048, 192, 192
steps = 2
iterations = nx * 4

# 3 nodes, 2048x256^2, 4 steps (16.03.2017)
# timings in seconds, rounded up to whole second
cycle = 9
diagnostic = 6
size = 2048 * 256 * 256

# calculate new expected runtime
factor = (nx * ny * nz) / float(size)
cycles = iterations * cycle * steps * factor
diagnostics = iterations * diagnostic * factor
time = (cycles + diagnostics) / 60.0 / 60.0
print("Runtime: " + str(round(time, 2)) + "h")
