"""
    Simple read and write functions for energy-momentum tensor files
"""
import numpy as np


def read(file_path, mt_manual=None):
    f = open(file_path)

    # read header
    header = np.fromfile(f, dtype=np.int64, count=2)
    NL = header[0]  # longitudinal cells

    if mt_manual is None:
        MT = header[1]  # max time
    else:
        # manual override for incomplete runs
        MT = mt_manual

    f.seek(16)
    # MT = 380

    # read body
    body = np.fromfile(f, dtype=np.double, count=-1).reshape(MT, 5, NL)
    EL = body[:, 0, :]
    BL = body[:, 1, :]
    ET = body[:, 2, :]
    BT = body[:, 3, :]
    SL = body[:, 4, :]

    return {'EL': EL, 'BL': BL, 'ET': ET, 'BT': BT, 'SL': SL, 'NL': NL, 'MT': MT}


def write(file_path, EL, BL, ET, BT, SL):
    f = open(file_path, "w+")

    # header data
    MT = len(EL)
    NL = len(EL[0])

    f.write(np.array([NL], dtype=np.int64).tobytes())
    f.write(np.array([MT], dtype=np.int64).tobytes())

    # body data
    for t in range(MT):
        f.write(EL[t])
        f.write(BL[t])
        f.write(ET[t])
        f.write(BT[t])
        f.write(SL[t])

    f.close()
