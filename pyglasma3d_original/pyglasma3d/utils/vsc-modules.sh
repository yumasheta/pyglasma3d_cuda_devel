# preamble for VSC3

export OMP_NUM_THREADS=32

export I_MPI_PMI_LIBRARY=/cm/shared/apps/slurm/current/lib/libpmi.so
export I_MPI_OFA_TRANSLATION_CACHE=on

module purge
module load intel/16 intel-mpi/5.1 python/2.7 mpi4py/2.0.0 intel-mkl/11 numpy/1.12.0 likwid/4.0