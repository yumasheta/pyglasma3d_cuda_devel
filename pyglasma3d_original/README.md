# pyglasma3d
### A 3D Glasma simulation implemented in Cython

This project aims to port and optimise the most important features
of the Java-based [OpenPixi](https://github.com/openpixi/openpixi/)
project to [Cython](http://cython.org/) and Python.
The code is used to simulate the early stages of heavy-ion
collisions in the Color Glass Condensate (CGC) framework using the
Colored Particle-in-Cell method (CPIC). The numerical method is
described in detail in our publications: 
[arXiv:1605.07184](https://arxiv.org/abs/1605.07184), 
[arXiv:1703.00017](https://arxiv.org/abs/1703.00017).

* * *

This version of the original [pyglasma3d](https://gitlab.com/monolithu/pyglasma3d) code is slightly modified:
- `./setup.sh`:  
  The setup script was modified to enforce Python version 2 for compilation.
  
- `./pyglasma3d/examples/mv.py`:  
  This script was modified to yield the same numerical results for different runs. At line 78 a fixed seed for the
  random number generator is set to ensure that the same set of random numbers is generated for each run.  
  This is necessary to be able to compare different runs.
  
- `./pyglasma3d/examples/mv_simple.py` & `./pyglasma3d/examples/mv_very_simple.py`:  
  These two simulation setup scripts were added.  
  They provide simplified versions of the `./pyglasma3d/examples/mv.py` setup with reduced grid sizes and iteration counts.
  
- `./pyglasma3d/examples/mv_simple_gauss.py`:  
  This setup is based on the `./pyglasma3d/examples/mv_simple.py` setup script. However, it does not produce an
  output file but instead only prints information to the execution shell. The output only contains information
  about the gauss constraints and gauss violation.

* * *

Run `setup.sh` to compile the Cython modules  
run example codes in `pyglasma3d.examples` to try it out, e.g

```python -m pyglasma3d.examples.mv```

**Implemented features**
* Leap-frog solver for Yang-Mills fields on a lattice and
nearest-grid-point (NGP) interpolation for color currents
* Computing various energy momentum tensor components
* Unit tests for many functions
* McLerran Venugopalan model initial conditions with and without
randomness in the longitudinal extent
* Multi-threading using Cython's `prange` (basically OpenMP)
* Distributed memory (MPI) using [mpi4py](http://pythonhosted.org/mpi4py/)
* Coulomb gauge
* Gluon occupation numbers *(inaccurate, still work in progress)*
* Semi-implicit solver for increased stability

**Future features**
* Readable, modular code *(haha)*
* JIMWLK evolution code

**Dependencies**
* Python 2.7.9
* NumPy 1.11.0
* SciPy 0.17.0 (only for unit tests)
* Cython 0.24.1
* gcc 5.4.0 or equivalent C compiler
* Matplotlib 1.5.1 (only for interactive runs)